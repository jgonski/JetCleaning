#include "JetCleaning/Candidate.h"

#include <TTree.h>

Analysis::Candidate::Candidate() {
    m_doJetSegments = kFALSE;
    m_fillLCJets = kFALSE;
    m_fillPFJets = kTRUE;
    m_fillLeptons = kTRUE;

    JetCleanVarf_keys.push_back("centR");
    JetCleanVarf_keys.push_back("secondR");
    JetCleanVarf_keys.push_back("ootfc5");
    JetCleanVarf_keys.push_back("ootfc10");
    JetCleanVarf_keys.push_back("n90const");

    JetCleanVarf_keys.push_back("timing");
    JetCleanVarf_keys.push_back("emfrac");
    JetCleanVarf_keys.push_back("hecf");
    JetCleanVarf_keys.push_back("hecq");
    JetCleanVarf_keys.push_back("fch"); //
    JetCleanVarf_keys.push_back("ntrk");
    //JetCleanVarf_keys.push_back("fchl");
    JetCleanVarf_keys.push_back("larq");
    JetCleanVarf_keys.push_back("avglarq");
    JetCleanVarf_keys.push_back("fmax");
    JetCleanVarf_keys.push_back("nege");
    JetCleanVarf_keys.push_back("clust1_pt");
    JetCleanVarf_keys.push_back("clust1_phi");
    JetCleanVarf_keys.push_back("clust1_eta");
    JetCleanVarf_keys.push_back("lambda");
    JetCleanVarf_keys.push_back("lambda2");
    //JetCleanVarf_keys.push_back("jvtxf");
    JetCleanVarf_keys.push_back("clust_sumpt");
    //JetCleanVarf_keys.push_back("clust_sumet");
    //JetCleanVarf_keys.push_back( "SV1plusIP3D_discriminant");
    //JetCleanVarf_keys.push_back("TruthLabelDeltaR_B");
    //JetCleanVarf_keys.push_back("TruthLabelDeltaR_C");
    //JetCleanVarf_keys.push_back("TruthLabelDeltaR_T");
    JetCleanVarf_keys.push_back("JVFCorr");
    JetCleanVarf_keys.push_back("jvt");
    JetCleanVarf_keys.push_back("passJVT");

    JetCleanVari_keys.push_back("isSpike");
    JetCleanVari_keys.push_back("fmaxi");
    JetCleanVari_keys.push_back("clust_n");
    JetCleanVari_keys.push_back("segment_n");
    //JetCleanVari_keys.push_back("PartonTruthLabelID");
    //JetCleanVari_keys.push_back("ConeTruthLabelID");

    JetSegmentVari_keys.push_back("segment_sector");
    JetSegmentVari_keys.push_back("segment_etaIndex");
    JetSegmentVari_keys.push_back("segment_nPrecisionHits");
    JetSegmentVari_keys.push_back("segment_nPhiLayers");
    JetSegmentVari_keys.push_back("segment_nTrigEtaLayers");

    JetSegmentVarf_keys.push_back("segment_x");
    JetSegmentVarf_keys.push_back("segment_y");
    JetSegmentVarf_keys.push_back("segment_z");
    JetSegmentVarf_keys.push_back("segment_px");
    JetSegmentVarf_keys.push_back("segment_py");
    JetSegmentVarf_keys.push_back("segment_pz");
    JetSegmentVarf_keys.push_back("segment_t0");
    JetSegmentVarf_keys.push_back("segment_t0error");
    JetSegmentVarf_keys.push_back("segment_chiSquared");
    JetSegmentVarf_keys.push_back("segment_numberDoF");

    JetKinematics_keys.push_back("pt");
    JetKinematics_keys.push_back("eta");
    JetKinematics_keys.push_back("phi");
    JetKinematics_keys.push_back("raw_pt");
    JetKinematics_keys.push_back("raw_eta");
    JetKinematics_keys.push_back("raw_phi");
    JetKinematics_keys.push_back("dphi_met");
    JetKinematics_keys.push_back("dphi_ee");
    JetKinematics_keys.push_back("dphi_mumu");
    JetKinematics_keys.push_back("m");
    JetKinematics_keys.push_back("e");
    
    LepKinematics_keys.push_back("pt");
    LepKinematics_keys.push_back("eta");
    LepKinematics_keys.push_back("phi");
    LepKinematics_keys.push_back("m");
    LepKinematics_keys.push_back("e");


    // data16 commissioning triggers
    indiv_trigger_keys.push_back("HLT_noalg_L1LowLumi");
    indiv_trigger_keys.push_back("HLT_noalg_L1J12");
    indiv_trigger_keys.push_back("HLT_noalg_L1J15");
    indiv_trigger_keys.push_back("HLT_noalg_L1J20");
    indiv_trigger_keys.push_back("HLT_noalg_L1J25");
    indiv_trigger_keys.push_back("HLT_noalg_L1J30");
    indiv_trigger_keys.push_back("HLT_noalg_L1J40");
    indiv_trigger_keys.push_back("HLT_noalg_L1J50");
    indiv_trigger_keys.push_back("HLT_noalg_L1J75");
    indiv_trigger_keys.push_back("HLT_noalg_L1J85");
    indiv_trigger_keys.push_back("HLT_noalg_L1J100");
    indiv_trigger_keys.push_back("HLT_noalg_L1J120");

    indiv_trigger_keys.push_back("HLT_j360_a4tchad");
    indiv_trigger_keys.push_back("L1_J12");
    indiv_trigger_keys.push_back("L1_J30");
    indiv_trigger_keys.push_back("L1_J50");
    indiv_trigger_keys.push_back("L1_J75");
    indiv_trigger_keys.push_back("L1_J100");
    indiv_trigger_keys.push_back("L1_J120");
    indiv_trigger_keys.push_back("L1_XE35");
    indiv_trigger_keys.push_back("L1_MBTS_1_1");
    indiv_trigger_keys.push_back("L1_RD0_FILLED");
    indiv_trigger_keys.push_back("HLT_noalg_L1XE35");

    /*indiv_trigger_keys.push_back("HLT_j15");
    indiv_trigger_keys.push_back("HLT_j25");
    indiv_trigger_keys.push_back("HLT_j35");
    indiv_trigger_keys.push_back("HLT_j60");
    indiv_trigger_keys.push_back("HLT_j260");*/
    indiv_trigger_keys.push_back("HLT_xe80");
    indiv_trigger_keys.push_back("HLT_xe100");
    indiv_trigger_keys.push_back("HLT_xe80_tc_lcw_L1XE50");
    indiv_trigger_keys.push_back("HLT_xe90_mht_L1XE50");
    indiv_trigger_keys.push_back("HLT_xe100_mht_L1XE50");
    indiv_trigger_keys.push_back("HLT_xe110_mht_L1XE50");
    indiv_trigger_keys.push_back("HLT_xe110_mht_L1XE50");

    // from Physics_pp_v5.py
    indiv_trigger_keys.push_back("HLT_j100");
    indiv_trigger_keys.push_back("HLT_j110");
    indiv_trigger_keys.push_back("HLT_j110_320eta490");
    indiv_trigger_keys.push_back("HLT_j15");
    indiv_trigger_keys.push_back("HLT_j150");
    indiv_trigger_keys.push_back("HLT_j175");
    indiv_trigger_keys.push_back("HLT_j175_320eta490");
    indiv_trigger_keys.push_back("HLT_j200");
    //indiv_trigger_keys.push_back("HLT_j200_jes_PS");

    indiv_trigger_keys.push_back("HLT_j25");

    indiv_trigger_keys.push_back("HLT_j260");
    indiv_trigger_keys.push_back("HLT_j260_320eta490");
    //indiv_trigger_keys.push_back("HLT_j260_320eta490_jes");
    //indiv_trigger_keys.push_back("HLT_j260_320eta490_lcw");
    //indiv_trigger_keys.push_back("HLT_j260_320eta490_lcw_jes");
    //indiv_trigger_keys.push_back("HLT_j260_320eta490_lcw_nojcalib");
    //indiv_trigger_keys.push_back("HLT_j260_320eta490_nojcalib");

    indiv_trigger_keys.push_back("HLT_j300");
    //indiv_trigger_keys.push_back("HLT_j300_lcw_nojcalib");

    indiv_trigger_keys.push_back("HLT_j320");

    indiv_trigger_keys.push_back("HLT_j35");
    //indiv_trigger_keys.push_back("HLT_j35_jes");
    //indiv_trigger_keys.push_back("HLT_j35_lcw");
    //indiv_trigger_keys.push_back("HLT_j35_lcw_jes");
    //indiv_trigger_keys.push_back("HLT_j35_lcw_nojcalib");
    //indiv_trigger_keys.push_back("HLT_j35_nojcalib");

    indiv_trigger_keys.push_back("HLT_j45");

    indiv_trigger_keys.push_back("HLT_j360");
    indiv_trigger_keys.push_back("HLT_j360_320eta490");
    //indiv_trigger_keys.push_back("HLT_j360_320eta490_jes");
    //indiv_trigger_keys.push_back("HLT_j360_320eta490_lcw");
    //indiv_trigger_keys.push_back("HLT_j360_320eta490_lcw_jes");
    //indiv_trigger_keys.push_back("HLT_j360_320eta490_lcw_nojcalib");
    //indiv_trigger_keys.push_back("HLT_j360_320eta490_nojcalib");

    //indiv_trigger_keys.push_back("HLT_j380");
    //indiv_trigger_keys.push_back("HLT_j380_jes");
    //indiv_trigger_keys.push_back("HLT_j380_lcw");
    //indiv_trigger_keys.push_back("HLT_j380_lcw_jes");
    //indiv_trigger_keys.push_back("HLT_j380_lcw_nojcalib");
    //indiv_trigger_keys.push_back("HLT_j380_nojcalib");

    indiv_trigger_keys.push_back("HLT_j400");
    //indiv_trigger_keys.push_back("HLT_j400_jes");
    //indiv_trigger_keys.push_back("HLT_j400_lcw");
    //indiv_trigger_keys.push_back("HLT_j400_lcw_jes");
    //indiv_trigger_keys.push_back("HLT_j400_nojcalib");
    //indiv_trigger_keys.push_back("HLT_j400_sub");

    indiv_trigger_keys.push_back("HLT_j420");
    //indiv_trigger_keys.push_back("HLT_j420_jes");
    //indiv_trigger_keys.push_back("HLT_j420_lcw");
    //indiv_trigger_keys.push_back("HLT_j420_lcw_jes");
    //indiv_trigger_keys.push_back("HLT_j420_lcw_nojcalib");
    //indiv_trigger_keys.push_back("HLT_j420_nojcalib");


    indiv_trigger_keys.push_back("HLT_j440");
    //indiv_trigger_keys.push_back("HLT_j45_L1RD0_FILLED");

    indiv_trigger_keys.push_back("HLT_j460");
    //indiv_trigger_keys.push_back("HLT_j460_a10_sub_L1J100");
    //indiv_trigger_keys.push_back("HLT_j460_a10r_L1J100");
    //indiv_trigger_keys.push_back("HLT_j460_a10_nojcalib_L1J100");
    //indiv_trigger_keys.push_back("HLT_j460_a10_lcw_nojcalib_L1J100");
    //indiv_trigger_keys.push_back("HLT_j360_a10_sub_L1J100");
    //indiv_trigger_keys.push_back("HLT_j360_a10_lcw_sub_L1J100");
    //indiv_trigger_keys.push_back("HLT_j460_a10_lcw_sub_L1J100");

    indiv_trigger_keys.push_back("HLT_ht850_L1J75");
    indiv_trigger_keys.push_back("HLT_ht700_L1J75");

    indiv_trigger_keys.push_back("HLT_ht850_L1J100");
    indiv_trigger_keys.push_back("HLT_ht700_L1J100");
    //indiv_trigger_keys.push_back("HLT_j360_a10r_L1J100");

    indiv_trigger_keys.push_back("HLT_j55");
    //indiv_trigger_keys.push_back("HLT_j55_L1RD0_FILLED");

    indiv_trigger_keys.push_back("HLT_j60");
    indiv_trigger_keys.push_back("HLT_j60_280eta320");
    indiv_trigger_keys.push_back("HLT_j60_320eta490");
    indiv_trigger_keys.push_back("HLT_j60_L1RD0_FILLED");

    indiv_trigger_keys.push_back("HLT_j85");
    indiv_trigger_keys.push_back("HLT_j85_280eta320");
    //indiv_trigger_keys.push_back("HLT_j85_280eta320_jes");
    //indiv_trigger_keys.push_back("HLT_j85_280eta320_lcw");
    //indiv_trigger_keys.push_back("HLT_j85_280eta320_lcw_jes");
    indiv_trigger_keys.push_back("HLT_j85_320eta490");
    //indiv_trigger_keys.push_back("HLT_j85_L1RD0_FILLED");
    //indiv_trigger_keys.push_back("HLT_j85_jes");
    //indiv_trigger_keys.push_back("HLT_j85_lcw");
    //indiv_trigger_keys.push_back("HLT_j85_lcw_jes");
    //indiv_trigger_keys.push_back("HLT_j85_lcw_nojcalib");
    //indiv_trigger_keys.push_back("HLT_j85_nojcalib");

    indiv_trigger_keys.push_back("HLT_mistimemonl1bccorr");
    indiv_trigger_keys.push_back("HLT_mistimemonl1bccorrnomu");
    indiv_trigger_keys.push_back("HLT_mistimemoncaltimenomu");
    indiv_trigger_keys.push_back("HLT_mistimemonj400");

    //Single electron
    indiv_trigger_keys.push_back("HLT_e24_lhmedium_L1EM18VH");
    indiv_trigger_keys.push_back("HLT_e24_lhmedium_L1EM20VH");
    indiv_trigger_keys.push_back("HLT_e60_lhmedium");
    indiv_trigger_keys.push_back("HLT_e120_lhloose");
     //Single muon
    indiv_trigger_keys.push_back("HLT_mu20_iloose_L1MU15");
    indiv_trigger_keys.push_back("HLT_mu26_imedium");
    indiv_trigger_keys.push_back("HLT_mu50");
    //Single Photon
    indiv_trigger_keys.push_back("HLT_g140_loose");
    indiv_trigger_keys.push_back("HLT_g160_loose");
    indiv_trigger_keys.push_back("HLT_g120_tight");

 
    indiv_trigger_keys.push_back("HLT_mu24_ivarmedium");
    indiv_trigger_keys.push_back("HLT_mu26_ivarmedium");
    indiv_trigger_keys.push_back("HLT_e24_lhtight_nod0_ivarloose");
    indiv_trigger_keys.push_back("HLT_e26_lhtight_ivarloose");
    indiv_trigger_keys.push_back("HLT_e26_lhtight_nod0_ivarloose");
    indiv_trigger_keys.push_back("HLT_e60_lhmedium_nod0");
    indiv_trigger_keys.push_back("HLT_e60_medium");
    indiv_trigger_keys.push_back("HLT_e120_lhloose_nod0");
    indiv_trigger_keys.push_back("HLT_e140_lhloose_nod0");


    /*indiv_trigger_keys.push_back("HLT_j300");
    indiv_trigger_keys.push_back("HLT_j320");
    indiv_trigger_keys.push_back("HLT_j360");
    indiv_trigger_keys.push_back("HLT_j380");
    indiv_trigger_keys.push_back("HLT_j400");
    indiv_trigger_keys.push_back("HLT_j420");
    indiv_trigger_keys.push_back("HLT_j440");
    indiv_trigger_keys.push_back("HLT_j480");*/

    reset();
}

Analysis::Candidate::~Candidate() {
}

void Analysis::Candidate::reset() {
    // convention:
    //   - set to 0 variables which have a meaningful default value
    //   - set to -9999 the rest

    isMC    = -9999;
    is13TeV = -9999;
    run     = -9999;
    event   = -9999;
    lbn     = -9999;
    bcid    = -9999;
    mu      = -9999;
    trigger = -9999;
    PRWHash = -9999;
    bib = 0;
    bib_flag = -9999;

    trig_HLT_j360_a4tchad = 0;
    trig_HLT_j_xe = 0;
    trig_HLT_1j = 0;
    trig_L1_1j = 0;


    trig_L1_lep = 0;
    trig_th_L1_1e = 0;
    trig_th_L1_2e = 0;
    trig_th_L1_1mu = 0;
    trig_th_L1_2mu = 0;

    trig_HLT_lep = 0;
    trig_th_HLT_1e = 0;
    trig_th_HLT_2e = 0;
    trig_th_HLT_1mu = 0;
    trig_th_HLT_2mu = 0;


    trig_n_HLT_1j = 0;
    trig_th_HLT_1j.clear();
    trig_ps_HLT_1j.clear();
    trig_n_L1_1J = 0;
    trig_th_L1_1J.clear();
    trig_ps_L1_1J.clear();

    hfor = -9999;
    n_vx = 0;
    n_ph = 0;
    n_jet = 0;
    //n_pf_jet = 0;
    n_monojet = 0;
    n_good_jet = 0;
    n_baseline_jet = 0;
    n_goodel = 0;
    n_goodmu = 0;
    met = -9999;
    sumet = -9999;
    met_sig = -9999;
    met_phi = -9999;

    averageIntPerXing = -9999;

    pu_weight = 1;
    ps_weight = 1;
    ps2_weight = 1;
    pu_weight_J12 = 1;
    pu_weight_J50 = 1;
    pu_weight_J12vx = 1;
    pu_weight_J50vx = 1;
    btag_weight = 1;
    vxz_weight = 1;
    overlap_weight = 1;
    event_weight = 1;

    mumu_m = -9999;
    mumu_pt = -9999;
    mumu_eta = -9999;
    mumu_phi = -9999;
    ee_m = -9999;
    ee_pt = -9999;
    ee_eta = -9999;
    ee_phi = -9999;

    jet_dphi_leadsublead = -9999;
    jet_lc_dphi_leadsublead = -9999;
    mht = 0;
    mhtjvt = 0;
    mht_phi = -9999;
    mhtjvt_phi = -9999;
    jet_dphi_leadmht = -9999;
    jet_dphi_leadmhtjvt = -9999;

    passVxCut = 0;
    passMCCut = 0;
    passGRLCut = 0;
    isGoodJet = false;
    isBadJet = false;
    isHardJet = false;
    jet_isSpike.clear();
    jet_lc_isSpike.clear();

    jet_true_pt = -9999;
    jet_true_eta = -9999;
    jet_true_phi = -9999;

    for (unsigned int i = 0; i < JetCleanVarf_keys.size(); i++) {
        JetCleanVarf["jet_a4lc_" + JetCleanVarf_keys[i]].clear();
        JetCleanVarf["jet_a4em_" + JetCleanVarf_keys[i]].clear();
        JetCleanVarf["jet_a4pf_" + JetCleanVarf_keys[i]].clear();
    }
    for (unsigned int i = 0; i < JetCleanVari_keys.size(); i++) {
        JetCleanVari["jet_a4lc_" + JetCleanVari_keys[i]].clear();
        JetCleanVari["jet_a4em_" + JetCleanVari_keys[i]].clear();
        JetCleanVari["jet_a4pf_" + JetCleanVari_keys[i]].clear();
    }
    for (unsigned int i = 0; i < JetSegmentVarf_keys.size(); i++) {
        JetSegmentVarf["jet_a4lc_" + JetSegmentVarf_keys[i]].clear();
        JetSegmentVarf["jet_a4em_" + JetSegmentVarf_keys[i]].clear();
        JetSegmentVarf["jet_a4pf_" + JetSegmentVarf_keys[i]].clear();
    }
    for (unsigned int i = 0; i < JetSegmentVari_keys.size(); i++) {
        JetSegmentVari["jet_a4lc_" + JetSegmentVari_keys[i]].clear();
        JetSegmentVari["jet_a4em_" + JetSegmentVari_keys[i]].clear();
        JetSegmentVari["jet_a4pf_" + JetSegmentVari_keys[i]].clear();
    }
    for (unsigned int i = 0; i < JetKinematics_keys.size(); i++) {
        JetKinematics["jet_a4lc_" + JetKinematics_keys[i]].clear();
        JetKinematics["jet_a4em_" + JetKinematics_keys[i]].clear();
        JetKinematics["jet_a4pf_" + JetKinematics_keys[i]].clear();
    }

    for (unsigned int i = 0; i < LepKinematics_keys.size(); i++) {
        LepKinematics["mu_" + LepKinematics_keys[i]].clear();
        LepKinematics["el_" + LepKinematics_keys[i]].clear();
    }
    
    for (unsigned int i = 0; i < indiv_trigger_keys.size(); i++) {
        indiv_triggers[indiv_trigger_keys[i]] = 0;
        indiv_triggers_ps[indiv_trigger_keys[i]] = 0;
        indiv_triggers_psFIX[indiv_trigger_keys[i]] = 0;
    }



    jet_weight.clear();
    jet_pt.clear();
    jet_eta.clear();
    jet_phi.clear();
    jet_raw_pt.clear();
    jet_raw_eta.clear();
    jet_raw_phi.clear();
    jet_m.clear();
    jet_dphi_met.clear();
    jet_e.clear();
    jet_tag.clear();
    jet_met_dphi.clear();
    jet_timing.clear();
    jet_emfrac.clear();
    jet_hecf.clear();
    jet_hecq.clear();
    jet_fch.clear();
    jet_larq.clear();
    jet_avglarq.clear();
    jet_fmax.clear();
    jet_fmaxi.clear();
    jet_nege.clear();
    jet_clust1_pt.clear();
    jet_clust1_phi.clear();
    jet_clust1_eta.clear();
    jet_lambda.clear();
    jet_lambda2.clear();
    jet_jvtxf.clear();
    jet_clust_n.clear();
    jet_clust_sumpt.clear();
    jet_SV1plusIP3D_discriminant.clear();

    jet_lc_weight.clear();
    jet_lc_pt.clear();
    jet_lc_eta.clear();
    jet_lc_phi.clear();
    jet_lc_raw_pt.clear();
    jet_lc_raw_eta.clear();
    jet_lc_raw_phi.clear();
    jet_lc_m.clear();
    jet_lc_dphi_met.clear();
    jet_lc_e.clear();
    jet_lc_tag.clear();
    jet_lc_met_dphi.clear();
    jet_lc_timing.clear();
    jet_lc_emfrac.clear();
    jet_lc_hecf.clear();
    jet_lc_hecq.clear();
    jet_lc_fch.clear();
    jet_lc_larq.clear();
    jet_lc_avglarq.clear();
    jet_lc_fmax.clear();
    jet_lc_fmaxi.clear();
    jet_lc_nege.clear();
    jet_lc_clust1_pt.clear();
    jet_lc_clust1_phi.clear();
    jet_lc_clust1_eta.clear();
    jet_lc_lambda.clear();
    jet_lc_lambda2.clear();
    jet_lc_jvtxf.clear();
    jet_lc_clust_n.clear();
    jet_lc_clust_sumpt.clear();
    jet_lc_SV1plusIP3D_discriminant.clear();

    jet_pf_pt.clear();
    jet_pf_eta.clear();
    jet_pf_phi.clear();
    jet_pf_raw_pt.clear();
    jet_pf_raw_eta.clear();
    jet_pf_raw_phi.clear();
    jet_pf_m.clear();
    jet_pf_dphi_met.clear();
    jet_pf_e.clear();
    jet_pf_tag.clear();
    jet_pf_met_dphi.clear();
    jet_pf_timing.clear();
    jet_pf_emfrac.clear();
    jet_pf_hecf.clear();
    jet_pf_hecq.clear();
    jet_pf_fch.clear();
    jet_pf_larq.clear();
    jet_pf_avglarq.clear();
    jet_pf_fmax.clear();
    jet_pf_fmaxi.clear();
    jet_pf_nege.clear();
    jet_pf_clust1_pt.clear();
    jet_pf_clust1_phi.clear();
    jet_pf_clust1_eta.clear();
    jet_pf_lambda.clear();
    jet_pf_lambda2.clear();
    jet_pf_jvtxf.clear();
    jet_pf_clust_n.clear();
    jet_pf_clust_sumpt.clear();
    jet_pf_SV1plusIP3D_discriminant.clear();

    n_clust = 0;
    clust_eta.clear();
    clust_phi.clear();
    clust_e.clear();

}

void Analysis::Candidate::attachToTree(TTree *tree) {

    tree->Branch("isMC", &isMC, "isMC/I");
    tree->Branch("is13TeV", &is13TeV, "is13TeV/I");
    tree->Branch("run", &run, "run/I");
    tree->Branch("event", &event, "event/I");
    tree->Branch("lbn", &lbn, "lbn/I");
    tree->Branch("bcid", &bcid, "bcid/I");
    tree->Branch("mu", &mu, "mu/I");
    tree->Branch("passVxCut", &passVxCut, "passVxCut/I");
    tree->Branch("passMCCut", &passMCCut, "passMCCut/I");
    tree->Branch("passGRLCut", &passGRLCut, "passGRLCut/I");
    tree->Branch("bib", &bib, "bib/I");
    tree->Branch("bib_flag", &bib_flag, "bib_flag/I");

    tree->Branch("PRWHash", &PRWHash, "PRWHash/l");


    for (unsigned int i = 0; i < indiv_trigger_keys.size(); i++) {
        tree->Branch("trig_" + indiv_trigger_keys[i], &(indiv_triggers[indiv_trigger_keys[i]]), "trig_" + indiv_trigger_keys[i] + "/I");
        tree->Branch("trig_ps_" + indiv_trigger_keys[i], &(indiv_triggers_ps[indiv_trigger_keys[i]]), "trig_ps_" + indiv_trigger_keys[i] + "/F");
        tree->Branch("trig_psFIX_" + indiv_trigger_keys[i], &(indiv_triggers_psFIX[indiv_trigger_keys[i]]), "trig_psFIX_" + indiv_trigger_keys[i] + "/F");
    }

    tree->Branch("trig_n_HLT_1j",  &trig_n_HLT_1j,   "trig_n_HLT_1j/I");
    tree->Branch("trig_th_HLT_1j", &trig_th_HLT_1j);
    tree->Branch("trig_ps_HLT_1j", &trig_ps_HLT_1j);
    tree->Branch("trig_n_L1_1J",  &trig_n_L1_1J,   "trig_n_L1_1J/I");
    tree->Branch("trig_th_L1_1J", &trig_th_L1_1J);
    tree->Branch("trig_ps_L1_1J", &trig_ps_L1_1J);

    //lepton triggers
    //
    tree->Branch("trig_pass_L1_lep", &trig_L1_lep,   "trig_L1_lep/I");
    tree->Branch("trig_th_L1_1e",   &trig_th_L1_1e, "trig_th_L1_1e/I");
    tree->Branch("trig_th_L1_2e",   &trig_th_L1_2e, "trig_th_L1_2e/I");
    tree->Branch("trig_th_L1_1mu",  &trig_th_L1_1mu, "trig_th_L1_1mu/I");
    tree->Branch("trig_th_L1_2mu",  &trig_th_L1_2mu, "trig_th_L1_2mu/I");

    tree->Branch("trig_pass_HLT_lep", &trig_HLT_lep,   "trig_HLT_lep/I");
    tree->Branch("trig_th_HLT_1e",    &trig_th_HLT_1e, "trig_th_HLT_1e/I");
    tree->Branch("trig_th_HLT_2e",    &trig_th_HLT_2e, "trig_th_HLT_2e/I");
    tree->Branch("trig_th_HLT_1mu",   &trig_th_HLT_1mu, "trig_th_HLT_1mu/I");
    tree->Branch("trig_th_HLT_2mu",   &trig_th_HLT_2mu, "trig_th_HLT_2mu/I");


    tree->Branch("hfor", &hfor, "hfor/I");
    tree->Branch("n_vx", &n_vx, "n_vx/I");
    tree->Branch("n_jet", &n_jet, "n_jet/I");
    //tree->Branch("n_pf_jet", &n_pf_jet, "n_pf_jet/I");
    tree->Branch("n_monojet", &n_monojet, "n_monojet/I");
    tree->Branch("n_good_jet", &n_good_jet, "n_good_jet/I");
    tree->Branch("n_baseline_jet", &n_baseline_jet, "n_baseline_jet/I");
    tree->Branch("n_goodel", &n_goodel, "n_goodel/I");
    tree->Branch("n_goodmu", &n_goodmu, "n_goodmu/I");
    tree->Branch("met", &met, "met/F");
    tree->Branch("sumet", &sumet, "sumet/F");
    tree->Branch("met_sig", &met_sig, "met_sig/F");
    tree->Branch("met_phi", &met_phi, "met_phi/F");

    tree->Branch("mumu_m",   &mumu_m,   "mumu_m/F");
    tree->Branch("mumu_pt",  &mumu_pt,  "mumu_pt/F");
    tree->Branch("mumu_eta", &mumu_eta, "mumu_eta/F");
    tree->Branch("mumu_phi", &mumu_phi, "mumu_phi/F");
    tree->Branch("ee_m",   &ee_m,   "ee_m/F");
    tree->Branch("ee_pt",  &ee_pt,  "ee_pt/F");
    tree->Branch("ee_eta", &ee_eta, "ee_eta/F");
    tree->Branch("ee_phi", &ee_phi, "ee_phi/F");

    tree->Branch("averageIntPerXing", &averageIntPerXing, "averageIntPerXing/F");

    tree->Branch("pu_weight", &pu_weight, "pu_weight/F");
    tree->Branch("ps_weight", &ps_weight, "ps_weight/F");
    tree->Branch("ps2_weight", &ps2_weight, "ps2_weight/F");
    // tree->Branch("pu_weight_J12", &pu_weight_J12, "pu_weight_J12/F");
    //tree->Branch("pu_weight_J50", &pu_weight_J50, "pu_weight_J50/F");
    //tree->Branch("pu_weight_J12vx", &pu_weight_J12vx, "pu_weight_J12vx/F");
    //tree->Branch("pu_weight_J50vx", &pu_weight_J50vx, "pu_weight_J50vx/F");
    tree->Branch("btag_weight", &btag_weight, "btag_weight/F");
    tree->Branch("vxz_weight", &vxz_weight, "vxz_weight/F");
    tree->Branch("overlap_weight", &overlap_weight, "overlap_weight/F");
    tree->Branch("event_weight", &event_weight, "event_weight/F");

    tree->Branch("jet_a4em_dphi_leadsublead", &jet_dphi_leadsublead, "jet_a4em_dphi_leadsublead/F");
    tree->Branch("jet_a4lc_dphi_leadsublead", &jet_lc_dphi_leadsublead, "jet_a4lc_dphi_leadsublead/F");
    tree->Branch("mht", &mht, "mht/F");
    tree->Branch("mhtjvt", &mhtjvt, "mhtjvt/F");
    tree->Branch("mht_phi", &mht_phi, "mht_phi/F");
    tree->Branch("mhtjvt_phi", &mhtjvt_phi, "mhtjvt_phi/F");
    tree->Branch("jet_a4em_dphi_leadmht", &jet_dphi_leadmht, "jet_a4em_dphi_leadmht/F");
    tree->Branch("jet_a4em_dphi_leadmhtjvt", &jet_dphi_leadmhtjvt, "jet_a4em_dphi_leadmhtjvt/F");

    /*tree->Branch("n_clust", &n_clust, "n_clust/I");
    tree->Branch("clust_eta", &clust_eta);
    tree->Branch("clust_phi", &clust_phi);
    tree->Branch("clust_e", &clust_e);*/
    tree->Branch("jet_true_pt", &jet_true_pt, "jet_true_pt/F");
    tree->Branch("jet_true_eta", &jet_true_eta, "jet_true_eta/F");
    tree->Branch("jet_true_phi", &jet_true_phi, "jet_true_phi/F");

    fillJetCollection(tree, "a4em");
    if (m_fillLCJets) fillJetCollection(tree, "a4lc");
    if (m_fillPFJets) fillJetCollection(tree, "a4pf");
    if (m_fillLeptons) fillLeptons(tree);
}

void Analysis::Candidate::fillLeptons(TTree *tree) {
   for (unsigned int i = 0; i < LepKinematics_keys.size(); i++) {
        TString key = "mu_" + LepKinematics_keys[i];
        tree->Branch(key, &LepKinematics[key]);
        TString key2 = "el_" + LepKinematics_keys[i];
        tree->Branch(key2, &LepKinematics[key2]);
        TString key3 = "ph_" + LepKinematics_keys[i];
        tree->Branch(key3, &LepKinematics[key3]);
    }
}

void Analysis::Candidate::fillJetCollection(TTree *tree, TString col) {
    for (unsigned int i = 0; i < JetCleanVarf_keys.size(); i++) {
        TString key = "jet_" + col + "_" + JetCleanVarf_keys[i];
        tree->Branch(key, &JetCleanVarf[key]);
    }
    for (unsigned int i = 0; i < JetCleanVari_keys.size(); i++) {
        TString key = "jet_" + col + "_" + JetCleanVari_keys[i];
        tree->Branch(key, &JetCleanVari[key]);
    }
    for (unsigned int i = 0; i < JetKinematics_keys.size(); i++) {
        TString key = "jet_" + col + "_" + JetKinematics_keys[i];
        tree->Branch(key, &JetKinematics[key]);
    }
    if (m_doJetSegments) {
        for (unsigned int i = 0; i < JetSegmentVarf_keys.size(); i++) {
            TString key = "jet_" + col + "_" + JetSegmentVarf_keys[i];
            tree->Branch(key, &(JetSegmentVarf[key]));
        }
        for (unsigned int i = 0; i < JetSegmentVari_keys.size(); i++) {
            TString key = "jet_" + col + "_" + JetSegmentVari_keys[i];
            tree->Branch(key, &(JetSegmentVari[key]));
        }
    }

}

void Analysis::Candidate::setLCJets(Bool_t val) {
    m_fillLCJets = val;
}

void Analysis::Candidate::setJetSegments(Bool_t val) {
    m_doJetSegments = val;
}
Bool_t Analysis::Candidate::getJetSegments() {
    return m_doJetSegments;
}
