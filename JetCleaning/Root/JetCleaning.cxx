#include <AsgTools/MessageCheck.h>
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include "EventLoop/OutputStream.h"
#include "EventLoopAlgs/NTupleSvc.h"

#include <JetCleaning/JetCleaning.h>

#include <xAODEventInfo/EventInfo.h>
//#include <xAODJet/JetContainer.h>
//#include <xAODTracking/VertexContainer.h>
#include <xAODCore/ShallowCopy.h>
#include <xAODCutFlow/CutBookkeeper.h>
#include <xAODCutFlow/CutBookkeeperContainer.h>
#include <xAODMissingET/MissingETAuxContainer.h>
#include <xAODRootAccess/Init.h>
#include <xAODRootAccess/TEvent.h>
#include "xAODRootAccess/TStore.h"

#include <SUSYTools/SUSYObjDef_xAOD.h>
#include <PathResolver/PathResolver.h>
// utils
#include "CxxUtils/fpcompare.h"

// ROOT generic
#include <TSystem.h>
#include <TTree.h>
#include <TTreeFormula.h>
#include <TH1F.h>
#include <TFile.h>

#include <TSystem.h>


// this is needed to distribute the algorithm to the workers
ClassImp(JetCleaning)

// convenience methods (not class members)
Bool_t addOriginalObjectLink(xAOD::IParticle *particle, const xAOD::IParticleContainer *target_cont);
Bool_t comparePt(const xAOD::IParticle *a, const xAOD::IParticle *b);

JetCleaning :: JetCleaning () : 
  m_event(0), 
  m_ntupleSvc(0),
  m_histoEventCount(0),  
  m_grl ("GoodRunsListSelectionTool/grl", this),
  m_jetCleaning ("JetCleaningTool/JetCleaning", this),
  m_trigConfigTool("TrigConf::xAODConfigTool"), // gives us access to the meta-data
  //m_trigDecisionTool("Trig::TrigDecisionTool"),
  m_emjet_objTool("ST::SUSYObjDef_xAOD"),
  m_pfjet_objTool("ST::SUSYObjDef_xAOD"),
  m_eventInfo(0), 
  m_muons(0),
  m_allMuons(0),
  m_goodMuons(0),
  m_electrons(0),
  m_allElectrons(0),
  m_goodElectrons(0),
  m_photons(0),
  m_allPhotons(0),
  m_goodPhotons(0),
  //m_lc_jets(0),
  m_pf_jets(0),
  m_em_jets(0),
  m_tru_jets(0), 
  m_allJets(0),
  m_allLCJets(0),
  m_allPFJets(0),
  m_allTruJets(0),
  m_goodJets(0){
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().	
}

EL::StatusCode JetCleaning :: setupJob (EL::Job& job){	
	// let's initialize the algorithm to use the xAODRootAccess package
	job.useXAOD ();
        return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetCleaning :: histInitialize (){
	// Here you do everything that needs to be done at the very
	// beginning on each worker node, e.g. create histograms and output
	// trees.  This method gets called before any input files are
	// connected.
	ANA_MSG_INFO("Initializing event count histogram (derivation-proof)");
	m_histoEventCount = new TH1F("histoEventCount", "event count (derivation-proof, only MC weight if any)", 100, 0, 100);
	m_histoEventCount->Fill("initial_weighted", 0);
	m_histoEventCount->Fill("initial_raw", 0);
	wk()->addOutput(m_histoEventCount);
	return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetCleaning :: fileExecute (){
	// Here you do everything that needs to be done exactly once for every
	// single file, e.g. collect a list of all lumi-blocks processed
	xAOD::TEvent *event = wk()->xaodEvent();	

	TTree *MetaData = dynamic_cast<TTree*>(wk()->inputFile()->Get("MetaData"));
	if (!MetaData) {
		ANA_MSG_INFO("Metadata not found!");
		return EL::StatusCode::FAILURE;
	}
	MetaData->LoadTree(0);

	const Bool_t isDerivation = !MetaData->GetBranch("StreamAOD");
	bool testData = (wk()->metaData()->castString("isData") == "YES");

	if (isDerivation && (!testData) && m_isMC) {
		// look for actual info
		const xAOD::CutBookkeeperContainer *completeCBK = nullptr;
		if (!event->retrieveMetaInput(completeCBK, "CutBookkeepers").isSuccess()) {
			ANA_MSG_INFO("Failed to retrieve CutBookkeepers from MetaData");
			return EL::StatusCode::FAILURE;
		}
		const xAOD::CutBookkeeper *allEventsCBK = nullptr;

		int maxCycle = -1;
		for (auto cbk : *completeCBK) {
			if (cbk->inputStream() == "StreamAOD" && cbk->name() == "AllExecutedEvents" && cbk->cycle() > maxCycle) {
				maxCycle = cbk->cycle();
				allEventsCBK = cbk;
			}
		}

		// if exists, read info
		if (allEventsCBK) {
			const uint64_t n_processed = allEventsCBK->nAcceptedEvents();
			const Double_t sum_weights = allEventsCBK->sumOfEventWeights();
			//const Double_t sum_weights_squared = allEventsCBK->sumOfEventWeightsSquared();

			m_histoEventCount->Fill("initial_weighted", sum_weights);
			m_histoEventCount->Fill("initial_raw", n_processed);
		}
	} // is derivation

	return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetCleaning :: changeInput (bool /*firstFile*/){
	// Here you do everything you need to do when we change input files,
	// e.g. resetting branch addresses on trees.  If you are using
	// D3PDReader or a similar service this method is not needed.
	return EL::StatusCode::SUCCESS;
}

//TODO
//EL::StatusCode JetCleaning :: configure_purw(ST::SUSYObjDef_xAOD & objTool){
	//ANA_CHECK( objTool.setProperty("DataSource", datasource) ) ;
	//std::vector<std::string> prw_conf;
	//std::string confFile = PathResolverFindCalibFile("JetCleaning/data/pileup/purw_config_mc15c.root");
	//std::cout << "PathResolver PURW conf file: " << confFile << std::endl;
	//prw_conf.push_back(confFile);
	//prw_conf.push_back("/afs/cern.ch/work/j/jgonski/Harvard/cmakeTests/522_jetCleaning_3/source/JetCleaning/data/pileup/purw_config_mc15c.root"); 
	//prw_conf.push_back("$ROOTCOREBIN/data/JetCleaning/pileup/purw_config_mc15c.root");
	//CHECK(objTool.setProperty("PRWConfigFiles", prw_conf));
	//CHECK(objTool.setProperty("PRWDefaultChannel", 410000)); //361000));
	//std::vector<std::string> prw_lumicalc;
	//prw_lumicalc.push_back("$ROOTCOREBIN/data/JetCleaning/pileup/ilumicalc_histograms_None_276262-280614.root");
	//prw_lumicalc.push_back(PathResolverFindCalibFile("/afs/cern.ch/work/j/jgonski/Harvard/cmakeTests/522_jetCleaning_3/source/JetCleaning/data/pileup/ilumicalc_histograms_None_276262-284484.root"));
	//if(!m_isMC) 
	//prw_lumicalc.push_back(PathResolverFindDataFile("JetCleaning/data/pileup/ilumicalc_histograms_None_276262-284154_IBLOFF.root"));
	//CHECK(objTool.setProperty("PRWLumiCalcFiles", prw_lumicalc));
	//return EL::StatusCode::SUCCESS;
//}

EL::StatusCode JetCleaning :: initialize (){
	// Here you do everything that you need to do after the first input
	// file has been connected and before the first event is processed,
	// e.g. create additional histograms based on which variables are
	// available in the input files.  You can also create all of your
	// histograms and trees in here, but be aware that this method
	// doesn't get called if no events are processed.  So any objects
	// you create here won't be available in the output if you have no
	// input events.

	useLeptons = false;
	useTruJets = false;
	useLCJets = false;
	usePFJets = true;
	//isData = (wk()->metaData()->castString("isData") == "YES");
	//isDeriv = (wk()->metaData()->castString("isDeriv") == "YES");
	//isJETM1 = (wk()->metaData()->castString("isJETM1") == "YES");
	//isEXOT2 = (wk()->metaData()->castString("isEXOT2") == "YES");
	//isEXOT5 = (wk()->metaData()->castString("isEXOT5") == "YES");
	isData = true; 
	isDeriv = true;
	isJETM1 = true;
	isEXOT2 = false;
	isEXOT5 = false;
	useMET = !isJETM1 && isDeriv; // TODO: remove patch when ATLASRECTS-2315 is fixed

	// check if trigger info exists by reading metadata
	hasTrigger = false;
	TTree *metadata = dynamic_cast<TTree*>(wk()->inputFile()->Get("MetaData"));
	if (metadata && metadata->GetBranch("TriggerMenu") != NULL) {
		//metadata->Print();
		hasTrigger = true;
	}

	m_event = wk()->xaodEvent();

	////////////////////////
	// Counters
	////////////////////////
	m_eventCounter = 0;
	ANA_MSG_INFO("Number of events = " << m_event->getEntries());  // print long long int


	////////////////////////
	// Trigger
	////////////////////////
	// Initialize and configure trigger tools
	ANA_CHECK (m_trigConfigTool.initialize());
	//ANA_CHECK (m_trigDecisionTool.setProperty ("ConfigTool", m_trigConfigTool.getHandle())); // connect the TrigDecisionTool to the ConfigTool
	//ANA_CHECK (m_trigDecisionTool.setProperty ("TrigDecisionKey", "xTrigDecision"));
	//ANA_CHECK (m_trigDecisionTool.initialize());


	////////////////////////
	// GRL
	////////////////////////
	//const char* GRLFilePath = "$ALRB_TutorialData/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml";
	//const char* fullGRLFilePath = gSystem->ExpandPathName (GRLFilePath);
	std::vector<std::string> vecStringGRL;
	//vecStringGRL.push_back(fullGRLFilePath);
	 //vecStringGRL.push_back((PathResolverFindCalibFile("GoodRunsLists/data15_13TeV/20160720/physics_25ns_20.7.xml")));
	//vecStringGRL.push_back((PathResolverFindCalibFile("JetCleaning/data/grl/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml")));
	//vecStringGRL.push_back((PathResolverFindDataFile("JetCleaning/data/grl/data17_13TeV.periodAllYear_DetStatus-v96-pro21-12_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml")));
	//vecStringGRL.push_back((PathResolverFindCalibFile("GoodRunsLists/data17_13TeV/20171103/physics_25ns_Triggerno17e33prim.xml")));
	vecStringGRL.push_back((PathResolverFindCalibFile("GoodRunsLists/data18_13TeV/20180518/data18_13TeV.periodAllYear_HEAD_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml")));

        ANA_CHECK(m_grl.setProperty( "GoodRunsListVec", vecStringGRL));
	ANA_CHECK(m_grl.setProperty("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
	ANA_CHECK(m_grl.initialize());
	

	// initialize and configure the jet cleaning tool
	ANA_CHECK (m_jetCleaning.setProperty( "CutLevel", "LooseBad"));
	ANA_CHECK (m_jetCleaning.setProperty("DoUgly", false));
	ANA_CHECK (m_jetCleaning.initialize());

	////////////////////////
	// ST Jet Objects
	////////////////////////
	std::string config_file = PathResolverFindDataFile("JetCleaning/SUSYTools_JetCleaning.config");
	std::cout << "PathResolver find Config file !" << config_file << std::endl;
	ANA_CHECK(m_emjet_objTool.setProperty("DataSource", ST::ISUSYObjDef_xAODTool::Data));
	if(!m_isMC)     ANA_CHECK(m_emjet_objTool->setProperty("DataSource", ST::ISUSYObjDef_xAODTool::Data));
	//else if(isAFII) ANA_CHECK(m_emjet_objTool->setProperty("DataSource", ST::ISUSYObjDef_xAODTool::AtlfastII));
	//else             ANA_CHECK(m_emjet_objTool->setProperty("DataSource", ST::ISUSYObjDef_xAODTool::FullSim));
	if(!config_file.empty())
		ANA_CHECK( m_emjet_objTool.setProperty("ConfigFile", config_file) );
	//ANA_CHECK (m_emjet_objTool.initialize());
	
	std::string config_file_pf = PathResolverFindDataFile("JetCleaning/SUSYTools_JetCleaning_pf.config");
	ANA_CHECK(m_pfjet_objTool.setProperty("DataSource", ST::ISUSYObjDef_xAODTool::Data));
	if(!m_isMC)     ANA_CHECK(m_pfjet_objTool->setProperty("DataSource", ST::ISUSYObjDef_xAODTool::Data));
	//else if(isAFII) ANA_CHECK(m_pfjet_objTool->setProperty("DataSource", ST::ISUSYObjDef_xAODTool::AtlfastII));
	//else             ANA_CHECK(m_pfjet_objTool->setProperty("DataSource", ST::ISUSYObjDef_xAODTool::FullSim));
	if(!config_file.empty())
		ANA_CHECK( m_pfjet_objTool.setProperty("ConfigFile", config_file) );
	//ANA_CHECK (m_pfjet_objTool.initialize());

	////////////////////////
	// Output tree
	////////////////////////
	m_ntupleSvc = EL::getNTupleSvc(wk(), "minitrees");
	//m_cand.setJetSegments(useSegments);
	//m_cand.setJetSegments(useLCJets);
	m_cand.attachToTree(m_ntupleSvc->tree());


	return EL::StatusCode::SUCCESS;

}



EL::StatusCode JetCleaning :: execute (){
	// Here you do everything that needs to be done on every single
	// events, e.g. read input variables, apply cuts, and fill
	// histograms and trees.  This is where most of your actual analysis
	// code will go.

	//----------------------------
	// Event information
	//--------------------------- 
	m_eventInfo = 0;
	ANA_CHECK(evtStore()->retrieve( m_eventInfo, "EventInfo"));  
	ANA_CHECK_SET_TYPE (EL::StatusCode);

 	// print out run and event number from retrieved object
	ANA_MSG_DEBUG ("in execute, runNumber = " << m_eventInfo->runNumber() << ", eventNumber = " << m_eventInfo->eventNumber());		
	if ((m_eventCounter % 200) == 0) ANA_MSG_INFO("Event number = " << m_eventCounter);
	m_eventCounter++;

        // check if the event is data or MC
	// (many tools are applied either to data or MC)
	m_isMC = false;
	// check if the event is MC
	if (m_eventInfo->eventType (xAOD::EventInfo::IS_SIMULATION)) {
		m_isMC = true; // can do something with this later
	}

	MonoJetCuts::CutID last;

	//examine the HLT_j* chains, see if they passed/failed and their total prescale
	//ANA_MSG_INFO("Starting trig loop!");
        //auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_j400");
	//std::map<std::string,int> triggerCounts;
	//for(auto &trig : chainGroup->getListOfTriggers()) {
	//	auto cg = m_trigDecisionTool->getChainGroup(trig);
	//	std::string thisTrig = trig;
	//	ANA_MSG_INFO ("execute(): " << thisTrig << ", chain passed(1)/failed(0) = " << cg->isPassed() << ", total chain prescale (L1*HLT) = " << cg->getPrescale());
	//} // end for loop (C++11 style) over chain group matching "HLT_j*" 

	//retrieve objects, apply GRL, basic event cleaning, etc
	if (getLastCutPassed(last) != EL::StatusCode::SUCCESS) {
		ANA_MSG_INFO("getLastCutPassed failed");
		return EL::StatusCode::FAILURE;
	}
        
        
	if (last >= MonoJetCuts::vertex){ //objects have been filled 
        
		ANA_MSG_VERBOSE(" Event passed quality cuts");
        
		//clear output variables
		m_cand.reset();
		ANA_MSG_VERBOSE("ntuple vars reset");
        
		//read trigger info if it exists
		
		bool passTrigger =  hasTrigger && parseEventTriggerInfo();
		//bool passTrigger = true;
		ANA_MSG_VERBOSE("triggers parsed");
        
		//kinematic acceptance in addition to trigger requirement, for debugging/eff checks
		bool passCut = m_allJets->size() >= 1 && (*m_allJets)[0]->pt() / 1000 > 20;
        
		if (passCut && passTrigger) {
			ANA_MSG_VERBOSE("filling output tree...");
			fillOutputTree();
		}
	}

	// clean ups
	//m_store.clear();
	if (useLeptons) {
		SafeDelete(m_my_photons.first);
		SafeDelete(m_my_muons.first);
		SafeDelete(m_my_electrons.first);
		SafeDelete(m_my_photons.second);
		SafeDelete(m_my_muons.second);
		SafeDelete(m_my_electrons.second);
	}
	SafeDelete(m_my_pf_jets.first);
	//SafeDelete(m_my_lc_jets.first);
	SafeDelete(m_my_em_jets.first);
	SafeDelete(m_my_pf_jets.second);
	//SafeDelete(m_my_lc_jets.second);
	SafeDelete(m_my_em_jets.second);
	if (useTruJets) {
		SafeDelete(m_my_tru_jets.first);
		SafeDelete(m_my_tru_jets.second);
	}


	return EL::StatusCode::SUCCESS;
}

///////////////////////////////
//// Trigger Parsing Functions
//////////////////////////////////

bool JetCleaning :: parseEventTriggerInfo() {
	bool passTrigger = false;
	num_jet_triggers = 0;
	jet_trigger_name.clear();
	jet_trigger_index.clear();
	jet_trigger_prescale.clear();

	for (unsigned int i = 0; i < m_cand.indiv_trigger_keys.size(); i++) {
		TString trig = m_cand.indiv_trigger_keys[i];
		if(trig.Contains("HLT")) ANA_MSG_DEBUG("Trigger: " << trig << ", hasTrigger: " << hasTrigger << ", is trig passed: " << m_emjet_objTool->IsTrigPassed(trig.Data()));
		int pass = (hasTrigger) ? m_emjet_objTool->IsTrigPassed(trig.Data()) : 0;
		m_cand.indiv_triggers[trig] = pass;
		//m_cand.indiv_triggers_ps[trig] = (hasTrigger) ? (pass ? m_emjet_objTool->GetTrigPrescale(trig.Data()) : -1) : -1;
		//m_cand.indiv_triggers_psFIX[trig] = m_cand.indiv_triggers_ps[trig];
		//if (abs(m_cand.indiv_triggers_ps[trig]) != 1) m_cand.indiv_triggers_psFIX[trig] = 0xFFFFFF / (0x1000000 -  m_cand.indiv_triggers_ps[trig]);
		if (pass) passTrigger = true;
	}
	return passTrigger;
}

///////////////////////////////
//// Output Tree Funtion
//////////////////////////////////
//
EL::StatusCode JetCleaning :: fillOutputTree() {

	//////////////////
	// event variables
	//////////////////

	//PRW_OFF CHECK(m_emjet_objTool->ApplyPRWTool());
	ANA_MSG_VERBOSE("Defining event variables");
	m_cand.isMC = m_isMC;
	//m_cand.is13TeV = is13TeV;
	//m_cand.run = (m_isMC) ? m_eventInfo->mcChannelNumber() : m_eventInfo->runNumber();
	m_cand.run = m_eventInfo->runNumber();
	m_cand.event = m_eventInfo->eventNumber();
	m_cand.lbn = m_eventInfo->lumiBlock();
	m_cand.bcid = m_eventInfo->bcid();
	m_cand.mu = m_eventInfo->averageInteractionsPerCrossing();
	m_cand.passVxCut = passVxCut;
	m_cand.passGRLCut = passGRLCut;

	ANA_MSG_DEBUG("Event Info: " << m_cand.run << ", mu: " << m_cand.mu);

	m_cand.bib = m_eventInfo->eventFlags(xAOD::EventInfo::Background) & (1 << 20);
	m_cand.bib_flag = m_eventInfo->eventFlags(xAOD::EventInfo::Background);

	//m_cand.averageIntPerXing = m_emjet_objTool->GetCorrectedAverageInteractionsPerCrossing();

	m_cand.hfor = -9999;

	ANA_MSG_VERBOSE(", # object variables");
	m_cand.n_vx = m_vertices->size(); // absolute number of PV's (i.e. no track cut)
	//m_cand.n_jet = m_allJets->size();

	int n_baseline_jets = 0;
	int n_good_jets = 0;
	for (unsigned int i = 0; i < m_jetTag.size(); i++) {
		if (m_jetTag[i] == 2) n_good_jets ++;
		else if (m_jetTag[i] == 1) n_baseline_jets ++;
	}
	m_cand.n_good_jet = n_good_jets;
	m_cand.n_baseline_jet = n_baseline_jets;

	//if(usePFJets) m_cand.n_pf_jet = m_allPFJets->size(); 

	if (useLeptons) {
		m_cand.n_goodel = m_goodElectrons->size(); // counts only veto leptons
		m_cand.n_goodmu = m_goodMuons->size(); // counts only veto leptons
	}

	//m_cand.pu_hash = (m_isMC) ? m_objTool->GetPRWHash() : 9999;
	//m_tool->GetDataWeight( *evtInfo, "TriggerExpression" );
	// TEMP REMOVED
	if (m_isMC) {
		ANA_MSG_VERBOSE(", pileup weights");
		//m_cand.pu_weight = m_cand.pu_weight = m_emjet_objTool->GetPileupWeight();
	}

	m_cand.vxz_weight = 1.0;
	m_cand.overlap_weight = 1.0;
	m_cand.event_weight = 1.0;
	//m_cand.event_weight = (isData) ? 1.0 : m_eventInfo->mcEventWeight();


	/*  if (m_isMC) {
       const xAOD::TruthEventContainer *truthE(nullptr);
       if (!event->retrieve(truthE, "TruthEvents").isSuccess()) {
       Error(APP_NAME, "Failed to retrieve Truth container");
       } else {
       try {
       xAOD::TruthEventContainer::const_iterator truthE_itr = truthE->begin();
       (*truthE_itr)->pdfInfoParameter(m_cand.pdf_id1, xAOD::TruthEvent::PDGID1);
       (*truthE_itr)->pdfInfoParameter(m_cand.pdf_id2, xAOD::TruthEvent::PDGID2);
       (*truthE_itr)->pdfInfoParameter(m_cand.pdf_x1, xAOD::TruthEvent::X1);
       (*truthE_itr)->pdfInfoParameter(m_cand.pdf_x2, xAOD::TruthEvent::X2);
       (*truthE_itr)->pdfInfoParameter(m_cand.pdf_pdf1, xAOD::TruthEvent::PDF1);
       (*truthE_itr)->pdfInfoParameter(m_cand.pdf_pdf2, xAOD::TruthEvent::PDF2);
       (*truthE_itr)->pdfInfoParameter(m_cand.pdf_scale, xAOD::TruthEvent::Q);
       } catch (SG::ExcBadAuxVar) {
	// ignore this variable, when unavailable
	//          // (happens often, see
	//                   // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MC15aKnownIssues
	//                            // )
	}
	}
	
	// truth leading pt
	if (getPhaseSpaceWeight(m_cand) != EL::StatusCode::SUCCESS) {
	Error(APP_NAME, "Error in getPhaseSpaceWeight");
	return EL::StatusCode::FAILURE;
	}
	
	// truth jet information
	// leading two jets are saved; this is needed for example for QCD MC, where
	// events with ((pt1+pt2)/2)/pt1 < 1.4 are ignored, in order to avoid counting
	// pileup jets as leading jets when combining different slices of leading jet pt
	const xAOD::JetContainer *truthJets(nullptr);
	static Bool_t failedLookingFor(kFALSE); // trick to avoid infinite RuntimeWarning's for EXOT5
	if (!failedLookingFor) {
	if (!event->retrieve(truthJets, "AntiKt4TruthJets").isSuccess()) {
	Error(APP_NAME, "Failed to access Truth Jets container; not attempting again, truth_jet* variables will be empty");
	failedLookingFor = kTRUE;
	} else {
	if (truthJets->size() > 0) {
	m_cand.truth_jet1_pt = (*truthJets)[0]->p4().Pt();
	m_cand.truth_jet1_eta = (*truthJets)[0]->p4().Eta();
	m_cand.truth_jet1_phi = (*truthJets)[0]->p4().Phi();
	m_cand.truth_jet1_m = (*truthJets)[0]->p4().M();
	if (truthJets->size() > 1) {
	m_cand.truth_jet2_pt = (*truthJets)[1]->p4().Pt();
	m_cand.truth_jet2_eta = (*truthJets)[1]->p4().Eta();
	m_cand.truth_jet2_phi = (*truthJets)[1]->p4().Phi();
	m_cand.truth_jet2_m = (*truthJets)[1]->p4().M();
	}
	}
	}
	} // already found AntiKt4TruthJets, or never failed yet
	} // isMC
	*/


	//////////////////
	// object variables
	//////////////////
	
	if (useMET) {
		ANA_MSG_VERBOSE(", MET");
		//MET
		float met_nomuon_metx = (*m_met)["Final"]->mpx() / 1000;
		float met_nomuon_mety = (*m_met)["Final"]->mpy() / 1000;
		float sumet = (*m_met)["Final"]->sumet() / 1000;
		float met   = sqrt(met_nomuon_metx * met_nomuon_metx + met_nomuon_mety * met_nomuon_mety);
		float met_phi = atan2(met_nomuon_mety, met_nomuon_metx);
	
		m_cand.met = met;
		m_cand.sumet = sumet;
		m_cand.met_sig = met / sqrt(sumet);
		m_cand.met_phi = met_phi;
	}
	
		
	///////////
	// Leptons
	///////////
	if (useLeptons) {
	//ELECTRONS
		ANA_MSG_VERBOSE(", electrons");
		if (m_goodElectrons->size() > 1) {
			TLorentzVector v_2el = (*m_goodElectrons)[0]->p4() + (*m_goodElectrons)[1]->p4();
			m_cand.ee_pt  = v_2el.Pt() / 1000.;
			m_cand.ee_eta = v_2el.Eta();
			m_cand.ee_phi = v_2el.Phi();
			m_cand.ee_m = v_2el.M() / 1000.;
		} // at least two vetoed electrons

		//MUONS
		ANA_MSG_VERBOSE(", muons");
		if (m_goodMuons->size() > 1) {
			TLorentzVector v_2mu = (*m_goodMuons)[0]->p4() + (*m_goodMuons)[1]->p4();
			m_cand.mumu_pt = v_2mu.Pt() / 1000.;
			m_cand.mumu_eta = v_2mu.Eta();
			m_cand.mumu_phi = v_2mu.Phi();
			m_cand.mumu_m = v_2mu.M() / 1000.;
		} // at least two vetoed muons

		for (unsigned int i = 0; i <  m_allPhotons->size(); i++) {
			fillPhoton((*m_allPhotons)[i], "ph_");
		}
		for (unsigned int i = 0; i <  m_allElectrons->size(); i++) {
			fillElectron((*m_allElectrons)[i], "el_");
		}
		for (unsigned int i = 0; i <  m_allMuons->size(); i++) {
			fillMuon((*m_allMuons)[i], "mu_");
		}
	}


	///////////
	// JETSSSSS
	///////////
	ANA_MSG_VERBOSE(", JETS (our ");
	//MC Cleaning cut
	//For lower slices this cut prevents the leading jets from being pileup jets
	m_cand.passMCCut = 0;
	if (isData) m_cand.passMCCut = 1;
	else if (useTruJets && m_allTruJets->size() >= 1) {
		ANA_MSG_VERBOSE("true ");
		m_cand.jet_true_pt  = (*m_allTruJets)[0]->pt() / 1000;
		m_cand.jet_true_eta = (*m_allTruJets)[0]->eta();
		m_cand.jet_true_phi = (*m_allTruJets)[0]->phi();
		if (m_allJets->size() >= 2) {
			float avgpt = ((*m_allJets)[0]->pt() + (*m_allJets)[1]->pt()) / 2.0;
			if (avgpt / (*m_allTruJets)[0]->pt() < 1.4)
				m_cand.passMCCut = 1;
			else m_cand.passMCCut = 0;
		} else m_cand.passMCCut = 1;
	}
	ANA_MSG_VERBOSE("favorite)");

	if (m_allJets->size() >= 2) {
		m_cand.jet_dphi_leadsublead = TMath::ACos(TMath::Cos((*m_allJets)[0]->phi() - (*m_allJets)[1]->phi()));
	}

	//if (m_allLCJets->size() >= 2)
	//	m_cand.jet_lc_dphi_leadsublead = TMath::ACos(TMath::Cos((*m_allLCJets)[0]->phi() - (*m_allLCJets)[1]->phi()));


	//OLD: mht studies with jvt jets only 
	//float mhtx = 0;
	//float mhtx_jvt = 0;
	//float mhty = 0;
	//float mhty_jvt = 0;

	for (unsigned int i = 0; i < m_allJets->size(); i++) {
		if ((*m_allJets)[i]->pt() / 1000 > 20) {
			m_cand.n_jet++;
			fillJet((*m_allJets)[i], i, "jet_a4em_");
			//if (i < m_allLCJets->size()) fillJet((*m_allLCJets)[i], i, "jet_a4lc_");
			//mhtx += -(*m_allJets)[i]->px() / 1000;
			//mhty += -(*m_allJets)[i]->py() / 1000;
			if ((*m_allJets)[i]->pt() / 1000 > 30 && fabs((*m_allJets)[i]->eta()) < 2.8) m_cand.n_monojet++;

			if((*m_allJets)[i]->pt() / 1000 < 50 && fabs((*m_allJets)[i]->eta())  < 2.4){
				static SG::AuxElement::ConstAccessor<float> acc_jvt("Jvt");
				//if(acc_jvt(*(*m_allJets)[i])){
				//	mhtx_jvt += -(*m_allJets)[i]->px() / 1000;
				//	mhty_jvt += -(*m_allJets)[i]->py() / 1000;
				//}
			}
			//else{
			//	mhtx_jvt += -(*m_allJets)[i]->px() / 1000;
			//	mhty_jvt += -(*m_allJets)[i]->py() / 1000;
			//}


		}
	}
	
	//if (useLCJets)
	//	for (unsigned int i = 0; i <  m_allLCJets->size(); i++) {
	//		if ((*m_allLCJets)[i]->pt() / 1000 > 20)
	//			fillJet((*m_allLCJets)[i], i, "jet_a4lc_");
	//	}
	if (usePFJets)
		for (unsigned int i = 0; i <  m_allPFJets->size(); i++) {
			if ((*m_allPFJets)[i]->pt() / 1000 > 20){
				fillJet((*m_allPFJets)[i], i, "jet_a4pf_");
			}
		}


	//m_cand.mht = sqrt(mhtx * mhtx + mhty * mhty);
	//m_cand.mhtjvt = sqrt(mhtx_jvt * mhtx_jvt + mhty_jvt * mhty_jvt);
	//m_cand.mht_phi = atan2(mhty, mhtx);
	//m_cand.mhtjvt_phi = atan2(mhty_jvt, mhtx_jvt);
	//if (m_allJets->size() >= 1) {
	//	m_cand.jet_dphi_leadmht = TMath::ACos(TMath::Cos((*m_allJets)[0]->phi() - m_cand.mht_phi));
	//}
	//if (m_allJets->size() >= 1) {
	//	m_cand.jet_dphi_leadmhtjvt = TMath::ACos(TMath::Cos((*m_allJets)[0]->phi() - m_cand.mhtjvt_phi));
	//}


	//OLD: cluster studies
	//ANA_MSG_VERBOSE(", and clusters...");
	//if (useClusters) {
	//	if (m_verbose) std::std::cout << "looking into clusters again" << std::std::endl;
	//	if (m_verbose) std::std::cout << "using " << m_my_clusters.first << std::std::endl;
	//	xAOD::CaloClusterContainer::iterator c_itr = (m_my_clusters.first)->begin();
	//	xAOD::CaloClusterContainer::iterator c_end = (m_my_clusters.first)->end();
	//	for (; c_itr != c_end; ++c_itr) {
	//		if (m_verbose) std::std::cout << "  a new one, " << *c_itr << std::std::endl;
	//		if (m_verbose) std::std::cout << "    eta" << std::std::endl;
	//		float eta = (*c_itr)->eta() ;
	//		if (m_verbose) std::std::cout << "    phi" << std::std::endl;
	//		float phi = (*c_itr)->phi() ;
	//		if (m_verbose) std::std::cout << "    e" << std::std::endl;
	//		float e = (*c_itr)->e() ;
        //
	//		m_cand.n_clust ++;
	//		m_cand.clust_eta.push_back(eta);
	//		m_cand.clust_phi.push_back(phi);
	//		m_cand.clust_e.push_back(e);
	//		std::cout << "Cluster eta: " << eta << ", phi: " << phi << ", e: " << e << std::endl;
	//		if (m_verbose) std::std::cout << "    -> saved" << std::std::endl;
	//	}
	//}


	///////////
	// Skimming
	///////////

	if(doSkim && (isEXOT2||isJETM1) && isData){    //typical dijet selection
		bool isDijet = (m_allJets->size() >= 2  && (fabs((*m_allJets)[1]->pt() - (*m_allJets)[0]->pt()) / ((*m_allJets)[1]->pt() + (*m_allJets)[0]->pt()) < 0.3) && TMath::ACos(TMath::Cos((*m_allJets)[0]->phi() - (*m_allJets)[1]->phi()))  > 3.);
		bool isTrig = m_cand.indiv_triggers["HLT_j360"] ;
		bool isTrig2 = m_cand.indiv_triggers["HLT_j400"] ;
		bool isFake = m_allJets->size() >= 1 &&  m_cand.mht > 70; // && fabs(m_cand.JetCleanVarf["jet_a4em_timing"][0]) > 6;
		bool saveMe = isDijet || isTrig || isTrig2 || isFake;
		if(saveMe) m_ntupleSvc->setFilterPassed();
	}
	else if(doSkim && isEXOT5 && isData){   //typical fake selection
		bool isTrig = m_cand.indiv_triggers["HLT_j360"] ;
		bool isTrig2 = m_cand.indiv_triggers["HLT_j400"] ;
		//bool isFake = m_allJets->size() >= 1 &&  m_cand.mht > 70 && (m_cand.indiv_triggers["HLT_xe100"] || m_cand.indiv_triggers["HLT_xe80"] ||
		bool isFake = m_allJets->size() >= 1 &&  m_cand.mht > 70 &&  (m_cand.indiv_triggers["HLT_xe100_mht_L1XE50"] || m_cand.indiv_triggers["HLT_xe110_mht_L1XE50"]);
		bool saveMe = isTrig || isTrig2 ||isTrig2 ||  isFake;
		if(saveMe) m_ntupleSvc->setFilterPassed();
	}
	/*else if(doSkim && isEXOT5 && isData){    //lepton and photon skimming
	//bool hasLepton = m_allMuons->size() >=1 || m_allElectrons->size() >= 1;
	bool isTrig = m_cand.indiv_triggers["HLT_mu24_ivarmedium"] || m_cand.indiv_triggers["HLT_mu26_ivarmedium"] || m_cand.indiv_triggers["HLT_e24_lhtight_nod0_ivarloose"] || m_cand.indiv_triggers["HLT_e26_lhtight_ivarloose"] || m_cand.indiv_triggers["HLT_e26_lhtight_nod0_ivarloose"] || m_cand.indiv_triggers["HLT_e60_lhmedium_nod0"] || m_cand.indiv_triggers["HLT_e60_medium"] || m_cand.indiv_triggers["HLT_e120_lhloose_nod0"] || m_cand.indiv_triggers["HLT_e140_lhloose_nod0"] || m_cand.indiv_triggers["HLT_g140_loose"] || m_cand.indiv_triggers["HLT_g160_loose"] ||m_cand.indiv_triggers["HLT_g120_tight"] ; 
	if(isTrig) m_ntupleSvc->setFilterPassed();
	}*/
	else m_ntupleSvc->setFilterPassed();


    return EL::StatusCode::SUCCESS;

}




EL::StatusCode JetCleaning :: postExecute (){
	// Here you do everything that needs to be done after the main event
	// processing.  This is typically very rare, particularly in user
	// code.  It is mainly used in implementing the NTupleSvc.
	return EL::StatusCode::SUCCESS;
}



EL::StatusCode JetCleaning :: finalize (){
	// This method is the mirror image of initialize(), meaning it gets
	// called after the last event has been processed on the worker node
	// and allows you to finish up any objects you created in
	// initialize() before they are written to disk.  This is actually
	// fairly rare, since this happens separately for each worker node.
	// Most of the time you want to do your post-processing on the
	// submission node after all your histogram outputs have been
	// merged.  This is different from histFinalize() in that it only
	// gets called on worker nodes that processed input events.

	//SafeDelete(m_grl);
	//if (usePFJets) delete(m_pfjet_objTool);    
	//if (useLCJets) delete(m_lcjet_objTool);    
	//delete(m_emjet_objTool);
	
        //m_trigDecisionTool->finalize();

	ANA_MSG_INFO ("finalize(): Number of clean events = " << m_numCleanEvents);

	return EL::StatusCode::SUCCESS;
}



EL::StatusCode JetCleaning :: histFinalize (){
	// This method is the mirror image of histInitialize(), meaning it
	// gets called after the last event has been processed on the worker
	// node and allows you to finish up any objects you created in
	// histInitialize() before they are written to disk.  This is
	// actually fairly rare, since this happens separately for each
	// worker node.  Most of the time you want to do your
	// post-processing on the submission node after all your histogram
	// outputs have been merged.  This is different from finalize() in
	// that it gets called on all worker nodes regardless of whether
	// they processed input events.
	return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetCleaning::getObjects() {
	xAOD::MuonContainer::iterator mu_itr;
	xAOD::MuonContainer::iterator mu_end;
	xAOD::PhotonContainer::iterator ph_itr;
	xAOD::PhotonContainer::iterator ph_end;
	xAOD::ElectronContainer::iterator el_itr;
	xAOD::ElectronContainer::iterator el_end;
	
	if(useLeptons){
		///////////
		// Muons
		///////////
		m_muons = 0;
		m_allMuons = new xAOD::MuonContainer(SG::VIEW_ELEMENTS);
		m_goodMuons = new xAOD::MuonContainer(SG::VIEW_ELEMENTS);
		ANA_CHECK(evtStore()->record (m_allMuons, "MyAllMuons"));
		ANA_CHECK(evtStore()->record (m_goodMuons, "MyGoodMuons"));

		if (!m_event->retrieve(m_muons, "Muons").isSuccess()) {    // retrieve arguments: container type, container key
			ANA_MSG_INFO("Failed to retrieve Muons container");
			return EL::StatusCode::FAILURE;
		}

		m_my_muons = xAOD::shallowCopyContainer(*m_muons);
		//ANA_CHECK(evtStore()->record (m_my_muons, "MyMuonsAux"));
		mu_itr = (m_my_muons.first)->begin();
		mu_end  = (m_my_muons.first)->end();

		for (; mu_itr != mu_end; ++mu_itr) {

			ANA_CHECK(m_emjet_objTool->FillMuon(**mu_itr, 10000, 2.7));
			m_emjet_objTool->IsSignalMuon(**mu_itr, 25000, 3., 0.5); //  // d0, z0 significance cuts, high pt selection
			m_emjet_objTool->IsCosmicMuon(**mu_itr, 1., 0.2);
			//ANA_CHECK(m_objTool->GetMuons(m_allMuons, m_my_muons, kFALSE, "Muons")); 

			if (!addOriginalObjectLink((*mu_itr), m_muons)) {
				ANA_MSG_INFO("Input muon not part of a container, 'originalObjectLink' ElementLink not established");
			}

			//static SG::AuxElement::Accessor<char> acc_baseline("baseline");
			//if (acc_baseline(**mu_itr) == 1) {
			//    m_allMuons->push_back(*mu_itr);
			//} // baseline muon
			static SG::AuxElement::Accessor<char> acc_signal("baseline");
			if (acc_signal(**mu_itr) == 1) {
				m_allMuons->push_back(*mu_itr);
			} // baseline muon

		} // loop over muons

		m_allMuons->sort(&comparePt); // sort only this (cannot sort my_muons, no need to sort m_goodMuons afterwards)


		///////////
		// Photons
		///////////
		m_photons = 0;
		m_allPhotons = new xAOD::PhotonContainer(SG::VIEW_ELEMENTS);
		m_goodPhotons = new xAOD::PhotonContainer(SG::VIEW_ELEMENTS);
		//CHANGE BACK
		ANA_CHECK(evtStore()->record (m_allPhotons, "MyAllPhotons"));
		ANA_CHECK(evtStore()->record (m_goodPhotons, "MyGoodPhotons"));

		if (!m_event->retrieve(m_photons, "Photons").isSuccess()) {    // retrieve arguments: container type, container key
			ANA_MSG_INFO("Failed to retrieve Photons container");
			return EL::StatusCode::FAILURE;
		}
		m_my_photons = xAOD::shallowCopyContainer(*m_photons);
		//ANA_CHECK(evtStore()->record (m_my_photons, "MyPhotonsAux"));
		ph_itr = (m_my_photons.first)->begin();
		ph_end  = (m_my_photons.first)->end();

		for (; ph_itr != ph_end; ++ph_itr) {

			ANA_CHECK(m_emjet_objTool->FillPhoton(**ph_itr, 20000, 2.47));
			m_emjet_objTool->IsSignalPhoton(**ph_itr, 25000, 5);

			if (!addOriginalObjectLink((*ph_itr), m_photons)) {
				ANA_MSG_INFO("Input photon not part of a container, 'originalObjectLink' ElementLink not established");
			}

			// static SG::AuxElement::Accessor<char> acc_baseline("baseline");
			//if (acc_baseline(**el_itr) == 1) {
			//    m_allElectrons->push_back(*el_itr);
			//}
			static SG::AuxElement::Accessor<char> acc_signal("baseline");
			if (acc_signal(**ph_itr) == 1) {
				ANA_MSG_VERBOSE("signal photon found");
				m_allPhotons->push_back(*ph_itr);
			}
		} // loop over photons

		m_allPhotons->sort(&comparePt); //FIX for mc14_8TeV

		///////////
		// Electrons
		///////////
		m_electrons = 0;
		m_allElectrons = new xAOD::ElectronContainer(SG::VIEW_ELEMENTS);
		m_goodElectrons = new xAOD::ElectronContainer(SG::VIEW_ELEMENTS);
		//CHANGE BACK
		ANA_CHECK(evtStore()->record (m_allElectrons, "MyAllElectrons"));
		ANA_CHECK(evtStore()->record (m_goodElectrons, "MyGoodElectrons"));

		if (!m_event->retrieve(m_electrons, "Electrons").isSuccess()) {    // retrieve arguments: container type, container key
			ANA_MSG_INFO("Failed to retrieve Electrons container");
			return EL::StatusCode::FAILURE;
		}
		m_my_electrons = xAOD::shallowCopyContainer(*m_electrons);
		//ANA_CHECK(evtStore()->record (m_my_electrons, "MyElectronsAux"));
		el_itr = (m_my_electrons.first)->begin();
		el_end  = (m_my_electrons.first)->end();

		for (; el_itr != el_end; ++el_itr) {

			ANA_CHECK(m_emjet_objTool->FillElectron(**el_itr, 10000, 2.47));
			m_emjet_objTool->IsSignalElectron(**el_itr, 25000, 5, 0.5);

			if (!addOriginalObjectLink((*el_itr), m_electrons)) {
				ANA_MSG_INFO("Input electron not part of a container, 'originalObjectLink' ElementLink not established");
			}

			// static SG::AuxElement::Accessor<char> acc_baseline("baseline");
			//if (acc_baseline(**el_itr) == 1) {
			//    m_allElectrons->push_back(*el_itr);
			//}
			static SG::AuxElement::Accessor<char> acc_signal("baseline");
			if (acc_signal(**el_itr) == 1) {
				ANA_MSG_VERBOSE("signal electron found");
				m_allElectrons->push_back(*el_itr);
			}
		} // loop over electrons

		m_allElectrons->sort(&comparePt); //FIX for mc14_8TeV
	}//useLeptons 


	///////////
	// Jets
	///////////
	//m_lc_jets = nullptr;
	m_em_jets = nullptr;
	m_pf_jets = nullptr;
	m_allJets = new xAOD::JetContainer(SG::VIEW_ELEMENTS);
	//m_allLCJets = new xAOD::JetContainer(SG::VIEW_ELEMENTS);
	m_allPFJets = new xAOD::JetContainer(SG::VIEW_ELEMENTS);
	m_goodJets = new xAOD::JetContainer(SG::VIEW_ELEMENTS);

	m_jetTag.clear();
	m_em_jetTag.clear();
	ANA_CHECK (evtStore()->record (m_allJets, "MyAllJets"));
	//ANA_CHECK (evtStore()->record (m_allLCJets, "MyAllLCJets"));
	ANA_CHECK (evtStore()->record (m_allPFJets, "MyAllPFJets"));
	ANA_CHECK (evtStore()->record (m_goodJets, "MyGoodJets"));


	if (!m_event->retrieve(m_em_jets, "AntiKt4EMTopoJets").isSuccess()) {    // retrieve arguments: container type, container key
		ANA_MSG_INFO("Failed to retrieve AntiKt4EMTopo Jet container");
		return EL::StatusCode::FAILURE;
	}
	//if (useLCJets && !m_event->retrieve(m_lc_jets, "AntiKt4LCTopoJets").isSuccess()) {    // retrieve arguments: container type, container key
	//	ANA_MSG_INFO("Failed to retrieve AntiKt4LCTopo Jet container");
	//	useLCJets = false;
	//	//return EL::StatusCode::FAILURE;
	//}
	if (usePFJets && !m_event->retrieve(m_pf_jets, "AntiKt4EMPFlowJets").isSuccess()) {    // retrieve arguments: container type, container key
		ANA_MSG_INFO("Failed to retrieve AntiKt4EMPFlow Jet container");
		usePFJets = false;
		//return EL::StatusCode::FAILURE;
	}
	
	m_my_em_jets = xAOD::shallowCopyContainer(*m_em_jets);
	//if (useLCJets)m_my_lc_jets = xAOD::shallowCopyContainer(*m_lc_jets);
	if (usePFJets)m_my_pf_jets = xAOD::shallowCopyContainer(*m_pf_jets);
	//ANA_CHECK(evtStore()->record (m_my_em_jets, "MyEmJetsAux"));
	//ANA_CHECK(evtStore()->record (m_my_pf_jets, "MyPfJetsAux"));

	////Fill EM Jets
	xAOD::JetContainer::iterator jet_itr = (m_my_em_jets.first)->begin();
	xAOD::JetContainer::iterator jet_end = (m_my_em_jets.first)->end();

	for (; jet_itr != jet_end; ++jet_itr) {

		//ANA_CHECK(m_emjet_objTool->FillJet(**jet_itr));
		//m_emjet_objTool->IsSignalJet(**jet_itr, 20000, 2.8);
		//m_emjet_objTool->IsBJet(**jet_itr);

		if (!addOriginalObjectLink((*jet_itr), m_em_jets)){
			ANA_MSG_INFO("Input jet not part of a container, 'originalObjectLink' ElementLink not established");
		}

		//static SG::AuxElement::Accessor<char> acc_baseline("baseline");
		// if (acc_baseline(**jet_itr) == 1){
		m_allJets->push_back(*jet_itr);
	}

	m_allJets->sort(&comparePt);

	// fill LC jets
	//if (useLCJets) {
	//	jet_itr = (m_my_lc_jets.first)->begin();
	//	jet_end = (m_my_lc_jets.first)->end();
	//	for (; jet_itr != jet_end; ++jet_itr) {

	//		ANA_CHECK(m_lcjet_objTool->FillJet(**jet_itr));
	//		m_lcjet_objTool->IsSignalJet(**jet_itr, 20000, 2.8);
	//		//m_lcjet_objTool->IsBJet(**jet_itr);

	//		if (!addOriginalObjectLink((*jet_itr), m_lc_jets)) {
	//			ANA_MSG_INFO("Input jet not part of a container, 'originalObjectLink' ElementLink not established");
	//		}

	//		//static SG::AuxElement::Accessor<char> acc_baseline("baseline");
	//		// if (acc_baseline(**jet_itr) == 1){
	//		m_allLCJets->push_back(*jet_itr);
	//	}
	//	m_allLCJets->sort(&comparePt);
	//}

	// fill PF jets
	if (usePFJets) {
		jet_itr = (m_my_pf_jets.first)->begin();
		jet_end = (m_my_pf_jets.first)->end();
		for (; jet_itr != jet_end; ++jet_itr) {
	
			ANA_CHECK(m_pfjet_objTool->FillJet(**jet_itr));
			m_pfjet_objTool->IsSignalJet(**jet_itr, 20000, 2.8);
			//m_pfjet_objTool->IsBJet(**jet_itr);
	
			if (!addOriginalObjectLink((*jet_itr), m_pf_jets)) {
				ANA_MSG_INFO("Input jet not part of a container, 'originalObjectLink' ElementLink not established");
			}
	
			//static SG::AuxElement::Accessor<char> acc_baseline("baseline");
			// if (acc_baseline(**jet_itr) == 1){
			m_allPFJets->push_back(*jet_itr);
		}
		m_allPFJets->sort(&comparePt);
	}

	///////////
	// Truth Jets
	///////////
	if (!isData && useTruJets) {
		m_tru_jets = 0;
		m_allTruJets = new xAOD::JetContainer(SG::VIEW_ELEMENTS);
		ANA_CHECK(evtStore()->record (m_allTruJets, "MyAllTruJets"));
		if (!isJETM1 && !m_event->retrieve(m_tru_jets, "AntiKt4TruthJets").isSuccess()) {    // retrieve arguments: container type, container key
			ANA_MSG_INFO("Failed to retrieve Jets container");
			return EL::StatusCode::FAILURE;
		}

		m_my_tru_jets = xAOD::shallowCopyContainer(*m_tru_jets);
		//ANA_CHECK(evtStore()->record (m_my_tru_jets, "MyTruJetsAux"));
		jet_itr = (m_my_tru_jets.first)->begin();
		jet_end  = (m_my_tru_jets.first)->end();

		for (; jet_itr != jet_end; ++jet_itr) {
			static SG::AuxElement::Accessor<char> acc_baseline("baseline");
			if ((*jet_itr)->pt() > 20000 && TMath::Abs((*jet_itr)->eta()) < 2.8) {
				m_allTruJets->push_back(*jet_itr);
			} // baseline jet
		} // loop over jets

		m_allTruJets->sort(&comparePt);
	}


	///////////
	// Overlap removal
	///////////
	//const Bool_t doHarmo(kFALSE);
	if (useLeptons) ANA_CHECK(m_emjet_objTool->OverlapRemoval(m_my_electrons.first, m_my_muons.first, m_my_em_jets.first, m_my_photons.first));

	// good jet selection
	jet_itr = m_allJets->begin();
	jet_end = m_allJets->end();

	static SG::AuxElement::Accessor< char > acc_passOR("passOR");
	static SG::AuxElement::Accessor< char > acc_bad("bad");
	for (; jet_itr != jet_end; ++jet_itr) {
		// ask good jets to be baseline jets (i.e. those considered for overlap removal)
		// but with pT > 30 GeV, |eta| < 2.8
		if (acc_passOR(**jet_itr) == 1  &&
				acc_bad(**jet_itr) == 0 &&
				(*jet_itr)->pt() > 30000.  && (TMath::Abs((*jet_itr)->eta()) < 2.8)) {
			m_goodJets->push_back(*jet_itr);
		}
		static SG::AuxElement::Accessor<char> acc_baseline("baseline");

		if (acc_passOR(**jet_itr) == 1  && acc_bad(**jet_itr) == 0) m_jetTag.push_back(2);
		else if (acc_baseline(**jet_itr) == 1) m_jetTag.push_back(1);
		else m_jetTag.push_back(0);

	}

	if (useLeptons) {
		// good muon selection (i.e. muons for veto)
		mu_itr = m_allMuons->begin();
		mu_end = m_allMuons->end();

		for (; mu_itr != mu_end; ++mu_itr) {
			static SG::AuxElement::Accessor< char > acc_passOR("passOR");
			if (acc_passOR(**mu_itr) == 1) {
				m_goodMuons->push_back(*mu_itr);
			}
		}

		// good electron selection (i.e. electrons for veto)
		ph_itr = m_allPhotons->begin();
		ph_end = m_allPhotons->end();

		for (; ph_itr != ph_end; ++ph_itr) {
			static SG::AuxElement::Accessor< char > acc_passOR("passOR");
			if (acc_passOR(**ph_itr) == 1) {
				m_goodPhotons->push_back(*ph_itr);
			}
		}

		// good electron selection (i.e. electrons for veto)
		el_itr = m_allElectrons->begin();
		el_end = m_allElectrons->end();

		for (; el_itr != el_end; ++el_itr) {
			static SG::AuxElement::Accessor< char > acc_passOR("passOR");
			if (acc_passOR(**el_itr) == 1) {
				m_goodElectrons->push_back(*el_itr);
			}
		}
	}

	///////////
	// MET
	///////////
	if (useMET) {

		m_met = new xAOD::MissingETContainer;
		m_metAux = new xAOD::MissingETAuxContainer;
		m_met->setStore(m_metAux);
		ANA_CHECK(evtStore()->record (m_met, "MET_MET"));
		ANA_CHECK(evtStore()->record (m_metAux, "MET_METoAux"));

		ANA_CHECK(m_emjet_objTool->GetMET(*m_met,
					m_my_em_jets.first, // use all objects (before OR) for MET utility
					m_my_electrons.first,
					m_my_muons.first,
					0, // photon term
					0,  // tau term
					kTRUE,
					kTRUE,
					0 ));//m_my_muons.first));

		//xAOD::MissingETContainer::const_iterator met_it = m_met->find("Final");
		xAOD::MissingETContainer::const_iterator met_it = m_met->find("Final");

		if (met_it == m_met->end()) {
			ANA_MSG_INFO("No LocHadTopo inside MET container");
			return EL::StatusCode::FAILURE;
		}
	}

    return EL::StatusCode::SUCCESS;
}


EL::StatusCode JetCleaning :: fillPhoton(const xAOD::Photon *thisPhoton, TString prefix) {
    std::map <TString, float> kineVarsf;
    kineVarsf["pt"]  = thisPhoton->pt() / 1000;
    kineVarsf["eta"] = thisPhoton->eta();
    kineVarsf["phi"] = thisPhoton->phi();
    kineVarsf["m"] = thisPhoton->m() / 1000;
    kineVarsf["e"] = thisPhoton->e() / 1000;
    for (unsigned int j = 0; j < m_cand.LepKinematics_keys.size(); j++) {
        TString key = m_cand.LepKinematics_keys[j];
        if (kineVarsf.count(key) == 1 && m_cand.LepKinematics.count(prefix + key) == 1)
            m_cand.LepKinematics[prefix + key].push_back(kineVarsf[key]);
    }

    return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetCleaning :: fillElectron(const xAOD::Electron *thisElectron, TString prefix) {
    std::map <TString, float> kineVarsf;
    kineVarsf["pt"]  = thisElectron->pt() / 1000;
    kineVarsf["eta"] = thisElectron->eta();
    kineVarsf["phi"] = thisElectron->phi();
    kineVarsf["m"] = thisElectron->m() / 1000;
    kineVarsf["e"] = thisElectron->e() / 1000;
    for (unsigned int j = 0; j < m_cand.LepKinematics_keys.size(); j++) {
        TString key = m_cand.LepKinematics_keys[j];
        if (kineVarsf.count(key) == 1 && m_cand.LepKinematics.count(prefix + key) == 1)
            m_cand.LepKinematics[prefix + key].push_back(kineVarsf[key]);
    }

    return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetCleaning :: fillMuon(const xAOD::Muon *thisMuon, TString prefix) {
    std::map <TString, float> kineVarsf;
    kineVarsf["pt"]  = thisMuon->pt() / 1000;
    kineVarsf["eta"] = thisMuon->eta();
    kineVarsf["phi"] = thisMuon->phi();
    kineVarsf["m"] = thisMuon->m() / 1000;
    kineVarsf["e"] = thisMuon->e() / 1000;
    for (unsigned int j = 0; j < m_cand.LepKinematics_keys.size(); j++) {
        TString key = m_cand.LepKinematics_keys[j];
        if (kineVarsf.count(key) == 1 && m_cand.LepKinematics.count(prefix + key) == 1)
            m_cand.LepKinematics[prefix + key].push_back(kineVarsf[key]);
    }

    return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetCleaning :: fillJet(const xAOD::Jet *thisJet, int i, TString prefix) {
	//const xAOD::Jet *thisRawJet = dynamic_cast< const xAOD::Jet* >(xAOD::getOriginalObject(*thisJet));
	//jet kinematics
	std::map <TString, float> kineVarsf;
	kineVarsf["pt"]  = thisJet->pt() / 1000;
	kineVarsf["eta"] = thisJet->eta();
	kineVarsf["phi"] = thisJet->phi();
	//kineVarsf["raw_pt"]  = thisRawJet->pt() / 1000;
	//kineVarsf["raw_eta"] = thisRawJet->eta();
	//kineVarsf["raw_phi"] = thisRawJet->phi();
	kineVarsf["m"] = thisJet->m() / 1000;
	kineVarsf["e"] = thisJet->e() / 1000;
	kineVarsf["dphi_met"]  = -9999;
	kineVarsf["dphi_ee"]   = -9999;
	kineVarsf["dphi_mumu"] = -9999;
	if (useMET) {
		kineVarsf["dphi_met"]  = TMath::ACos(TMath::Cos(thisJet->phi() - m_cand.met_phi));
		kineVarsf["dphi_ee"]   = TMath::ACos(TMath::Cos(thisJet->phi() - m_cand.ee_phi));
		kineVarsf["dphi_mumu"] = TMath::ACos(TMath::Cos(thisJet->phi() - m_cand.mumu_phi));
	}

	m_cand.jet_tag.push_back(m_jetTag[i]);

	for (unsigned int j = 0; j < m_cand.JetKinematics_keys.size(); j++) {
		TString key = m_cand.JetKinematics_keys[j];
		if (kineVarsf.count(key) == 1 && m_cand.JetKinematics.count(prefix + key) == 1)
			m_cand.JetKinematics[prefix + key].push_back(kineVarsf[key]);
	}

	//jet moments/attributes
	std::map <TString, float> cleanVarsf;
	std::map <TString, int> cleanVarsi;

	thisJet->getAttribute(xAOD::JetAttribute::CentroidR,            cleanVarsf["centR"]);
	thisJet->getAttribute(xAOD::JetAttribute::OotFracClusters5,     cleanVarsf["ootfc5"]);
	thisJet->getAttribute(xAOD::JetAttribute::OotFracClusters10,    cleanVarsf["ootfc10"]);
	thisJet->getAttribute(xAOD::JetAttribute::N90Constituents,      cleanVarsf["n90const"]);
	thisJet->getAttribute(xAOD::JetAttribute::Timing,               cleanVarsf["timing"]);
	thisJet->getAttribute(xAOD::JetAttribute::EMFrac,               cleanVarsf["emfrac"]);
	thisJet->getAttribute(xAOD::JetAttribute::HECFrac,              cleanVarsf["hecf"]);
	thisJet->getAttribute(xAOD::JetAttribute::HECQuality,           cleanVarsf["hecq"]);
	thisJet->getAttribute(xAOD::JetAttribute::AverageLArQF,         cleanVarsf["avglarq"]);
	thisJet->getAttribute(xAOD::JetAttribute::LArQuality,           cleanVarsf["larq"]);
	thisJet->getAttribute(xAOD::JetAttribute::FracSamplingMax,      cleanVarsf["fmax"]);
	int fmaxi = 0;
	thisJet->getAttribute(xAOD::JetAttribute::FracSamplingMaxIndex, fmaxi);
	cleanVarsi["fmaxi"] = fmaxi;
	thisJet->getAttribute(xAOD::JetAttribute::NegativeE,            cleanVarsf["nege"]);
	cleanVarsf["nege"] /= 1000;
	cleanVarsf["fch"] = -9999;
	cleanVarsf["ntrk"] = -9999;

	if (fabs(thisJet->eta()) < 2.5) {
		std::vector<Float_t> sumpttrk_vec;
		if (!thisJet->getAttribute<std::vector<float> >("SumPtTrkPt500_IDLoose", sumpttrk_vec))
			thisJet->getAttribute(xAOD::JetAttribute::SumPtTrkPt500, sumpttrk_vec);

		if (sumpttrk_vec.size() > 0) cleanVarsf["fch"] = sumpttrk_vec[0] / thisJet->pt();

		std::vector<int> ntrk_vec;
		if (!thisJet->getAttribute<std::vector<int> >("NumPtTrkPt500_IDLoose", ntrk_vec))
			thisJet->getAttribute(xAOD::JetAttribute::NumTrkPt500, ntrk_vec);
		if (ntrk_vec.size() > 0) cleanVarsf["ntrk"] = ntrk_vec[0];

	}
	//SumPtTrkPt500_IDLoose
	//float ptlead = 0;
	//float intlead = -1;
	cleanVarsf["clust_sumpt"] = 0;
	//cleanVarsf["clust_sumet"] = 0;
	cleanVarsi["clust_n"] = 0;
	//float summedBadLC = 0.0;
	//bool gotBadClus = false;
	//bool spikeJet = false;

	//cluster variables
	//cleanVarsf["lambda"] = -999;
	//cleanVarsf["lambda2"] = -999;
	//cleanVarsf["clust1_pt"] = -999;
	thisJet->getAttribute<float >("LeadingClusterPt", cleanVarsf["clust1_pt"]);
	thisJet->getAttribute<float >("LeadingClusterSecondLambda", cleanVarsf["lambda2"]);
	thisJet->getAttribute<float >("LeadingClusterCenterLambda", cleanVarsf["lambda"]);
	thisJet->getAttribute<float >("LeadingClusterSecondR", cleanVarsf["secondR"]);
	cleanVarsf["clust1_eta"] = -999;
	cleanVarsf["clust1_phi"] = -999;


	// V: JVT info
	ANA_MSG_VERBOSE("JVT time now!");
	static SG::AuxElement::ConstAccessor<float> acc_jvt("Jvt");
	cleanVarsf["jvt"] = acc_jvt(*thisJet);
	static SG::AuxElement::Decorator<char> dec_passJvt("passJvt");
	//static SG::AuxElement::Accessor<char>   acc_passFJvt("passFJVT");    
	cleanVarsf["passJvt"] = dec_passJvt(*thisJet);

	// VI: Save to Candidate
	for (unsigned int j = 0; j < m_cand.JetCleanVarf_keys.size(); j++) {
		TString key = m_cand.JetCleanVarf_keys[j];
		if (cleanVarsf.count(key) == 1 && m_cand.JetCleanVarf.count(prefix + key) == 1)
			m_cand.JetCleanVarf[prefix + key].push_back(cleanVarsf[key]);
	}

	for (unsigned int j = 0; j < m_cand.JetCleanVari_keys.size(); j++) {
		TString key = m_cand.JetCleanVari_keys[j];
		if (cleanVarsi.count(key) == 1 && m_cand.JetCleanVari.count(prefix + key) == 1)
			m_cand.JetCleanVari[prefix + key].push_back(cleanVarsi[key]);
	}


	//if (useClusters) {
	//	if (m_verbose) std::std::cout << "using clusters" << std::std::endl;
	//	xAOD::JetConstituentVector constits = thisJet->getConstituents();
	//	if (m_verbose) std::std::cout << "total constituents " << thisJet->numConstituents() << std::std::endl;
	//	for (size_t j = 0; j < thisJet->numConstituents(); ++j) {
	//		const xAOD::CaloCluster* cluster = dynamic_cast< const xAOD::CaloCluster* >(thisJet->rawConstituent(j));
	//		if (m_verbose) std::std::cout << "  constit #" << j << " has cluster pointer " << cluster << std::std::endl;
	//		if (cluster) {
	//			cleanVarsi["clust_n"] += 1;
	//			cleanVarsf["clust_sumpt"] += constits[j]->pt() / 1000;
	//			//cleanVarsf["clust_sumet"] += constits[j]->et()/1000;
	//			if (constits[j]->pt() > ptlead) {
	//				ptlead = constits[j]->pt();
	//				intlead = j;
	//			}
	//			// determine if the cluster is bad
	//			if (m_verbose) std::std::cout << "    checking PSEndCapEnergy" << std::std::endl;
	//			float PSEndCapEnergy = fabs(cluster->eSample((CaloSampling::CaloSample)4));
	//			if (m_verbose) std::std::cout << "                 value = " << PSEndCapEnergy << std::std::endl;
	//			float totalEnergy = 0;
	//			if (m_verbose) std::std::cout << "    checking totalEnergy" << std::std::endl;
	//			for (int s = 0; s <= 20; s++) {
	//				if (m_verbose) std::std::cout << "    term s=" << s << std::std::endl;
	//				totalEnergy += fabs(cluster->eSample((CaloSampling::CaloSample)s)) ;
	//			}
	//			if (m_verbose) std::std::cout << "                 value = " << totalEnergy << std::std::endl;
	//			if (PSEndCapEnergy / totalEnergy > 0.8) {
	//				gotBadClus = true;
	//				if (m_verbose) std::std::cout << "    bad cluster #" << j << " !" << std::std::endl;
	//				summedBadLC += constits[j]->e();
	//				if (m_verbose) std::std::cout << "      energy summed up" << std::std::endl;
	//			}
	//		}
	//	}

	//	if (m_verbose) std::std::cout << "      intlead=" << intlead << std::std::endl;
	//	if (intlead >= 0) {
	//		const xAOD::CaloCluster* cluster = dynamic_cast< const xAOD::CaloCluster* >(thisJet->rawConstituent(intlead));
	//		if (m_verbose) std::std::cout << "      cluster=" << cluster << std::std::endl;
	//		cleanVarsf["clust1_pt"] = constits[intlead]->pt() / 1000;
	//		cleanVarsf["clust1_eta"] = constits[intlead]->eta();
	//		cleanVarsf["clust1_phi"] = constits[intlead]->phi();
	//		double lambda;
	//		cluster->retrieveMoment(xAOD::CaloCluster::CENTER_LAMBDA, lambda);
	//		double lambda2;
	//		cluster->retrieveMoment(xAOD::CaloCluster::SECOND_LAMBDA, lambda2);
	//		double r2;
	//		cluster->retrieveMoment(xAOD::CaloCluster::SECOND_R, r2);
	//		cleanVarsf["secondR"] = r2;
	//		cleanVarsf["lambda"] = lambda;
	//		cleanVarsf["lambda2"] = lambda2;
	//	}
	//}//useCluster
	//
	//if (m_verbose) std::std::cout << "checking decision" << std::std::endl;
	//if (gotBadClus && (summedBadLC / thisJet->jetP4(xAOD::JetScale::JetConstitScaleMomentum).E()) > 0.1)
	//	spikeJet = true;
	//
	//if (m_verbose) std::std::cout << " updating isSpike to " << spikeJet << std::std::endl;
	//cleanVarsi["isSpike"] = (int)spikeJet;
	//
	//	
	//
	// VI: segment part
	//std::map <TString, std::vector<float>> segmentVarsf;
	//std::map <TString, std::vector<int>> segmentVarsi;

	//if (useSegments) {
	//	static SG::AuxElement::ConstAccessor<int> muonSegCount("GhostMuonSegmentCount");
	//	if (muonSegCount.isAvailable(*thisJet)) {
	//		if (m_verbose) std::std::cout << "segments available" << std::std::endl;
	//		if (m_verbose) std::std::cout << "number is " << muonSegCount(*thisJet) << std::std::endl;
	//		cleanVarsi["segment_n"] = muonSegCount(*thisJet);

	//		std::vector<const xAOD::MuonSegment*> pobjs = thisJet->getAssociatedObjects<xAOD::MuonSegment>("GhostMuonSegment");
	//		if (m_verbose) std::std::cout << "segments are " << pobjs.size() << std::std::endl;

	//		for (auto segment : pobjs) {
	//			if (m_verbose) std::std::cout << "segment with x=" << segment->x() << " y=" << segment->y() << " z=" << segment->z() << std::std::endl;
	//			if (m_verbose) std::std::cout << "             px=" << segment->px() << " py=" << segment->py() << " pz=" << segment->pz() << std::std::endl;
	//			if (m_verbose) std::std::cout << "             sector=" << segment->sector() << " etaIndex=" << segment->etaIndex() << std::std::endl;
	//			if (m_verbose) std::std::cout << "             nPrecisionHits=" <<  segment->nPrecisionHits() << std::std::endl;
	//			if (m_verbose) std::std::cout << "             nPhiLayers=" << segment->nPhiLayers() << " nTrigEtaLayers=" << segment->nTrigEtaLayers() << std::std::endl;
	//			if (m_verbose) std::std::cout << "             t0=" << segment->t0() << " t0error=" << segment->t0error() << std::std::endl;
	//			if (m_verbose) std::std::cout << "             chiSquared=" << segment->chiSquared() << " numberDoF=" << segment->numberDoF() << std::std::endl;

	//			segmentVarsi["segment_sector"].push_back(segment->sector());
	//			segmentVarsi["segment_etaIndex"].push_back(segment->etaIndex());
	//			segmentVarsi["segment_nPrecisionHits"].push_back(segment->nPrecisionHits());
	//			segmentVarsi["segment_nPhiLayers"].push_back(segment->nPhiLayers());
	//			segmentVarsi["segment_nTrigEtaLayers"].push_back(segment->nTrigEtaLayers());

	//			segmentVarsf["segment_x"].push_back(segment->x());
	//			segmentVarsf["segment_y"].push_back(segment->y());
	//			segmentVarsf["segment_z"].push_back(segment->z());
	//			segmentVarsf["segment_px"].push_back(segment->px());
	//			segmentVarsf["segment_py"].push_back(segment->py());
	//			segmentVarsf["segment_pz"].push_back(segment->pz());
	//			segmentVarsf["segment_t0"].push_back(segment->t0());
	//			segmentVarsf["segment_t0error"].push_back(segment->t0error());
	//			segmentVarsf["segment_chiSquared"].push_back(segment->chiSquared());
	//			segmentVarsf["segment_numberDoF"].push_back(segment->numberDoF());
	//		}

	//	} else  {
	//		cleanVarsi["segment_n"] = -9999;
	//		if (m_verbose) std::std::cout << "segments NOT available" << std::std::endl;
	//	}
	//} // add info on segments to saved jets
	//if (useSegments) {
	//    for (unsigned int j = 0; j < m_cand.JetSegmentVarf_keys.size(); j++) {
	//        TString key = m_cand.JetSegmentVarf_keys[j];
	//        if (segmentVarsf.count(key) == 1 && m_cand.JetSegmentVarf.count(prefix + key) == 1)
	//            m_cand.JetSegmentVarf[prefix + key].push_back(segmentVarsf[key]);
	//    }
	//
	//    for (unsigned int j = 0; j < m_cand.JetSegmentVari_keys.size(); j++) {
	//        TString key = m_cand.JetSegmentVari_keys[j];
	//        if (segmentVarsi.count(key) == 1 && m_cand.JetSegmentVari.count(prefix + key) == 1)
	//            m_cand.JetSegmentVari[prefix + key].push_back(segmentVarsi[key]);
	//    }
	//} // segments


	// VII: b-tagging part
	/* static SG::AuxElement::ConstAccessor<int> PartonTruthLabelID("PartonTruthLabelID");
	   static SG::AuxElement::ConstAccessor<int> ConeTruthLabelID("ConeTruthLabelID");
	   static SG::AuxElement::ConstAccessor<float> TruthLabelDeltaR_B("TruthLabelDeltaR_B");
	   static SG::AuxElement::ConstAccessor<float> TruthLabelDeltaR_C("TruthLabelDeltaR_C");
	   static SG::AuxElement::ConstAccessor<float> TruthLabelDeltaR_T("TruthLabelDeltaR_T");
	   if (PartonTruthLabelID.isAvailable(*thisJet)) {
	   if (m_verbose) std::std::cout << "PartonTruthLabelID available" << std::std::endl;
	   if (m_verbose) std::std::cout << "number is " << PartonTruthLabelID(*thisJet) << std::std::endl;
	   if (m_verbose) std::std::cout << "dr_B is " << TruthLabelDeltaR_B(*thisJet) << std::std::endl;
	   if (m_verbose) std::std::cout << "dr_C is " << TruthLabelDeltaR_C(*thisJet) << std::std::endl;
	   if (m_verbose) std::std::cout << "dr_T is " << TruthLabelDeltaR_T(*thisJet) << std::std::endl;
	   cleanVarsi["PartonTruthLabelID"] = PartonTruthLabelID(*thisJet);
	   cleanVarsi["ConeTruthLabelID"] = ConeTruthLabelID(*thisJet);
	   cleanVarsf["TruthLabelDeltaR_B"] = TruthLabelDeltaR_B(*thisJet);
	   cleanVarsf["TruthLabelDeltaR_C"] = TruthLabelDeltaR_C(*thisJet);
	   cleanVarsf["TruthLabelDeltaR_T"] = TruthLabelDeltaR_T(*thisJet);
	   } else {
	   if (m_verbose) std::std::cout << "PartonTruthLabelID NOT available" << std::std::endl;
	   cleanVarsi["PartonTruthLabelID"] = -9999;
	   cleanVarsi["ConeTruthLabelID"] = -9999;
	   }*/

	return EL::StatusCode::SUCCESS;

}

EL::StatusCode JetCleaning::getLastCutPassed(MonoJetCuts::CutID &last) {
	//// GRL

	last = MonoJetCuts::processed;

	passGRLCut = true;   //TODO
	if (!m_isMC) {
		ANA_MSG_VERBOSE(" Run: "  << m_eventInfo->runNumber()
				<< ", Evt: " << m_eventInfo->eventNumber()
				<< ", LBN: " << m_eventInfo->lumiBlock()
				<< ", BCID: " << m_eventInfo->bcid()) ; 
	//	if (!m_grl->passRunLB(*m_eventInfo)) {
	//		ANA_MSG_VERBOSE ("drop event: GRL");
	//		passGRLCut = false;
	//		return EL::StatusCode::SUCCESS; // go to next event
	//	}
	//	else{ last = MonoJetCuts::GRL; 
	//		passGRLCut = true;
	//	}
	} // end if not MC
	else passGRLCut = true;

	ANA_MSG_VERBOSE(" Passed GRL..."); 

	//// cleaning
	if (!m_isMC) {
		if ((m_eventInfo->errorState(xAOD::EventInfo::LAr) == xAOD::EventInfo::Error) ||
				(m_eventInfo->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error) ||
				(m_eventInfo->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error) ||
				(m_eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18))) {
			ANA_MSG_DEBUG("Did not pass cleaning!");
			//return EL::StatusCode::SUCCESS; // go to the next event
		} // end if event flags check
		else {last = MonoJetCuts::cleaning;}
	} // end if the event is data
	ANA_MSG_VERBOSE("Passed Event Cleaning...");
	m_numCleanEvents++;

	//// good vertex
	Bool_t passesVertex(kFALSE);
	const UInt_t nMinTracks(2);
	m_vertices = 0;
	passVxCut = false;

	if (!m_event->retrieve(m_vertices, "PrimaryVertices").isSuccess()) {
		ANA_MSG_INFO("Failed to retrieve PrimaryVertices container");
		//return EL::StatusCode::FAILURE;
	} else {
		xAOD::VertexContainer::const_iterator vx_itr = m_vertices->begin();
		xAOD::VertexContainer::const_iterator vx_end  = m_vertices->end();
		for (; vx_itr != vx_end && !passesVertex; ++vx_itr) {
			if ((UInt_t)((*vx_itr)->nTrackParticles()) >= nMinTracks) {
				passesVertex = kTRUE;
				passVxCut = true;
			}
		}

		if (!passesVertex) {
			ANA_MSG_INFO("Did not pass vertex!");
			//return EL::StatusCode::SUCCESS;
		} else {
			last = MonoJetCuts::vertex;
		}
	}
	ANA_MSG_VERBOSE("Passed primary vertex...");

	// get objects
	if (getObjects() != EL::StatusCode::SUCCESS) {
		ANA_MSG_INFO("Error in getObjects!!");
		return EL::StatusCode::FAILURE;
	}


	return EL::StatusCode::SUCCESS;
}

Bool_t addOriginalObjectLink(xAOD::IParticle *particle, const xAOD::IParticleContainer *target_cont) {
    typedef ElementLink <xAOD::IParticleContainer> Link_t;
    static const char *linkName = "originalObjectLink";
    const xAOD::IParticleContainer* its_cont = dynamic_cast<const xAOD::IParticleContainer*>(particle->container());

    if (!its_cont) {
        return kFALSE; // particle not in container
    } else {
        const Link_t link(*target_cont, particle->index());
        Link_t &auxLink = particle->auxdata<Link_t>(linkName);
        auxLink = link;
        auxLink.toPersistent();
    }

    return kTRUE;
}

Bool_t comparePt(const xAOD::IParticle *a, const xAOD::IParticle *b) {
    return CxxUtils::fpcompare::greater(a->pt(), b->pt());
}

std::map<TString, std::vector<float>> JetCleaning :: compactFillVarsF(std::vector<TString> keys,
                                   std::map<TString, float> vars,
                                   std::map<TString, std::vector<float>> output, TString prefix) {
    for (unsigned int j = 0; j < keys.size(); j++) {
        if (vars.count(keys[j]) == 1 && output.count(prefix + keys[j]) == 1)
            output[prefix + keys[j]].push_back(vars[keys[j]]);
        else if (output.count(prefix + keys[j]) != 1) {
            ANA_MSG_INFO("Variable " + prefix + keys[j] + " not set for outputTree");
        } else if (vars.count(keys[j]) != 1) {
            ANA_MSG_INFO("Variable " + keys[j] + " not initialized");
        }
    }
    return output;
}

std::map<TString, std::vector<int>> JetCleaning :: compactFillVarsI(std::vector<TString> keys,
                                 std::map<TString, int> vars,
                                 std::map<TString, std::vector<int>> output, TString prefix) {
    for (unsigned int j = 0; j < keys.size(); j++) {
        if (vars.count(keys[j]) == 1 && output.count(prefix + keys[j]) == 1)
            output[prefix + keys[j]].push_back(vars[keys[j]]);
        else if (output.count(prefix + keys[j]) != 1) {
            ANA_MSG_INFO("Variable " + prefix + keys[j] + " not set for outputTree");
        } else if (vars.count(keys[j]) != 1) {
            ANA_MSG_INFO("Variable " + keys[j] + " not initialized");
        }
    }
    return output;
}

