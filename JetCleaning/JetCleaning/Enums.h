#ifndef __JetCleaning_Enums_h__
#define __JetCleaning_Enums_h__

namespace MonoJetCuts {
    typedef enum {
        processed = 0,
        GRL,
        cleaning,
        trigger,
        vertex,
        MET_cleaning,
        BCH_cleaning,
        MET_cut,
        Leading_Pt_cut,
        Leading_cleaning,
        electron_veto,
        muon_veto,
        jet_multiplicity,
        jet_MET_overlap,
        MET_cut_2,
        Leading_Pt_cut_2,
        // TODO: add below removing numbers
        track_veto = 11,
        met_leadjet_balance = 12,
        mT_cut = 13,
        third_jet_veto = 14,
    } CutID;
}

namespace MonoJetMuonCuts {
    typedef enum {
        exists = 0,
        family,
        quality,
        eta,
        pt,
        MCP,
        isolated,
    } CutID;
}

namespace MonoJetElectronCuts {
    typedef enum {
        exists = 0,
        family,
        isMedium,
        author,
        kinematics,
        OQ,
    } CutID;
}

#endif
