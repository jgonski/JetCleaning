#ifndef JetCleaning_JetCleaning_H
#define JetCleaning_JetCleaning_H

//event loop
#include <EventLoop/Algorithm.h>
#include <EventLoop/StatusCode.h>
#include <memory>
#include <AsgTools/AnaToolHandle.h>

//root 
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "TRegexp.h"
#include "TH2D.h"
#include <TH1.h>
#include "PATInterfaces/SystematicRegistry.h"

// tools
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include <JetInterface/IJetSelector.h>
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include <TrigDecisionTool/TrigDecisionTool.h>
#include "AsgAnalysisInterfaces/IPileupReweightingTool.h"
#include <SUSYTools/SUSYObjDef_xAOD.h>
#include <PathResolver/PathResolver.h>

//jet cleaning
#include "JetCleaning/Candidate.h"
#include "JetCleaning/Enums.h"

namespace EL {
    class NTupleSvc;
}
namespace ST { 
	class SUSYObjDef_xAOD; 
}

class JetCleaning : public EL::Algorithm
{
	// put your configuration variables here as public variables.
	//   // that way they can be set directly from CINT and python.
	public:
		Bool_t passGRLCut;
		Bool_t useLCJets;
		Bool_t usePFJets;
		Bool_t passVxCut;
		Bool_t useLeptons;
		Bool_t useMET;
		Bool_t useTruJets;
		Bool_t doSkim;
		Bool_t is13TeV;
		Bool_t isData;
		Bool_t isData16;
		Bool_t isJETM1;
		Bool_t isEXOT2;
		Bool_t isEXOT5;
		Bool_t isDeriv;
		TString outputName;
		TString treeName;
		Bool_t hasTrigger;


		// variables that don't get filled at submission time should be
		// protected from being send from the submission node to the worker
		// node (done by the //!)
	public:
		// things used to access xAOD information
		xAOD::TEvent *m_event;  //!
		std::vector<CP::SystematicSet> m_sysList; //!

		// things used for output
		EL::NTupleSvc *m_ntupleSvc; //!
		Analysis::Candidate m_cand; //!	

		// things used in the analysis
		Int_t m_eventCounter; //! 
		Bool_t m_isMC; //! 
		//Analysis::CutFlowTool m_cutFlow; //! 
		TH1F *m_histoEventCount; //!
		int m_numCleanEvents = 0; //!

		// trigger counting
		Int_t num_jet_triggers; //!
		std::vector<TString> jet_trigger_name; //!
		std::vector<int>     jet_trigger_index; //!
		std::vector<int>     jet_trigger_prescale; //!
		Int_t num_jetmet_triggers; //!
		std::vector<TString> jetmet_trigger_name; //!
		std::vector<int>     jetmet_trigger_index; //!
		std::vector<int>     jetmet_trigger_prescale; //!
		//<TString,JetObject> m_triggers;


		//tool handles
		asg::AnaToolHandle<IGoodRunsListSelectionTool> m_grl; //!
		asg::AnaToolHandle<IJetSelector> m_jetCleaning; //!  
		asg::AnaToolHandle<TrigConf::ITrigConfigTool> m_trigConfigTool; //!
		//asg::AnaToolHandle<Trig::TrigDecisionTool> m_trigDecisionTool; //!
		asg::AnaToolHandle<ST::SUSYObjDef_xAOD> m_emjet_objTool; //!
		asg::AnaToolHandle<ST::SUSYObjDef_xAOD> m_pfjet_objTool; //!

		// methods used in the analysis
		virtual EL::StatusCode getObjects();
		virtual EL::StatusCode getLastCutPassed(MonoJetCuts::CutID &last);
		virtual EL::StatusCode fillOutputTree();
		virtual bool parseEventTriggerInfo();
		//virtual EL::StatusCode configure_purw(ST::SUSYObjDef_xAOD & objTool);
		virtual EL::StatusCode fillMuon(const xAOD::Muon_v1 *thisMuon, TString prefix = "");
		virtual EL::StatusCode fillPhoton(const xAOD::Photon_v1 *thisPhoton,  TString prefix = "");
		virtual EL::StatusCode fillElectron(const xAOD::Electron_v1 *thisElectron,  TString prefix = "");
		virtual EL::StatusCode fillJet(const xAOD::Jet *thisJet, int i, TString prefix = "");
		virtual std::map<TString, std::vector<float>> compactFillVarsF(std::vector<TString> keys, std::map<TString, float> vars, std::map<TString, std::vector<float>> output, TString prefix);
		virtual std::map<TString, std::vector<int>> compactFillVarsI(std::vector<TString> keys,
				std::map<TString, int> vars,
				std::map<TString, std::vector<int>> output,
				TString prefix);


		//xAOD things
		const xAOD::EventInfo *m_eventInfo; //!
		const xAOD::VertexContainer *m_vertices; //!

		const xAOD::MuonContainer *m_muons; //!
		std::pair< xAOD::MuonContainer*, xAOD::ShallowAuxContainer* > m_my_muons; //!
		xAOD::MuonContainer *m_allMuons; //!
		xAOD::MuonContainer *m_goodMuons; //!

		const xAOD::ElectronContainer *m_electrons; //!
		std::pair< xAOD::ElectronContainer*, xAOD::ShallowAuxContainer* > m_my_electrons; //!
		xAOD::ElectronContainer *m_allElectrons; //!
		xAOD::ElectronContainer *m_goodElectrons; //!

		const xAOD::PhotonContainer *m_photons; //!
		std::pair< xAOD::PhotonContainer*, xAOD::ShallowAuxContainer* > m_my_photons; //!
		xAOD::PhotonContainer *m_allPhotons; //!
		xAOD::PhotonContainer *m_goodPhotons; //!

		//things read from xAOD and made accessible to functions 
		//const xAOD::JetContainer *m_lc_jets; //!
		const xAOD::JetContainer *m_pf_jets; //!
		const xAOD::JetContainer *m_em_jets; //!
		const xAOD::JetContainer *m_tru_jets; //!
		//std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > m_my_lc_jets; //!
		std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > m_my_pf_jets; //!
		std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > m_my_em_jets; //!
		std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > m_my_tru_jets; //!
		xAOD::JetContainer *m_allJets; //!
		xAOD::JetContainer *m_allLCJets; //!
		xAOD::JetContainer *m_allPFJets; //!
		xAOD::JetContainer *m_allTruJets; //!
		xAOD::JetContainer *m_goodJets; //!
		std::vector<int> m_jetTag;
		std::vector<int> m_em_jetTag;

    		xAOD::MissingETContainer *m_met; //!
    		xAOD::MissingETAuxContainer *m_metAux; //!

		// this is a standard constructor
		JetCleaning ();

		// these are the functions inherited from Algorithm
		virtual EL::StatusCode setupJob (EL::Job& job);
		virtual EL::StatusCode fileExecute ();
		virtual EL::StatusCode histInitialize ();
		virtual EL::StatusCode changeInput (bool firstFile);
		virtual EL::StatusCode initialize ();
		virtual EL::StatusCode execute ();
		virtual EL::StatusCode postExecute ();
		virtual EL::StatusCode finalize ();
		virtual EL::StatusCode histFinalize ();

		// this is needed to distribute the algorithm to the workers
		ClassDef(JetCleaning, 1);
};

#endif
