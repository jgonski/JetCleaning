#ifndef __Analysis_Candidate__
#define __Analysis_Candidate__

#include <Rtypes.h>
#include <map>

class TTree;

namespace Analysis {
    class Candidate {
    public:
        ////
        // NOTE: variables must be sorted with inverse sizeof() criteria
        //
        //   >>> this explains why integers are AT THE BOTTOM <<<
        //
        ////
        //////////
        // event variables
        //////////
        Float_t averageIntPerXing;
        Float_t pu_weight;
        Float_t ps_weight;
        Float_t ps2_weight;
        Float_t pu_weight_J50;
        Float_t pu_weight_J12;
        Float_t pu_weight_J50vx;
        Float_t pu_weight_J12vx;
        Float_t btag_weight;
        Float_t vxz_weight;
        Float_t overlap_weight;
        Float_t event_weight;
        Float_t pdf_x1;
        Float_t pdf_x2;
        Float_t pdf_pdf1;
        Float_t pdf_pdf2;
        Float_t pdf_scale;
        ULong64_t PRWHash;
        //////////
        // composite observables (involve >1 objects)
        //////////
        Float_t shatR;
        Float_t gaminvR;
        Float_t gaminvRp1;
        Float_t dphi_BETA_R;
        Float_t dphi_J1_J2_R;
        Float_t gamma_Rp1;
        Float_t costhetaR;
        Float_t dphi_R_Rp1;
        Float_t mdeltaR;
        Float_t cosptR;
        Float_t costhetaRp1;
        Float_t munu_mT;
        Float_t enu_mT;
        Float_t jj_m;
        Float_t mumu_m;
        Float_t mumu_pt;
        Float_t mumu_eta;
        Float_t mumu_phi;
        Float_t ee_m;
        Float_t ee_pt;
        Float_t ee_eta;
        Float_t ee_phi;
        //////////
        // met
        //////////
        Float_t met;
        Float_t sumet;
        Float_t met_sig;
        Float_t met_phi;
        Float_t met_nomuon_et;
        Float_t met_nomuon_etx;
        Float_t met_nomuon_ety;
        Float_t met_nomuon_sumet;
        Float_t met_wmuon_et;
        Float_t met_wmuon_etx;
        Float_t met_wmuon_ety;
        Float_t met_wmuon_sumet;
        Float_t met_noelectron_et;
        Float_t met_noelectron_etx;
        Float_t met_noelectron_ety;
        Float_t met_noelectron_sumet;
        //////////
        // vetoed muons
        //////////
        Float_t mu_leading_charge;
        Float_t mu_leading_weight;
        Float_t mu_leading_weight_up;
        Float_t mu_leading_weight_down;
        Float_t mu_leading_pt;
        Float_t mu_leading_eta;
        Float_t mu_leading_phi;
        Float_t mu_leading_m;
        Float_t mu_leading_id_pt;
        Float_t mu_leading_id_eta;
        Float_t mu_leading_id_phi;
        Float_t mu_leading_id_m;
        Float_t mu_leading_me_pt;
        Float_t mu_leading_me_eta;
        Float_t mu_leading_me_phi;
        Float_t mu_leading_me_m;
        Float_t mu_leading_ptcone20;
        Float_t mu_leading_etcone20;
        Float_t mu_leading_ptcone40;
        Float_t mu_leading_etcone40;
        Float_t mu_leading_d0;
        Float_t mu_leading_d0sig;
        Float_t mu_leading_z0;
        Float_t mu_leading_z0sig;
        Float_t mu_leading_met_dphi;
        //
        Float_t mu_subleading_charge;
        Float_t mu_subleading_weight;
        Float_t mu_subleading_weight_up;
        Float_t mu_subleading_weight_down;
        Float_t mu_subleading_pt;
        Float_t mu_subleading_eta;
        Float_t mu_subleading_phi;
        Float_t mu_subleading_m;
        Float_t mu_subleading_id_pt;
        Float_t mu_subleading_id_eta;
        Float_t mu_subleading_id_phi;
        Float_t mu_subleading_id_m;
        Float_t mu_subleading_me_pt;
        Float_t mu_subleading_me_eta;
        Float_t mu_subleading_me_phi;
        Float_t mu_subleading_me_m;
        Float_t mu_subleading_ptcone20;
        Float_t mu_subleading_etcone20;
        Float_t mu_subleading_ptcone40;
        Float_t mu_subleading_etcone40;
        Float_t mu_subleading_d0;
        Float_t mu_subleading_d0sig;
        Float_t mu_subleading_z0;
        Float_t mu_subleading_z0sig;
        Float_t mu_subleading_met_dphi;
        //////////
        // vetoed electrons
        //////////
        Float_t el_leading_weight;
        Float_t el_leading_weight_up;
        Float_t el_leading_weight_down;
        Float_t el_leading_charge;
        Float_t el_leading_pt;
        Float_t el_leading_eta;
        Float_t el_leading_phi;
        Float_t el_leading_m;
        Float_t el_leading_id_pt;
        Float_t el_leading_id_eta;
        Float_t el_leading_id_phi;
        Float_t el_leading_id_m;
        Float_t el_leading_cl_pt;
        Float_t el_leading_cl_eta;
        Float_t el_leading_cl_phi;
        Float_t el_leading_cl_m;
        Float_t el_leading_ptcone20;
        Float_t el_leading_etcone20;
        Float_t el_leading_ptcone40;
        Float_t el_leading_etcone40;
        Float_t el_leading_d0;
        Float_t el_leading_d0sig;
        Float_t el_leading_z0;
        Float_t el_leading_z0sig;
        Float_t el_leading_met_dphi;
        Float_t el_leading_demaxs1;
        Float_t el_leading_fside;
        Float_t el_leading_weta2;
        Float_t el_leading_ws3;
        Float_t el_leading_eratio;
        Float_t el_leading_reta;
        Float_t el_leading_rphi;
        Float_t el_leading_time_cl;
        Float_t el_leading_time_maxEcell;
        Float_t el_leading_truth_pt;
        Float_t el_leading_truth_eta;
        Float_t el_leading_truth_phi;
        Float_t el_leading_truth_E;
        //
        Float_t el_subleading_weight;
        Float_t el_subleading_weight_up;
        Float_t el_subleading_weight_down;
        Float_t el_subleading_charge;
        Float_t el_subleading_pt;
        Float_t el_subleading_eta;
        Float_t el_subleading_phi;
        Float_t el_subleading_m;
        Float_t el_subleading_id_pt;
        Float_t el_subleading_id_eta;
        Float_t el_subleading_id_phi;
        Float_t el_subleading_id_m;
        Float_t el_subleading_cl_pt;
        Float_t el_subleading_cl_eta;
        Float_t el_subleading_cl_phi;
        Float_t el_subleading_cl_m;
        Float_t el_subleading_ptcone20;
        Float_t el_subleading_etcone20;
        Float_t el_subleading_ptcone40;
        Float_t el_subleading_etcone40;
        Float_t el_subleading_d0;
        Float_t el_subleading_d0sig;
        Float_t el_subleading_z0;
        Float_t el_subleading_z0sig;
        Float_t el_subleading_met_dphi;
        Float_t el_subleading_demaxs1;
        Float_t el_subleading_fside;
        Float_t el_subleading_weta2;
        Float_t el_subleading_ws3;
        Float_t el_subleading_eratio;
        Float_t el_subleading_reta;
        Float_t el_subleading_rphi;
        Float_t el_subleading_time_cl;
        Float_t el_subleading_time_maxEcell;
        Float_t el_subleading_truth_pt;
        Float_t el_subleading_truth_eta;
        Float_t el_subleading_truth_phi;
        Float_t el_subleading_truth_E;
        //////////
        // jets
        //////////


        std::vector<float> jet_weight; // TODO: add variations
        std::vector<float> jet_pt;
        std::vector<float> jet_eta;
        std::vector<float> jet_phi;
        std::vector<float> jet_raw_pt;
        std::vector<float> jet_raw_eta;
        std::vector<float> jet_raw_phi;
        std::vector<float> jet_dphi_met;
        std::vector<float> jet_m;
        std::vector<float> jet_e;
        std::vector<int>   jet_tag;
        std::vector<float> jet_met_dphi;
        std::vector<float> jet_timing;
        std::vector<float> jet_emfrac;
        std::vector<float> jet_hecf;
        std::vector<float> jet_hecq;
        std::vector<float> jet_fch;
        std::vector<float> jet_larq;
        std::vector<float> jet_avglarq;
        std::vector<float> jet_fmax;
        std::vector<int>   jet_fmaxi;
        std::vector<float> jet_nege;
        std::vector<float> jet_clust1_pt;
        std::vector<float> jet_clust1_eta;
        std::vector<float> jet_clust1_phi;
        std::vector<float> jet_lambda;
        std::vector<float> jet_lambda2;
        std::vector<float> jet_jvtxf;
        std::vector<int>   jet_clust_n;
        std::vector<float> jet_clust_sumpt;
        std::vector<float> jet_SV1plusIP3D_discriminant;
        std::vector<bool> jet_isSpike;
        Float_t jet_dphi_leadsublead;
        Float_t mht;
        Float_t mhtjvt;
        Float_t mht_phi;
        Float_t mhtjvt_phi;
        Float_t jet_dphi_leadmht;
        Float_t jet_dphi_leadmhtjvt;

        Float_t jet_true_pt;
        Float_t jet_true_eta;
        Float_t jet_true_phi;

        std::vector<float> jet_lc_weight; // TODO: add variations

        std::map <TString, std::vector<float>> LepKinematics;
        std::map <TString, std::vector<float>> JetKinematics;
        std::map <TString, std::vector<float>> JetCleanVarf;
        std::map <TString, std::vector<int>> JetCleanVari;
        std::map <TString, std::vector<std::vector<float>>> JetSegmentVarf;
        std::map <TString, std::vector<std::vector<int>>> JetSegmentVari;
        std::vector<TString> JetCleanVarf_keys;
        std::vector<TString> JetCleanVari_keys;
        std::vector<TString> LepKinematics_keys;
        std::vector<TString> JetKinematics_keys;
        std::vector<TString> JetSegmentVarf_keys;
        std::vector<TString> JetSegmentVari_keys;

        std::vector<float> jet_lc_pt;
        std::vector<float> jet_lc_eta;
        std::vector<float> jet_lc_phi;
        std::vector<float> jet_lc_raw_pt;
        std::vector<float> jet_lc_raw_eta;
        std::vector<float> jet_lc_raw_phi;
        std::vector<float> jet_lc_dphi_met;
        std::vector<float> jet_lc_m;
        std::vector<float> jet_lc_e;
        std::vector<int>   jet_lc_tag;
        std::vector<float> jet_lc_met_dphi;
        std::vector<float> jet_lc_timing;
        std::vector<float> jet_lc_emfrac;
        std::vector<float> jet_lc_hecf;
        std::vector<float> jet_lc_hecq;
        std::vector<float> jet_lc_fch;
        std::vector<float> jet_lc_larq;
        std::vector<float> jet_lc_avglarq;
        std::vector<float> jet_lc_fmax;
        std::vector<int>   jet_lc_fmaxi;
        std::vector<float> jet_lc_nege;
        std::vector<float> jet_lc_clust1_pt;
        std::vector<float> jet_lc_clust1_eta;
        std::vector<float> jet_lc_clust1_phi;
        std::vector<float> jet_lc_lambda;
        std::vector<float> jet_lc_lambda2;
        std::vector<float> jet_lc_jvtxf;
        std::vector<int>   jet_lc_clust_n;
        std::vector<float> jet_lc_clust_sumpt;
        std::vector<float> jet_lc_SV1plusIP3D_discriminant;
        std::vector<bool>  jet_lc_isSpike;

        std::vector<float> jet_pf_pt;
        std::vector<float> jet_pf_eta;
        std::vector<float> jet_pf_phi;
        std::vector<float> jet_pf_raw_pt;
        std::vector<float> jet_pf_raw_eta;
        std::vector<float> jet_pf_raw_phi;
        std::vector<float> jet_pf_dphi_met;
        std::vector<float> jet_pf_m;
        std::vector<float> jet_pf_e;
        std::vector<int>   jet_pf_tag;
        std::vector<float> jet_pf_met_dphi;
        std::vector<float> jet_pf_timing;
        std::vector<float> jet_pf_emfrac;
        std::vector<float> jet_pf_hecf;
        std::vector<float> jet_pf_hecq;
        std::vector<float> jet_pf_fch;
        std::vector<float> jet_pf_larq;
        std::vector<float> jet_pf_avglarq;
        std::vector<float> jet_pf_fmax;
        std::vector<int>   jet_pf_fmaxi;
        std::vector<float> jet_pf_nege;
        std::vector<float> jet_pf_clust1_pt;
        std::vector<float> jet_pf_clust1_eta;
        std::vector<float> jet_pf_clust1_phi;
        std::vector<float> jet_pf_lambda;
        std::vector<float> jet_pf_lambda2;
        std::vector<float> jet_pf_jvtxf;
        std::vector<int>   jet_pf_clust_n;
        std::vector<float> jet_pf_clust_sumpt;
        std::vector<float> jet_pf_SV1plusIP3D_discriminant;
        std::vector<bool>  jet_pf_isSpike;
        
	bool isGoodJet;
        bool isBadJet;
        bool isHardJet;
        int passVxCut;
        int passMCCut;
        int passGRLCut;

        Float_t jet_lc_dphi_leadsublead;


        Float_t jet_leading_weight; // TODO: add variations
        Float_t jet_leading_pt;
        Float_t jet_leading_eta;
        Float_t jet_leading_phi;
        Float_t jet_leading_m;
        Float_t jet_leading_met_dphi;
        Float_t jet_leading_timing;
        Float_t jet_leading_emfrac;
        Float_t jet_leading_hecf;
        Float_t jet_leading_fch;
        Float_t jet_leading_jvtxf;
        Float_t jet_leading_SV1plusIP3D_discriminant;
        //
        Float_t jet_subleading_weight; // TODO: add variations
        Float_t jet_subleading_pt;
        Float_t jet_subleading_eta;
        Float_t jet_subleading_phi;
        Float_t jet_subleading_m;
        Float_t jet_subleading_met_dphi;
        Float_t jet_subleading_timing;
        Float_t jet_subleading_emfrac;
        Float_t jet_subleading_hecf;
        Float_t jet_subleading_fch;
        Float_t jet_subleading_jvtxf;
        Float_t jet_subleading_SV1plusIP3D_discriminant;
        //
        Float_t jet_subsubleading_weight; // TODO: add variations
        Float_t jet_subsubleading_pt;
        Float_t jet_subsubleading_eta;
        Float_t jet_subsubleading_phi;
        Float_t jet_subsubleading_m;
        Float_t jet_subsubleading_met_dphi;
        Float_t jet_subsubleading_timing;
        Float_t jet_subsubleading_emfrac;
        Float_t jet_subsubleading_hecf;
        Float_t jet_subsubleading_fch;
        Float_t jet_subsubleading_jvtxf;
        Float_t jet_subsubleading_SV1plusIP3D_discriminant;
        //////////
        // hemispheres, or mega-jets
        //////////
        Float_t hem1_pt;
        Float_t hem1_eta;
        Float_t hem1_phi;
        Float_t hem1_m;
        //
        Float_t hem2_pt;
        Float_t hem2_eta;
        Float_t hem2_phi;
        Float_t hem2_m;

        //////////
        // SR/CR flags
        //////////
        Int_t last; // referred to SR
        Int_t last_Wmunu; // referred to Wmunu CR
        Int_t last_Wenu; // referred to Wmunu CR
        Int_t selected; // referred to SR
        Int_t isMuonCR;
        Int_t isElectronCR;
        Int_t isJetCR;
        //////////
        // event variables
        //////////
        Int_t run;
        Int_t event;
        Int_t lbn;
        Int_t bcid;
        Int_t mu;
        Int_t isMC;
        Int_t is13TeV;
        
        Int_t bib;
        Int_t bib_flag;

        Int_t trigger; // 0: none, 1: jet, 2: egamma, 3: both
        Int_t trig_HLT_j360_a4tchad;


        Int_t trig_L1_lep;
        Int_t trig_th_L1_1e;
        Int_t trig_th_L1_2e;
        Int_t trig_th_L1_1mu;
        Int_t trig_th_L1_2mu;

        Int_t trig_HLT_lep;
        Int_t trig_th_HLT_1e;
        Int_t trig_th_HLT_2e;
        Int_t trig_th_HLT_1mu;
        Int_t trig_th_HLT_2mu;

        Int_t trig_HLT_j_xe;
        Int_t trig_HLT_1j;
        Int_t trig_L1_1j;

        std::vector<TString>      indiv_trigger_keys;
        std::map<TString, int>  indiv_triggers;
        std::map<TString, float>    indiv_triggers_ps;
        std::map<TString, float>    indiv_triggers_psFIX;

        Int_t trig_n_HLT_1j;
        std::vector<int> trig_th_HLT_1j;
        std::vector<float> trig_ps_HLT_1j;
        Int_t trig_n_L1_1J;
        std::vector<int> trig_th_L1_1J;
        std::vector<float> trig_ps_L1_1J;

        Int_t hfor;
        Int_t n_vx;
        Int_t n_ph; // with good objects
        Int_t n_jet;
        //Int_t n_pf_jet;
        Int_t n_monojet;
        Int_t n_baseline_jet;
        Int_t n_good_jet; // with good objects
        Int_t n_goodel; // with good objects
        Int_t n_goodmu; // with good objects
        Int_t pdf_id1;
        Int_t pdf_id2;
        Int_t bb_decision;
        //////////
        // composite observables (involve >1 objects)
        //////////
        //////////
        // met
        //////////
        //////////
        // vetoed muons
        //////////
        Int_t mu_leading_author;
        Int_t mu_leading_isSA;
        //
        Int_t mu_subleading_author;
        Int_t mu_subleading_isSA;
        //////////
        // vetoed electrons
        //////////
        Int_t el_leading_author;
        Int_t el_leading_isConv;
        Int_t el_leading_truth_matched;
        Int_t el_leading_truth_mothertype;
        Int_t el_leading_truth_status;
        Int_t el_leading_truth_type;
        Int_t el_leading_truth_typebkg;
        Int_t el_leading_truth_origin;
        Int_t el_leading_truth_originbkg;
        //
        Int_t el_subleading_author;
        Int_t el_subleading_isConv;
        Int_t el_subleading_truth_matched;
        Int_t el_subleading_truth_mothertype;
        Int_t el_subleading_truth_status;
        Int_t el_subleading_truth_type;
        Int_t el_subleading_truth_typebkg;
        Int_t el_subleading_truth_origin;
        Int_t el_subleading_truth_originbkg;

        Int_t n_clust;
        std::vector<float> clust_eta;
        std::vector<float> clust_phi;
        std::vector<float> clust_e;

    public:
        Candidate();
        ~Candidate();
        void reset();
        void attachToTree(TTree *tree);
        void setJetSegments(Bool_t val);
        void setLCJets(Bool_t val);
        Bool_t getJetSegments();
    private:
        void fillLeptons(TTree *tree);
        void fillJetCollection(TTree *tree, TString col);
        Bool_t m_doJetSegments;
        Bool_t m_fillLeptons;
        Bool_t m_fillLCJets;
        Bool_t m_fillPFJets;
    };
}

#endif
