#!/usr/bin/env python
import sys
from collections import defaultdict
from PyCool import cool
import TriggerMenu.l1.PrescaleHelper as psh

dbSvc = cool.DatabaseSvcFactory.databaseService()

def getCoolData(run, db, folder, fields):

    db = dbSvc.openDatabase('oracle://ATLAS_COOLPROD;schema=%s;dbname=CONDBR2' % db,False)
    fld = db.getFolder( folder )
    objs = fld.browseObjects( (run<<32)+1, ((run+1)<<32)-1, cool.ChannelSelection() )

    result = []    
    while objs.goToNext():
        obj = objs.currentRef()
        result += [ (obj.since() & 0xffffffff, (obj.until()-1) & 0xffffffff, obj.channelId(), map(obj.payloadValue, fields)) ]

    return result


def getChainsInfo(run, triggers):

    hltmenu = getCoolData( run,
                           'ATLAS_COOLONL_TRIGGER',
                           '/TRIGGER/HLT/Menu',
                           ["ChainName","LowerChainName","ChainCounter"] )

    return dict([ (v[0],v) for (b,e,c,v) in hltmenu if v[0] in triggers])


def getItemsInfo(run, triggers):

    lvl1menu = getCoolData( run,
                            'ATLAS_COOLONL_TRIGGER',
                            '/TRIGGER/LVL1/Menu',
                            ["ItemName"] )

    return dict([(v[0],[c]+v) for (b,e,c,v) in lvl1menu if v[0] in triggers])


def getL1PSK(run):

    return getCoolData( run,
                        'ATLAS_COOLONL_TRIGGER',
                        '/TRIGGER/LVL1/Lvl1ConfigKey',
                        ['Lvl1PrescaleConfigurationKey'] )


def getHLTPSK(run):

    return getCoolData( run,
                        'ATLAS_COOLONL_TRIGGER',
                        '/TRIGGER/HLT/PrescaleKey',
                        ['HltPrescaleKey'] )


def getL1Prescales(run, ctpIds):
    ps = getCoolData( run,
                      'ATLAS_COOLONL_TRIGGER',
                      '/TRIGGER/LVL1/Prescales',
                      ['Lvl1Prescale'] )

    psd = defaultdict(list)
    [map(psd[c].append,((b,e,int(v[0])),) ) for (b,e,c,v) in ps if c in ctpIds]
    return psd
    

def getHLTPrescales(run, chainIds):
    ps = getCoolData( run,
                      'ATLAS_COOLONL_TRIGGER',
                      '/TRIGGER/HLT/Prescales',
                      ['Prescale'] )

    psd = defaultdict(list)
    [map(psd[c-20000].append,((b,e,float(v[0])),) ) for (b,e,c,v) in ps if (c-20000) in chainIds]
    return psd


def getCombinedPrescales(triggerIDMap, l1pss, hltpss):

    psmap = {}
    
    for trigger, idmap in sorted(triggerIDMap.items()):
        print '\n',trigger
        print '-' * len(trigger)

        l1ID  = idmap["l1Id"]
        hltID = idmap["hltId"]

        l1lbps = l1pss[l1ID]
        hltlbps = hltpss[hltID]

        lbstart = sorted(list(set([v[0] for v in l1lbps]).union([v[0] for v in hltlbps])))
        lbstart.remove(0)

        psmap[trigger] = []
        for lb in lbstart:

            l1ps = None
            hltps = None
            
            for b,e,v in l1lbps:
                if lb>=b and lb<=e:
                    l1ps = psh.getPrescaleFromCut(v)
                    break
            for b,e,v in hltlbps:
                if lb>=b and lb<=e:
                    hltps = v
                    break

            ps = abs(l1ps * hltps)
            if l1ps<0 or hltps<0:
                ps = -1 * ps

            psmap[trigger] += [ (lb, ps, l1ps, hltps) ]

            print "LB %3i (l1ps %10.1f  hltps %7.2f)  ==>  ps %10.1f" % (lb,l1ps,hltps,ps)

    return psmap


def getMapForRun(run, triggers):
    chains = getChainsInfo( run, triggers )

    noL1Input = [c for c,v in chains.items() if v[1]=='']
    if noL1Input:
        print "These chains have no L1 input (ie they run on all L1Accept) and should not be used. They are removed from further analysis: %s" % ", ".join(noL1Input)
        map(triggers.remove, noL1Input)

    items = getItemsInfo( run, [v[1] for v in chains.values()] )

    triggerIDMap = dict([ (c, {"l1Id" : int(items[chains[c][1]][0]) , "hltId" : int(chains[c][2])}) for c in chains])

    # item and chain IDs (for getting the correct prescales)
    itemIDs  = [idd["l1Id"] for idd in triggerIDMap.values()]
    chainIDs = [idd["hltId"] for idd in triggerIDMap.values()]

    # getting the prescales
    l1pss  = getL1Prescales( run, itemIDs )
    hltpss = getHLTPrescales( run, chainIDs )

    return getCombinedPrescales(triggerIDMap, l1pss, hltpss)

def main():

    #runs = [276262,276329,276336,276416,276511,276689,276778,276790,276952,276954,278880,278912,278968,279169,279259,279279,279284,279345,279515,279598,279685,279764,279813,279867,279928,279932, 279984]
    #runs += [297730,298595,298609,298633,298687,298690,298771,298773,298862,298967,299055,299144,299147,299184,299243]
    
    #data15  
    #runs = [ 284484,  279984,  280500,  276262,  270816,  270949,  279928,  280231,  280673,  266919,  279284,  267162,  280853,  281070,  270806,  276952,  278748,  279259,  280753,  282631,  283270,  280614,  282712,  267073,  266904,  271388,  278880,  280368,  284420,  267638,  281074,  280862,  284006,  280950,  281411,  283074,  280423,  281385,  283155,  276329,  280319,  281130,  270953,  284154,  278968,  279764,  281317,  267639,  284213,  267167,  279685,  279932,  276183,  276954,  276161,  267152,  276147,  276176,  270441,  276189,  276245,  278970,  278727,  276073,  276731,  284427,  279169,  282784,  280464,  281143,  283608,  280977,  284285,  279279,  276511,  279598,  279867,  278912,  279813,  270588,  276790,  279345,  271516,  276330,  276336,  283780,  280520,  270448,  276689,  281381,  271298,  279515,  283429,  276181,  282625,  271744,  276212,  276416,  280273,  281075,  276778,  271421,  284473,  282992,  271595] 
    
    #data16
    #runs = [ 296939,    296942,    297041,    297083,    297170,    297447,    297730,    298591,    298595,    298609,    298633,    298687,    298690,    298771,    298773,    298862,    298967,    299055,    299144,    299147,    299184,    299241,    299243,    299288,    299315,    299340,    299343,    299390,    299584,    300279,    300287,    300345,    300415,    300418,    300487,    300540,    300571,    300600,    300655,    300687,    300784,    300800,    300863,    300908, 301912, 301915, 301918, 301932, 301973, 302956, 302925, 302919, 302831, 302737, 302393, 302391, 302380, 302347, 302300, 302269, 302265, 302137, 302053, 303892,  303846,  303832,  303638,  303560,  303499,  303421,  303338,  303304,  303291,  303266,  303264,  303208,  303201,  303079,  303007, 304008, 310249, 310405, 310738, 310691, 311071, 311281, 311321] 

    #data17
    runs = [325790, 326446, 326468, 326551, 326657, 326695, 326834, 329778, 329716, 329869, 329835, 329829, 329964, 329780, 333778, 333707, 333904, 333853, 333828, 333979]
    
    #data18
    runs = [351325, 351296, 351223, 351160, 351062, 350923, 350880, 350848, 350751, 350803, 350842, 350531, 350749, 350479]

    triggers = [
        "HLT_j460",
        "HLT_j400",
        "HLT_j360",
        "HLT_j380",
        "HLT_j260",
        "HLT_j175",
        "HLT_j150",
        "HLT_j60",
        "HLT_j35",
        "HLT_j25",
        "HLT_j15",
 	"HLT_xe80",
 	"HLT_xe100",
 	"HLT_xe80_tc_lcw_L1XE50", 
	"HLT_xe90_mht_L1XE50",
	"HLT_xe100_mht_L1XE50"
        #"HLT_mistimemonl1bccorr",
        #"HLT_mistimemonl1bccorrnomu",
        #"HLT_mistimemoncaltimenomu",
        #"HLT_mistimemonj400"
        #"L1_RD0_FILLED",
        #"L1_J20",
		#"L1_J40",
		#"L1_J75",
		#"L1_J100",
        ]

    f = open('trigger_prescales.cxx', 'w')
    f.write( "// Generated automatically by getPrescaleForTrigger.py \n")
    f.write( "#include <TROOT.h>\n\n")
    f.write( "float getPS(const TString trigger, const int run, const int lbn){\n" )
    for run in runs:
	psmap = getMapForRun(run, triggers)
        f.write( "    if (run == %i){\n" % run )
        for trigger, psset in psmap.items():
            f.write( "        if (trigger == \"%s\"){\n" % trigger )
            for i in range(len(psset)-1):
                (lb_start, ps, l1ps, hltps) = psset[i] 
                if ps > 0:
                    lb_end = psset[i+1][0]
                    f.write( "            if (lbn >= %i && lbn < %i) return %f;\n" % (lb_start, lb_end, ps) )
                    #print lb_start, lb_end, ps
            f.write( "        }\n" )
        f.write( "        return -1;\n" )
        f.write( "    }\n" )
    f.write( "    return -1;\n" )
    f.write( "}\n" )
    f.close()

if __name__=="__main__":
    try:
        sys.exit(main())
    except KeyboardInterrupt:
        sys.exit(1)
