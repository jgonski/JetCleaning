map<int,TString> path1;
map<int,float> ngen1;

void dictionary_set1(){
  ngen1[9007585] = 6397606.0 ;
  ngen1[9007526] = 61259480.0 ;
  ngen1[9007562] = 5581892.0 ;
  ngen1[9007516] = 404077160.0 ;
  ngen1[9007572] = 5807980.0 ;
  ngen1[9007504] = 925697056.0 ;
  ngen1[9007491] = 232702576.0 ;
  ngen1[9007597] = 6384352.0 ;
  ngen1[9007480] = 6403068.0 ;
  ngen1[9007608] = 6322994.0 ;
  ngen1[9007509] = 647984312.0 ;
  ngen1[9007498] = 189971408.0 ;
  ngen1[9007552] = 161036656.0 ;
  path1[9007516] = "/tmp/jgonski/mc15c/user.jgonski.mc15c.mc15_13TeV.361025_minitrees.root.88448583/user.jgonski.9007516._000003.minitrees.root" ;
  path1[9007585] = "/tmp/jgonski/mc15c/user.jgonski.mc15c.mc15_13TeV.361030_minitrees.root.88447913/user.jgonski.9007585._000002.minitrees.root" ;
  path1[9007480] = "/tmp/jgonski/mc15c/user.jgonski.mc15c.mc15_13TeV.361020_minitrees.root.88457092/user.jgonski.9007480._000001.minitrees.root" ;
  path1[9007498] = "/tmp/jgonski/mc15c/user.jgonski.mc15c.mc15_13TeV.361022_minitrees.root.88442386/user.jgonski.9007498._000001.minitrees.root" ;
  path1[9007491] = "/tmp/jgonski/mc15c/user.jgonski.mc15c.mc15_13TeV.361021_minitrees.root.88472741/user.jgonski.9007491._000001.minitrees.root" ;
  path1[9007509] = "/tmp/jgonski/mc15c/user.jgonski.mc15c.mc15_13TeV.361024_minitrees.root.88462640/user.jgonski.9007509._000006.minitrees.root" ;
  path1[9007509] = "/tmp/jgonski/mc15c/user.jgonski.mc15c.mc15_13TeV.361024_minitrees.root.88462640/user.jgonski.9007509._000003.minitrees.root" ;
  path1[9007509] = "/tmp/jgonski/mc15c/user.jgonski.mc15c.mc15_13TeV.361024_minitrees.root.88462640/user.jgonski.9007509._000002.minitrees.root" ;
  path1[9007572] = "/tmp/jgonski/mc15c/user.jgonski.mc15c.mc15_13TeV.361029_minitrees.root.88453526/user.jgonski.9007572._000001.minitrees.root" ;
  path1[9007608] = "/tmp/jgonski/mc15c/user.jgonski.mc15c.mc15_13TeV.361032_minitrees.root.88447038/user.jgonski.9007608._000001.minitrees.root" ;
  path1[9007526] = "/tmp/jgonski/mc15c/user.jgonski.mc15c.mc15_13TeV.361026_minitrees.root.88445447/user.jgonski.9007526._000001.minitrees.root" ;
  path1[9007516] = "/tmp/jgonski/mc15c/user.jgonski.mc15c.mc15_13TeV.361025_minitrees.root.88488511/user.jgonski.9007516._000007.minitrees.root" ;
  path1[9007562] = "/tmp/jgonski/mc15c/user.jgonski.mc15c.mc15_13TeV.361028_minitrees.root.88445072/user.jgonski.9007562._000001.minitrees.root" ;
  path1[9007516] = "/tmp/jgonski/mc15c/user.jgonski.mc15c.mc15_13TeV.361025_minitrees.root.88443731/user.jgonski.9007516._000002.minitrees.root" ;
  path1[9007516] = "/tmp/jgonski/mc15c/user.jgonski.mc15c.mc15_13TeV.361025_minitrees.root.88443731/user.jgonski.9007516._000001.minitrees.root" ;
  path1[9007552] = "/tmp/jgonski/mc15c/user.jgonski.mc15c.mc15_13TeV.361027_minitrees.root.88447753/user.jgonski.9007552._000001.minitrees.root" ;
  path1[9007504] = "/tmp/jgonski/mc15c/user.jgonski.mc15c.mc15_13TeV.361023_minitrees.root.88448350/user.jgonski.9007504._000003.minitrees.root" ;
  path1[9007509] = "/tmp/jgonski/mc15c/user.jgonski.mc15c.mc15_13TeV.361024_minitrees.root.88508637/user.jgonski.9007509._000008.minitrees.root" ;
  path1[9007597] = "/tmp/jgonski/mc15c/user.jgonski.mc15c.mc15_13TeV.361031_minitrees.root.88454459/user.jgonski.9007597._000001.minitrees.root" ;
  path1[9007585] = "/tmp/jgonski/mc15c/user.jgonski.mc15c.mc15_13TeV.361030_minitrees.root.88445427/user.jgonski.9007585._000001.minitrees.root" ;
  path1[9007504] = "/tmp/jgonski/mc15c/user.jgonski.mc15c.mc15_13TeV.361023_minitrees.root.88446112/user.jgonski.9007504._000002.minitrees.root" ;
  path1[9007504] = "/tmp/jgonski/mc15c/user.jgonski.mc15c.mc15_13TeV.361023_minitrees.root.88446112/user.jgonski.9007504._000001.minitrees.root" ;
}

map<int,float> xsec;
void dictionary_setcommon(){
  xsec[361020]= 78420000000 ; 
  xsec[361021]= 78420000000; 
  xsec[361022]= 2433200000; 
  xsec[361023]= 26454000 ; 
  xsec[361024]= 254630 ; 
  xsec[361025]= 4553.5; 
  xsec[361026]= 257.53; 
  xsec[361027]= 016.215; 
  xsec[361028]= 0.62502; 
  xsec[361029]= 0.019639 ; 
  xsec[361030]= 0.0011962 ; 
  xsec[361031]= 0.000042258; 
  xsec[361032]= 0.0000010367; 

}
