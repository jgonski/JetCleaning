/***********************************************************
 Classes & Functions to efficiently & intelligently read Cleaning TTrees
 Subclasses of classes from SmartReading.hxx
************************************************************/

#include <vector>
#include <map>
#include <iostream>
#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TH1.h>
#include <TH2.h>
#include <TChain.h>

#include "SmartReading.hxx"
#include "prescales/trigger_prescales.cxx"

/*****************************
 * Binning Definitions       *
 *****************************/

// histogram binning used by the jet cleaning CONF note
// Binning is a helper class that sotres binning info
// b.set() takes (num bins, x low, x high)
Binning getCONFBinning(TString param) {
    Binning b;
    if (param == "pt") b.set(50, 0, 1000);
    else if (param == "eta") b.set(25, -5, 5);
    else if (param == "phi") b.set(28, -3.14159, 3.14159);
    else if (param == "emfrac" || param == "hecf" || param == "hecq" || param == "avglarq" || param == "larq")
        b.set(50, -0.1, 1.1);
    else if (param == "fmax") b.set(50, -0.1, 1.2);
    else if (param == "fmaxi") b.set(24, 0, 24);
    else if (param == "fch") b.set(50, -0.1, 2.0);
    else if (param == "fchfmax") b.set(50, -0.1, 6.0);
    else if (param.Contains( "timing")) b.set(40, -50, 50);
    else if (param == "nege") b.set(50, -100, 50);
    else if (param == "lbn") b.set(200, 0, 600);
    else if (param == "bcid") b.set(50, 0, 3500);
    else if (param == "run") b.set(50, 295000, 302000);
    else if (param == ("n90const")) b.set(20, 0, 20);
    else if (param == "n_jet") b.set(10, -0.5, 9.5);
    else if (param == "mht") b.set(100, 0, 500);
    return b;
}

Binning getTestBinning(TString param) {
    Binning b;
    if (param == "pt") b.set(120, 0, 1200);
    else if (param == "mu") b.set(50, 0, 50);
    else if (param == "eta") b.set(25, -5, 5);
    else if (param == "phi") b.set(28, -3.14159, 3.14159);
    else if (param == "met_phi") b.set(28, -3.14159, 3.14159);
    else if (param == "emfrac" || param == "hecf" || param == "hecq" || param == "avglarq" || param == "larq")
        b.set(50, -0.1, 1.1);
    else if (param == "fmax") b.set(50, -0.1, 1.2);
    else if (param == "fmaxi") b.set(24, 0, 24);
    else if (param == "fch") b.set(50, -0.1, 2.0);
    else if (param == "fchfmax") b.set(50, -0.1, 6.0);
    else if (param.Contains( "timing")) b.set(40, -35, 35);
    else if (param == "nege") b.set(50, -100, 50);
    else if (param == "lbn") b.set(200, 0, 600);
    else if (param == "bcid") b.set(50, 0, 3500);
    else if (param == "run") b.set(50, 295000, 302000);
    else if (param == ("n90const")) b.set(30, 0, 30);
    else if (param == "n_jet") b.set(10, -0.5, 9.5);
    else if (param == "mht") b.set(100, 0, 500);
    else if (param == "met") b.set(200, 0, 1000);
    return b;
}

/*****************************
 * Sublclass Definitions       *
 *****************************/
 
  class CutHelper{
     private:
        bool (*pass_cut)();
        bool pass_val;
        bool current_entry;
     public:
        CutHelper( bool (*in_f)()){
            pass_val = false;
            current_entry = -1;
            pass_cut = in_f;
        }
        bool pass(int entry){
            if (entry != current_entry) {
                current_entry = entry;
                pass_val = pass_cut();
            }
            return pass_val;
        }
 };
     

 // jet cleaning subclass of TreeEvtInfo
class CleanTreeEvtInfo : public TreeEvtInfo {
    private:
        GeneralSmartReader * jet_pt;
        GeneralSmartReader * jet_eta;
        GeneralSmartReader * n_jet;
        GeneralSmartReader * n_goodel;
        GeneralSmartReader * n_goodmu;
        map<TString,CutHelper *> cuts;
    public:
        CleanTreeEvtInfo(): TreeEvtInfo() {
            setCuts();
        }
        CleanTreeEvtInfo(TTree * tree): TreeEvtInfo(tree) {
            setCuts();
        }
        /******
            Fast Cutflow Functions
        *******/
        
        bool pass_leadjetpt(float threshold){
            return jet_pt->get(0) > threshold;
        }
  
        bool pass_leptonveto(){
            return n_goodel->get() == 0 && n_goodmu->get() == 0;
        }
        
        bool pass_maxjets(int n){
            return n_jet->get() <= n;
        }
        
        /*bool pass_dsfake() {

            if (pass_maxjets(4) && pass_leptonveto() 

             && evt->get("mht") > 200
             && evt->get("jet_a4em_dphi_leadmht", 0) > 0.4
             && pass_leadjetpt(150)
             && fabs(jet_eta->get(0)) < 2.4) 
                return true;

            else return false;
        }*/
        
        bool pass_Loose(TreeEvtInfo * evt, int j) {
            //LOOSER Cuts
            //HEC cut
            if (evt->get("jet_a4em_hecf", j)   > 0.5
                && fabs(evt->get("jet_a4em_hecq", j))  > 0.5
                &&      evt->get("jet_a4em_avglarq", j) / 65535 > 0.8) return false;
            //NegE cut
            if (fabs(evt->get("jet_a4em_nege", j)) > 60) return false;
            //NC1 cut
            if (evt->get("jet_a4em_emfrac", j) < 0.05
                &&      evt->get("jet_a4em_fch", j)    < 0.05
                && fabs(evt->get("jet_a4em_eta", j))   < 2) return false;
            //NC2 cut
            if (evt->get("jet_a4em_emfrac", j) < 0.05 && fabs(evt->get("jet_a4em_eta", j)) >= 2) return false;
            //fmax cut
            if (evt->get("jet_a4em_fmax", j) > 0.99 && fabs(evt->get("jet_a4em_eta", j)) < 2) return false;
            //emfrac cut
            if (evt->get("jet_a4em_emfrac", j) > 0.95
                && fabs(evt->get("jet_a4em_larq", j))  > 0.8
                &&      evt->get("jet_a4em_avglarq", j) / 65535 > 0.8
                && fabs(evt->get("jet_a4em_eta", j))   < 2.8) return false;

            return true;
        }
        
        static bool test_this(){return true;}

    private:
        void setPointers(){
            jet_pt = &branches["jet_a4em_pt"];
            jet_eta = &branches["jet_a4em_eta"];
            n_goodel = &branches["n_goodel"];
            n_goodmu = &branches["n_goodmu"];
            n_jet = &branches["n_jet"];

        }
        void setCuts(){
            cuts["test"]  = new CutHelper(test_this);
        }
    
    protected:
        void initBranches(TTree * t) {
        //construct a new smartreader object for each branch found in the tree
        branches.clear();
        for (BranchInfo b : getListOfBranches(t))
            if (!b.name.Contains("PRWHash")) branches[b.name].setType(b.className);
        setPointers();
    }
};



// jet cleaning subclass of SelectionHists
class CleanSelectionHists: public SelectionHists {
private:

    // lists of jet & event vars to help define histograms
    vector<TString> vars1D;
    vector<TString>vars2D_x;
    vector<TString>vars2D_y;

    // number of jets to add to histograms in each event
    int fillJets;
    int jetIndex;

public:
    CleanSelectionHists(TreeEvtInfo * in_evt, bool (*in_f)(TreeEvtInfo *), TString in_name = "") : SelectionHists(in_evt, in_f, in_name) {
        fillJets = 1;
        jetIndex = -1;
        internal_binning = getTestBinning;
        setVars(getDefaultVars());
        setMaps();
    }

    // fill histograms with leading & subleading jets
    void setDijet() {
        fillJets = 2;
        jetIndex = -1;
    }

    void setLeadjet() {
        fillJets = 1;
        jetIndex = 0;
    }

    void setSubleadjet() {
        fillJets = 1;
        jetIndex = 1;
    }
    
    void setInclusiveJet(int max_jets){
        fillJets = max_jets;
        jetIndex = -1;
    }

    // functions to tweak the variable lists
    // save more or fewer parameters for a given selection
    void setVars(vector<TString> vars) {
        vars1D = vars;
        setMaps();
    }

    void add2DHist(const TString x, const TString y) {
        vars2D_x.push_back(x);
        vars2D_y.push_back(y);
        setMaps();
    }

    // some different sets of variables
    static vector<TString> getDefaultVars() {
        vector<TString> vars;
        vars.push_back("mu");
        vars.push_back("pt");
        vars.push_back("eta");
        vars.push_back("phi");
        vars.push_back("emfrac");
        vars.push_back("hecf");
        vars.push_back("hecq");
        vars.push_back("fmax");
        vars.push_back("fmaxi");
        vars.push_back("larq");
        vars.push_back("fch");
        vars.push_back("avglarq");
        vars.push_back("timing");
        vars.push_back("nege");
        vars.push_back("fchfmax");
        vars.push_back("n90const");
        vars.push_back("n_jet");
        vars.push_back("mht");
        vars.push_back("met");
        vars.push_back("met_phi");
  	return vars;
    }

    static vector<TString> getShortVars() {
        vector<TString> vars;
        vars.push_back("pt");
        vars.push_back("eta");
        vars.push_back("phi");
        vars.push_back("fmax");
        return vars;
    }

private:

    // helper class to apply variable-specific tweaks
    // ie imposing jet acceptance on ID variables

    TString getTreeKey(TString param) {
        //assume jet vars as default
        if (param == "lbn" || param == "bcid" || param == "run" || param == "mu" || param == "n_jet" || param == "mht" || param == "met" || param == "met_phi") return param;
        return "jet_a4em_" + param;
    }

    float getParamValue(TString param, int i = 0) {
        if (param.Contains("avglarq")) return evt->get(param, i) / 65535;
        else if (param.Contains("fch")) {
            if (fabs(evt->get(getTreeKey("eta"), i)) > 2.5) return -9999;
            if (param.Contains("fchfmax")) return evt->get(getTreeKey("fch"), i) / evt->get(getTreeKey("fmax"), i);
            return evt->get(param, i);
        }
        return evt->get(param, i);
    }

    void fillHists(float weight) {

        for (auto & h : hists) {
            TString treekey =  getTreeKey(h.first);
            if(treekey.Contains("n_jet")){ h.second->Fill(evt->get("n_jet"));}
		//cout << "Njet: " << evt->get("n_jet") << endl;}
	    else if (treekey.Contains("jet")) {
		if (jetIndex > 0 && evt->get("n_jet") >= jetIndex+1) h.second->Fill(getParamValue(treekey, jetIndex), weight);
                else for (int i = 0; i < min(evt->get("n_jet"), float(fillJets)); i++)
                        h.second->Fill(getParamValue(treekey, i), weight);
            } 
	    else h.second->Fill(getParamValue(treekey), weight);
        }
        for (auto & h : hists2D) {
            TString treekey_x = getTreeKey(get2Dx(h.first));
            TString treekey_y = getTreeKey(get2Dy(h.first));
            if (treekey_x.Contains("[0]") && treekey_y.Contains("[1]")) h.second->Fill(getParamValue(treekey_x(0,treekey_x.Length()-3), 0), getParamValue(treekey_y(0,treekey_y.Length()-3), 1), weight);
            else if (treekey_x.Contains("[1]") && treekey_y.Contains("[0]")) h.second->Fill(getParamValue(treekey_x(0,treekey_x.Length()-3), 1), getParamValue(treekey_y(0,treekey_y.Length()-3), 0), weight);
            else if (treekey_x.Contains("jet") || treekey_y.Contains("jet")) { 
                if (jetIndex > 0 && evt->get("n_jet") >= jetIndex+1)
                    h.second->Fill(getParamValue(treekey_x, jetIndex),
                                   getParamValue(treekey_y, jetIndex), weight);
                else for (int i = 0; i < min(evt->get("n_jet"),float(fillJets)); i++)
                        h.second->Fill(getParamValue(treekey_x, i),
                                       getParamValue(treekey_y, i), weight);
            } else    h.second->Fill(getParamValue(treekey_x), getParamValue(treekey_y), weight);
        }
    }

    void setMaps() {
        clearMaps();
        TString prefix = "h_";
        if (name.Length() > 0) prefix = name + "_";
        for (TString var : vars1D) setHist(prefix, var);
        
        for (unsigned int v = 0; v < vars2D_x.size(); v++) setHist(prefix, vars2D_x[v], vars2D_y[v]);
    }
};
