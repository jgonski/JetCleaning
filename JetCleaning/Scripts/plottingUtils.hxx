#include <iostream>

#include <TLatex.h>
#include <TH1.h>
#include <TH2.h>
#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TColor.h>
#include <TLegend.h>

#include "atlasstyle-00-03-05/AtlasStyle.C"

/******************************
* Common global definitions   *
*******************************/

TCanvas* c1;
Int_t MyPalette[100];
const float square_text_size = 0.04;

/*************************
* Utility Functions     *
**************************/

void setPalette() {
    Double_t r[]    = {1.0, 0.0, 0.0, 0.9, 1.0};
    Double_t g[]    = {1.0, 0.9, 0.3, 0.0, 0.8};
    Double_t b[]    = {1.0, 0.9, 0.9, 0.5, 0.0};
    Double_t stop[] = {0., .17, .40, .73, 1.0};
    Int_t FI = TColor::CreateGradientColorTable(5, stop, r, g, b, 100);
    for (int i = 0; i < 100; i++) MyPalette[i] = FI + i;
    gStyle->SetPalette(100, MyPalette);
}

void formatLegend(TLegend & legend){
    legend.SetFillColor(0);
    legend.SetLineColor(0);
    legend.SetTextSize(0.035);
}

void basicBeautification(){
    SetAtlasStyle();
    setPalette();
    gStyle->SetOptStat(0);
    c1 = new TCanvas("c1", "First canvas", 600, 600);
}

int colorInterpolate(int col1, int col2, float w = 0.5) {
    TColor*c1 = gROOT->GetColor(col1);
    TColor*c2 = gROOT->GetColor(col2);
    float r = c1->GetRed()  * (1 - w) + c2->GetRed() * w;
    float g = c1->GetGreen() * (1 - w) + c2->GetGreen() * w;
    float b = c1->GetBlue() * (1 - w) + c2->GetBlue() * w;
    float s2 = c1->GetSaturation() * (1 - w) + c2->GetSaturation() * w;
    float h = 0, s = 0, l = 0;
    TColor::RGB2HLS(r, g, b, h, l, s);
    TColor::HLS2RGB(h, l, (s2 + s) * 0.5, r, g, b);
    int c = TColor::GetColor(r, g, b);
    return c;
}


void savePlot(TString name) {
    name.ReplaceAll("/", "");
    name.ReplaceAll(" ", "_");
    c1->Print(name + ".gif");
    c1->Print(name + ".pdf");
    c1->Print(name + ".eps");
    //c1->Print(name + ".png");
    c1->SaveAs(name + ".C");
}

void drawText(TString text, float x, float y, float size = square_text_size, int color = kBlack) {
    if (text.Length() > 0) {
        TLatex l;
        l.SetNDC();
        l.SetTextColor(color);
        l.SetTextSize(size);
        l.DrawLatex(x, y, text);
    } else cout << "Warning: request to draw empty string" << endl;
}

void ATLASLabel(Double_t x, Double_t y, TString text, float size = 0.0, Color_t color = kBlack, float dx = 0, float dy = 0) {
    TLatex l;
    l.SetNDC();
    l.SetTextFont(72);
    l.SetTextColor(color);
    if (size > 0.00001)
        l.SetTextSize(size);

    double delx = 0.08 + size * 1.5 + dx;
    if (size < 0.00001) delx = 0.12;
    double dely = 0.06 - size * 0.25 + dy;

    l.DrawLatex(x, y, "ATLAS");
    if (text) {
        TLatex p;
        p.SetNDC();
        p.SetTextFont(42);
        p.SetTextColor(color);
        if (size > 0.00001)
            p.SetTextSize(size);
        p.DrawLatex(x + delx, y, "Preliminary");
        p.DrawLatex(x, y - dely, "#sqrt{s}=13 TeV  #scale[0.6]{#int} Ldt ~ 78 pb^{-1}"); //
        p.DrawLatex(x, y - 2 * dely, "Anti-k_{t} R = 0.4 Jets");
        //p.DrawLatex(x,y-2*dely,"May 2015");
        //p.DrawLatex(x, y - dely, "May 2015 commissioning, 13 TeV");
    }
}

bool hist_sort(TH1D* h1, TH1D* h2) {
    return h1->Integral() > h2->Integral();
}

float getMinRatio(TH1D*h1, TH1D*h2) {
    float min = 1.0;
    for (int i = 1; i < h1->GetNbinsX(); i++) {
        float n = h2->GetBinContent(i);
        float d = h1->GetBinContent(i);
        if (d > 0 && n / d < min) min = n / d;
    }
    return min;
}

void setTitleLabelSize(TH1D*h, float size) {
    h->GetYaxis()->SetTitleSize(size);
    h->GetXaxis()->SetTitleSize(size);
    h->GetXaxis()->SetLabelSize(size);
    h->GetYaxis()->SetLabelSize(size);
}

void setTitleLabelSize(TH2D*h, float size) {
    h->GetYaxis()->SetTitleSize(size);
    h->GetXaxis()->SetTitleSize(size);
    h->GetZaxis()->SetTitleSize(size);
    h->GetXaxis()->SetLabelSize(size);
    h->GetYaxis()->SetLabelSize(size);
    h->GetZaxis()->SetLabelSize(size);
}

void prepHistFor2D(TH2D * h) {
    h->SetMinimum(0);
    setTitleLabelSize(h, square_text_size);
    h->GetZaxis()->SetTitleOffset(1.35);
}

void prepCanvasFor2D() {
    c1->SetBottomMargin(0.12);
    c1->SetTopMargin(0.1);
    c1->SetLeftMargin(0.14);
    c1->SetRightMargin(0.18);
    c1->SetLogz(1);
    c1->SetLogy(0);
    c1->SetLogx(0);
    gStyle->SetPalette(100, MyPalette);
}

void dataStyle(TH1D* h, int color = kBlack, int marker = 20) {
    h->SetLineColor(color);
    h->SetMarkerColor(color);
    h->SetFillColor(0);
    h->SetMarkerStyle(marker);
    h->SetLineWidth(1.0);
}

void mcStyle(TH1D* h, int color) {
    h->SetMarkerStyle(0);
    h->SetFillColor(color);
    h->SetLineWidth(1.0);
}

/*************************
* Utility Classes        *
**************************/

class ColorScheme {
private:
    map<TString, int> sample_colors;
    int error_color;
public:
    ColorScheme() {
        default_colors();
        error_color = kCyan - 9;
        add_custom_color("qcd", kBlack);
    }
    int color_for(TString key) {
        if (sample_colors.count(key) > 0) return sample_colors[key];
        else return error_color;
    }
    void default_colors() {
        beach();
    }

    void add_custom_color(TString key, int color) {
        sample_colors[key] = color;
    }

    void add_custom_color(TString key, TString color) {
        if (color.Length() == 6) add_custom_color(key, TColor::GetColor("#" + color));
        else if (color.Length() == 7 && color[0] == '#') add_custom_color(key, TColor::GetColor(color));
        else add_custom_color(key, error_color);
    }

    //a color scheme without imagination
    void spectrum() {
        sample_colors["Znunu"] = kAzure + 10;
        sample_colors["Wlnu"] =  kRed - 4;
        sample_colors["ttst"] =  colorInterpolate(kSpring - 4, kTeal + 2);
        sample_colors["Zll"] =   kOrange;
        sample_colors["VV"] =    kBlue + 2;
    }

    // an inofennsive color scheme
    void soft_spectrum() {
        sample_colors["Znunu"] = TColor::GetColor("#63d6ff");
        sample_colors["Wlnu"] =  TColor::GetColor("#ff5d69");
        sample_colors["ttst"] =  TColor::GetColor("#8bf687");
        sample_colors["Zll"] =   TColor::GetColor("#ffd74a");
        sample_colors["VV"] =    TColor::GetColor("#6335b1");
        sample_colors["qcd"] =   TColor::GetColor("#232232");
    }

    // a warm and relaxing color scheme
    void beach() {
        sample_colors["Znunu"] = TColor::GetColor("#2796c0");
        sample_colors["Wlnu"] =  TColor::GetColor("#54d1d0");
        sample_colors["ttst"] =  TColor::GetColor("#fce98c");
        sample_colors["Zll"] =   TColor::GetColor("#f33c4f");
        sample_colors["VV"] =    TColor::GetColor("#f6a702");
        sample_colors["qcd"] =   TColor::GetColor("#4b5880");
    }

    // a color scheme that you would use anyway
    void poison() {
        sample_colors["Znunu"] = TColor::GetColor("#fbf89d");
        sample_colors["Wlnu"] =  TColor::GetColor("#afeead");
        sample_colors["ttst"] =  TColor::GetColor("#47a2a9");
        sample_colors["Zll"] =   TColor::GetColor("#613064");
        sample_colors["VV"] =    TColor::GetColor("#b7376d");
        sample_colors["qcd"] =   TColor::GetColor("#f46996");
    }

    // a ho-hum color scheme
    void in_a_room() {
        sample_colors["Znunu"] = TColor::GetColor("#43378f");
        sample_colors["Wlnu"] =  TColor::GetColor("#47b1c8");
        sample_colors["ttst"] =  TColor::GetColor("#fadc8d");
        sample_colors["Zll"] =   TColor::GetColor("#aa343d");
        sample_colors["VV"] =    TColor::GetColor("#ef845d");
    }

};

/*************************
* Plotting Functions     *
**************************/

void draw2D(TH2D* h, TString xtitle = "", TString ytitle = ""){

    if(h){
    prepCanvasFor2D();
    prepHistFor2D(h);
    if (xtitle.Length() > 0) h->GetXaxis()->SetTitle(xtitle);
    if (ytitle.Length() > 0) h->GetYaxis()->SetTitle(ytitle);
    h->Draw("colz");
    }else cout << "hist not valid" << endl;
}
void draw2D_same(TH2D* h1,TH2D* h2, TString xtitle = "", TString ytitle = ""){

    if(h1 && h2){
    prepCanvasFor2D();
    prepHistFor2D(h1);
    prepHistFor2D(h2);
    if (xtitle.Length() > 0) h1->GetXaxis()->SetTitle(xtitle);
    if (ytitle.Length() > 0) h1->GetYaxis()->SetTitle(ytitle);
    h1->Draw("colz");
    h2->Draw("colz same");
    }else cout << "hist not valid" << endl;
}

