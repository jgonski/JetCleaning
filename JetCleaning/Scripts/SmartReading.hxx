/***********************************************************
 Classes & Functions to efficiently & intelligently read TTrees
 Valerio Ippolito - Harvard University
 Emma Tolley - Harvard University

 The classes are designed to be as general as possible
 Ntuple-specific functionality should be implemented
 via sublasses
************************************************************/

/*********************************
Valerio's Branch reading classes
**********************************/

// template classes to provide smart reading of TTree branches
// currently supported for:
//   - primitive types
//   - STL vectors
//
// the idea is to save the user from having to manually call
// TBranch::GetEntry each time a variable is read
// in this way, only variables which are actually needed are
// read from the input file, the first time they are called

#ifndef __SmartReader_h__
#define __SmartReader_h__


#include <set>
#include <TTree.h>

#include <iostream>

class SmartReaderBase {
    // base class
public:
    void setName(TString name) {
        m_name = name;
    }
    void setCurrentEntry(Long64_t *entry) {
        m_currentEntry = entry;
    }
    Long64_t getCurrentEntry() {
        return *m_currentEntry;
    }

    SmartReaderBase() : m_name(""), m_tree(nullptr), m_currentEntry(nullptr),  m_branch(nullptr) {}
    SmartReaderBase(TString name) : m_name(name), m_tree(nullptr), m_currentEntry(nullptr), m_branch(nullptr) {}
    virtual ~SmartReaderBase() {}
    virtual void attachToTree(TTree *tree, Long64_t *entry, TString newname) = 0;

protected:
    TString m_name;
    TTree *m_tree;
    Long64_t *m_currentEntry;
    TBranch *m_branch;
};

template <typename Type>
class SmartReader : public SmartReaderBase {
    // standard template class, to be used with primitive types
    // (ROOT takes a pointer to Type for SetBranchAddress)
    //
    // usage:
    //   SmartReader<Int_t> run("run");
    //   Long64_t entry(0);
    //   run.attachToTree(tree, &entry);
    //   for (Long64_t i = 0; i < tree.GetEntries(); i++) {
    //     entry = i;
    //
    //     if (run() == 216000) { // do something }
    //   }
public:
    typedef Type& result_type;
    typedef const Type& const_result_type;

    SmartReader() {}
    SmartReader(TString name) : SmartReaderBase(name) {}
    ~SmartReader() { }
    virtual void attachToTree(TTree *tree, Long64_t *entry, TString newname = "") {
        if (newname != "") setName(newname);
        m_tree = tree;
        m_tree->SetBranchAddress(m_name, &m_var, &m_branch);
        m_currentEntry = entry;
    }

    result_type operator()() {
        if (m_branch->GetReadEntry() != *m_currentEntry) {
            m_branch->GetEntry(*m_currentEntry);
        }

        return m_var;
    }
    const_result_type operator()() const {
        if (m_branch->GetReadEntry() != *m_currentEntry) {
            m_branch->GetEntry(*m_currentEntry);
        }

        return m_var;
    }

private:
    Type m_var;
};

template <typename Type>
class SmartReader<Type*> : public SmartReaderBase {
    // template class with pointer input, to be used with STL vectors
    // (ROOT takes a pointer to pointer to Type for SetBranchAddress)
    //
    // usage:
    //   SmartReader<std::vector<Float_t>*> jet_pt("jet_pt");
    //   // note the asterisk above...
    //   Long64_t entry(0);
    //   jet_pt.attachToTree(tree, &entry);
    //   for (Long64_t i = 0; i < tree.GetEntries(); i++) {
    //     entry = i;
    //
    //     if (jet_pt()->size() > 0 && jet_pt()->at(0) > 150000) { // do something }
    //   }
public:
    typedef Type* result_type;
    typedef const Type* const_result_type;

    SmartReader() : m_var(nullptr) { }
    SmartReader(TString name) : SmartReaderBase(name), m_var(nullptr)  {}
    ~SmartReader() {
        SafeDelete(m_var);
    }
    virtual void attachToTree(TTree *tree, Long64_t *entry, TString newname = "") {
        if (newname != "") setName(newname);
        m_tree = tree;
        m_tree->SetBranchAddress(m_name, &m_var, &m_branch);
        m_currentEntry = entry;
    }

    result_type operator()() {
        if (m_branch->GetReadEntry() != *m_currentEntry) {
            m_branch->GetEntry(*m_currentEntry);
        }

        return m_var;
    }
    const_result_type operator()() const {
        if (m_branch->GetReadEntry() != *m_currentEntry) {
            m_branch->GetEntry(*m_currentEntry);
        }

        return m_var;
    }

private:
    Type *m_var;
};

/***********************************************
* TTree TBranch utility functions & classes    *
************************************************/

enum {bFLOAT, bINT, bVECFLOAT, bVECINT} BranchType;

//Utility class to help with reading branch info from a TTree
class BranchInfo {
public:
    TString name;
    TString className;
    BranchInfo(TBranch * b) {
        name = b->GetName();
        className = getBranchClassName(b);
    }
    static TString getBranchClassName(TBranch * b) {
        TString title = b->GetTitle();
        TString className = b->GetClassName();
        if (className.Length() == 0) className = title(title.Index('/') + 1, 1);
        return className;
    }
};

// Modification of V's SmartReader class
// The template class model was discarded in favor
//   of a data-structure friendly class
// Branch type needs to be set using TBranch info
// Only float, int, vector<float>, and vector<int> are currently supported
class GeneralSmartReader: public SmartReaderBase {
public:
    GeneralSmartReader() {
        reset();
    }
    GeneralSmartReader(TString name) : SmartReaderBase(name) {
        reset();
    }
    ~GeneralSmartReader()   {
        if (fvecdata) delete fvecdata;
        if (ivecdata) delete ivecdata;
    }

    // Ideally called once when object is constructed
    // to determine branch type
    void setType(TString type) {
        //TODO: throw exceptions like confetti
        if (type == "I") bType = bINT;
        else if (type == "F") bType = bFLOAT;
        else if (type == "vector<float>") bType = bVECFLOAT;
        else if (type == "vector<int>") bType = bVECINT;
        else {
            cout << "type " << type << " not recognized" << endl;
            bType = -1;
        }
    }

    // Call upon reading a new ttree
    void attachToTree(TTree *tree, Long64_t *entry, TString newname = "") {
        if (bType == -1) setType(BranchInfo::getBranchClassName(tree->GetBranch(newname)));
        if (newname != "") setName(newname);
        m_tree = tree;
        switch (bType) {
            case bFLOAT:
                m_tree->SetBranchAddress(m_name, &fdata,    &m_branch);
                break;
            case bINT:
                m_tree->SetBranchAddress(m_name, &idata,    &m_branch);
                break;
            case bVECFLOAT:
                m_tree->SetBranchAddress(m_name, &fvecdata, &m_branch);
                break;
            case bVECINT:
                m_tree->SetBranchAddress(m_name, &ivecdata, &m_branch);
                break;
            default:
                cout << m_name << " reader type not supported or not set correctly." << endl;
        }
        m_currentEntry = entry;
    }

    // get size of vector data
    int size() {
        update();
        switch (bType) {
            case bFLOAT:
            case bINT:
                return 0;
            case bVECFLOAT:
                return fvecdata->size();
            case bVECINT:
                return ivecdata->size();
            default:
                return -9999;
        }
    }

    // get pointer to float vector data
    vector<float> * getFloatVec() {
        update();
        if (bType == bVECFLOAT) return fvecdata;
        else return 0;
    }

    // get int value, float value, or vector value at i
    float get(int i = 0) {
        update();
        switch (bType) {
            case bFLOAT:
                return fdata;
            case bINT:
                return idata;
            case bVECFLOAT:
                if (i < int(fvecdata->size()))
                    return fvecdata->at(i);
                else return -9999;
            case bVECINT:
                if (i < int(ivecdata->size()))
                    return ivecdata->at(i);
                else return -9999;
            default:
                return -9999;
        }
    }
private:

    int bType;
    vector<float> * fvecdata;
    vector<int> * ivecdata;
    float fdata;
    int idata;

    void reset() {
        bType = -1;
        fvecdata = 0;
        ivecdata = 0;
        fdata = -9999;
        idata = -9999;
    }

    void update() {
        if (m_branch->GetReadEntry() != *m_currentEntry) {
            m_branch->GetEntry(*m_currentEntry);
        }
    }
};

// a class to automatically manage all of the
// smartreaders for a given ttree
// WARNING: does not work on multi-file tchains
// if you hate maps, add pointers to map entries
// to bypass the map lookup
// cutflow functions should be added as
// subclass functions
class TreeEvtInfo {
protected:
    Long64_t currentEntry;
    bool branches_initialized;

    //dynamically generated branch map
    map<TString, GeneralSmartReader> branches;

public:
    TreeEvtInfo() {
        branches_initialized = false;
    }
    TreeEvtInfo(TTree * tree): TreeEvtInfo() {
        branches_initialized = false;
        attachToTree(tree);
    }
    
    virtual bool passCut(TString cut){return false;}

    // this function needs to be called once per new TTree
    void attachToTree(TTree * t) {
        if (t) {
            //TODO: add error checking for when initialized branches
            //      don't match up with ttree branches
            //initialize branch map if not done so already
            if (!branches_initialized) initBranches(t);
            //attach each branch reading object to the ttree branch
            for (auto &b : branches) b.second.attachToTree(t, &currentEntry, b.first);
        } else cout << "ERROR: null tree in attachToTree" << endl;
    }

    // this function needs to be called for every new entry of the TTree
    void setEntry(Long64_t i) {
        currentEntry = i;
    }

    // public accessor to get variables from the branch map
    // TODO: ??? exceptions ???
    float get(TString key, int i = 0) {
        if (branches.count(key) > 0) {
            return branches[key].get(i);
        }
        cout << key << " not mapped in branches" << endl;
        return -9999;
    }

    // get list of branch objects for a given ttree
    // BranchInfo is a helper class that stores the
    // branch name and stored type name
    static vector<BranchInfo> getListOfBranches(TTree *t) {
        vector<BranchInfo> branchNames;
        if (t) {
            TObjArray *branches = t->GetListOfBranches();
            for (int i = 0; i < branches->GetEntries(); ++i) {
                TBranch * b = ((TBranch*)branches->At(i));
                branchNames.push_back(BranchInfo(b));
            }
        } else cout << "ERROR: null TTree" << endl;
        return branchNames;
    }

protected:
    virtual void initBranches(TTree * t) {
        //construct a new smartreader object for each branch found in the tree
        branches.clear();
        for (BranchInfo b : getListOfBranches(t))
            if (!b.name.Contains("PRWHash")) branches[b.name].setType(b.className);
    }
};

/**********************************************
Classes & functions for Histogram management
***********************************************/

//utility class to store binning info
class Binning {
public:
    float high;
    float low;
    int n;
    Binning(int in_n = 50, float in_low = -0.1, float in_high = 1.2) {
        set(in_n, in_low, in_high);
    }
    void set(int in_n, float in_low, float in_high) {
        n = in_n;
        low = in_low;
        high = in_high;
    }
};

// a base class for filling histograms on a tree
// for all events passing a cutflow function
//
// subclasses must:
//   1) set which variables to read from TreeEvtInfo * evt with setMaps()
//   2) define how to fill the various histograms with fillHists()
//   3) set the preferred binning in the subclass constructor
//
// TODO: protect map reading
// TODO: some sort of pointer to a cutflow object to speed things up?? (janky-ass caching)
//       but the code is pretty fast as-is
class SelectionHists {
    private:
    TString title;
protected:
    // selection function
    bool (*pass_selection)(TreeEvtInfo *);

    // pointer to binning convention function (for easy switching)
    Binning(*internal_binning)(TString);

    // pointer to smart ttree reader to get branch entries
    TreeEvtInfo * evt;

    // self evident
    TString name;

public:
    //histograms filled by the class
    map<TString, TH1D*> hists;
    map<TString, TH2D*> hists2D;

    SelectionHists(TreeEvtInfo * in_evt, bool (*in_f)(TreeEvtInfo *), TString in_name = "") {
        evt = in_evt;
        pass_selection = in_f;
        name = in_name;
        title = "";
        internal_binning = defaultBinning;
    }
    
    void SetTitle(TString in_title){
        title = in_title;
    }

    // set binning scheme for histograms
    void setBinning(Binning(*new_binning)(TString)) {
        internal_binning = new_binning;
    }

    // function to fill all histograms passing the selection function
    virtual void fill(float weight = 1.) {
        if (pass_selection(evt)) fillHists(weight);
    }

    void setDir(TFile * f) {
        for (auto &h : hists)   h.second->SetDirectory(f);
        for (auto &h : hists2D) h.second->SetDirectory(f);
    }

    void write() {
        for (auto &h : hists)   h.second->Write();
        for (auto &h : hists2D) h.second->Write();
    }
   
    void toDelete() {
    	hists.clear();
	hists2D.clear();
    }
    virtual void clearMaps() {
        for (auto & h : hists)   delete h.second;
        for (auto & h : hists2D) delete h.second;
        hists.clear();
        hists2D.clear();
    }


protected:

    TString make2Dkey(TString x, TString y) {
        return x + "." + y;
    }

    TString get2Dx(TString key) {
        return key(0, key.Index('.'));
    }

    TString get2Dy(TString key) {
        return key(key.Index('.') + 1, key.Length());
    }


    //automatically define 1D histogram based on parameter
    virtual void setHist(TString prefix, TString param) {
        Binning b = internal_binning(param);
        hists[param] = new TH1D(prefix + param, (title.Length() > 0 ? title : prefix + param), b.n, b.low, b.high);
    }
    //automatically define 2D histogram based on parameter
    virtual void setHist(TString prefix, TString param1, TString param2) {
        Binning b1 = internal_binning(param1);
        Binning b2 = internal_binning(param2);
        hists2D[ make2Dkey(param1, param2)] = new TH2D(prefix + param1 + "_" + param2,
                                                       (title.Length() > 0 ? title : prefix + param1 + "_" + param2),
                                                       b1.n, b1.low, b1.high,
                                                       b2.n, b2.low, b2.high);
    }
private:

    // function in which histograms should be defined
    virtual void setMaps() = 0;

    // function in which histograms should be filled
    virtual void fillHists(float weight) = 0;

    // dummy binning function for the constructor
    static Binning defaultBinning(TString param = "") {
        Binning b;
        return b;
    }
};

// a function to print a progress bar
// will definitely mess up other print statements
void printProgress(int n, int i) {
    if (n > 1000 && i % (n / 1000) == 0) {
        cerr << "\t" << ceil(i * 100.0 / n) << "%" << "\t|";
        for (int k = 0; k < 10; k++) {
            if (k <= floor(i * 10.0 / n)) cerr << "-";
            else cerr << " ";
        }
        cerr << "| \t" << i << "/" << n;
        cerr << "\r";
    }
}

#endif
