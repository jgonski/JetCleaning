#include "plottingUtils.hxx"
#include "dictionary.hxx"
#include "cleaningUtils.hxx"

TLegend legend(0.22,0.75,0.57,0.9);

void compareHists(TH1D*h1, TH1D* h2, TString xtitle, TString h1_name, TString h2_name, bool ratio = true){
    
    c1 = new TCanvas("c1", "First canvas", 600, 600);

    dataStyle(h1, kAzure+3);
    dataStyle(h2, kRed-4);
    h1->SetLineWidth(3);
    h2->SetLineWidth(3);
    h1->SetMaximum(max(h1->GetMaximum(), h2->GetMaximum())*100);
    h1->GetXaxis()->SetTitle(xtitle);
    setTitleLabelSize(h1, 0.035);
    h1->Draw("hist");
    h2->Draw("hist+same");
    
    legend.Clear();
    legend.AddEntry(h1, h1_name);
    legend.AddEntry(h2, h2_name);
    legend.Draw();
}

void drawHist(TH1D*h1, TH1D*h2, TString xtitle, TString h1_name, TString h2_name, bool ratio = true){
   
    //TH1::SetDefaultSumw2(kTRUE); 
    if(ratio){
        TPad *p1 = new TPad("p1", "p1", 0.0, 0.3, 1.0, 1.0);
        p1->Draw();
        p1->cd();
        gPad->SetLogy(1);
    }else{
        c1->SetLogy(1);
    }
    
    //dataStyle(h2, kRed-4);
    dataStyle(h1, kViolet+9);
    mcStyle(h2, kCyan-7);
    //h1->SetLineColor(kAzure);
    //h1->SetLineColor(kViolet+8);
    //h2->SetLineColor(kRed-4);
    //h2->SetLineColor(kGreen+2);
    h1->SetLineWidth(2);
    h2->SetLineWidth(2);
   
    h1->Scale(1./h1->Integral());
    h2->Scale(1./h2->Integral());
    //h1->Scale(-1.);
    //h2->Scale(-1.);
		

    //if (xtitle.Contains("Phi"))
    //    h1->SetMaximum(max(h1->GetMaximum(), h2->GetMaximum())*10);
    //else
    //    h1->SetMaximum(max(h1->GetMaximum(), h2->GetMaximum())*100);
    //if (xtitle.Contains("p_{T}") && xtitle.Contains("Leading") )
    h1->SetMinimum(1e-5);
    h1->SetMaximum(1);
    h1->GetYaxis()->SetTitle("Events [a.u.]");
    h1->GetXaxis()->SetTitle(xtitle);
    setTitleLabelSize(h1, 0.035);
    h1->Draw("hist");
    //h1->Draw("hist+same");
    
    legend.Clear();
    legend.AddEntry(h1, h1_name, "l");
    legend.AddEntry(h2, h2_name,  "f");
    //legend.Draw();
    
    if(ratio){
        
    TH1::SetDefaultSumw2(kTRUE); 
        TPad *p2 = new TPad("p2", "p2", 0.0, 0.03, 1.0, 0.30);
        c1->cd();
        p2->Draw();
        p2->cd();
        gPad->SetGridy();

        TH1D *hr = (TH1D*) h1->Clone();
        dataStyle(hr, kBlue+1);
        hr->Divide(h2);
        hr->Draw("e1");
        hr->SetMinimum(0.0);
        hr->SetMaximum(2.0);
        hr->GetYaxis()->SetTitle("Data/Reference");
        setTitleLabelSize(hr, 0.12);
        hr->GetXaxis()->SetTitleOffset(1.15);
        hr->GetYaxis()->SetTitleOffset(0.5);
        hr->GetYaxis()->SetRangeUser(0.0, 3.0);
        hr->GetYaxis()->SetNdivisions(5);
        gPad->SetBottomMargin(0.32);

        c1->cd();
    }
}

void drawHist3pt(TH1D*h1, TH1D*h2, TH1D*h3, TString xtitle, TString h1_name, TString h2_name, TString h3_name, bool ratio = true){
    
    if(ratio){
        TPad *p1 = new TPad("p1", "p1", 0.0, 0.3, 1.0, 1.0);
        p1->Draw();
        p1->cd();
        gPad->SetLogy(1);
    }else{
        c1->SetLogy(1);
    }
    
    dataStyle(h1, kBlue+1);
    dataStyle(h2, kPink);
    mcStyle(h3, kOrange-2);
    //h1->SetLineColor(kAzure);
    //h1->SetLineColor(kViolet+8);
    //h2->SetLineColor(kRed-4);
    //h2->SetLineColor(kGreen+2);
    h1->SetLineWidth(2);
    h2->SetLineWidth(2);
    h3->SetLineWidth(2);
   
    h1->Sumw2();
    h3->Sumw2();
    h1->Scale(1./h1->Integral());
    h2->Scale(1./h2->Integral());
    h3->Scale(1./h3->Integral());
    //h1->Scale(-1.);
    //h2->Scale(-1.);
		

    if (xtitle.Contains("Phi"))
        h3->SetMaximum(10);
    else
        h3->SetMaximum(max(h1->GetMaximum(), h2->GetMaximum())*100);
    //if (xtitle.Contains("p_{T}") && xtitle.Contains("Leading") )
    //h1->SetMinimum(1e-12);
    h3->SetMaximum(h1->GetMaximum()*100);
    h3->GetXaxis()->SetTitle(xtitle);
    setTitleLabelSize(h3, 0.035);
    h3->Draw("hist");
    h1->Draw("hist+same");
    
    TLatex l;
    l.SetTextFont(132); 
    l.SetNDC(); 
    l.SetTextSize(0.04);
    l.SetTextFont(42);
    l.DrawLatex(0.65, 0.86,"#bf{#it{ATLAS}} Internal");
    TLatex l2;
    l2.SetTextFont(132); 
    l2.SetNDC(); 
    l2.SetTextSize(0.04);
    l2.SetTextFont(42);
    l2.DrawLatex(0.65, 0.81,"#sqrt{s} = 13 TeV");
    TLatex l3;
    l3.SetTextFont(132); 
    l3.SetNDC(); 
    l3.SetTextSize(0.04);
    l3.SetTextFont(42);
    l3.DrawLatex(0.65, 0.76,"Anti-k_{t} R = 0.4 PF Jets");
    
    legend.Clear();
    legend.AddEntry(h1, h1_name);
    legend.AddEntry(h3, h3_name);
    legend.Draw();

    if(ratio){
        
        TPad *p2 = new TPad("p2", "p2", 0.0, 0.03, 1.0, 0.30);
        c1->cd();
        p2->Draw();
        p2->cd();
        gPad->SetGridy();

        TH1D *hr = (TH1D*) h1->Clone();
        dataStyle(hr, kBlue+1);
        hr->Divide(h3);
        hr->Draw("e1");
        hr->SetMinimum(0.0);
        hr->SetMaximum(2.0);
        hr->GetYaxis()->SetTitle("Data/Reference");
        setTitleLabelSize(hr, 0.12);
        hr->GetXaxis()->SetTitleOffset(1.15);
        hr->GetYaxis()->SetTitleOffset(0.5);
        hr->GetYaxis()->SetRangeUser(0.0, 3.0);
        hr->GetYaxis()->SetNdivisions(5);
        gPad->SetBottomMargin(0.32);

        c1->cd();
    }
}
void drawHist3(TH1D*h1, TH1D*h2, TH1D*h3, TString xtitle, TString h1_name, TString h2_name, TString h3_name, bool ratio = true){
    
    if(ratio){
        TPad *p1 = new TPad("p1", "p1", 0.0, 0.3, 1.0, 1.0);
        p1->Draw();
        p1->cd();
        gPad->SetLogy(1);
    }else{
        c1->SetLogy(1);
    }
    
    dataStyle(h1, kBlue+1);
    dataStyle(h2, kPink);
    mcStyle(h3, kOrange-2);
    //dataStyle(h1, kTeal-1);
    //dataStyle(h2, kPink+7);
    //mcStyle(h3, kAzure+8);
    h1->SetLineWidth(2);
    h2->SetLineWidth(2);
    h3->SetLineWidth(2);
   
    //h1->Sumw2();
    //h2->Sumw2();
    //h3->Sumw2();
    h1->Scale(1./h1->Integral());
    h2->Scale(1./h2->Integral());
    h3->Scale(1./h3->Integral());
    //h1->Scale(-1.);
    //h2->Scale(-1.);
		

    if (xtitle.Contains("Phi"))
        h3->SetMaximum(10);
    else if (xtitle.Contains("fmax"))
        h3->SetMaximum(max(h1->GetMaximum(), h2->GetMaximum())*1000);
    else
        h3->SetMaximum(max(h1->GetMaximum(), h2->GetMaximum())*500);
    //if (xtitle.Contains("p_{T}") && xtitle.Contains("Leading") )
    if(xtitle.Contains("Avg")) h3->SetMinimum(0.00001);
    //h3->SetMaximum(h1->GetMaximum()*500);
    h3->GetYaxis()->SetTitle("Arbitrary Units");
    h3->GetXaxis()->SetTitle(xtitle);
    setTitleLabelSize(h3, 0.035);
    h3->Draw("hist");
    h1->Draw("same");
    h2->Draw("hist+same");
    
    TLatex l;
    l.SetTextFont(132); 
    l.SetNDC(); 
    l.SetTextSize(0.04);
    l.SetTextFont(42);
    l.DrawLatex(0.65, 0.86,"#bf{#it{ATLAS}} Internal");
    TLatex l2;
    l2.SetTextFont(132); 
    l2.SetNDC(); 
    l2.SetTextSize(0.04);
    l2.SetTextFont(42);
    l2.DrawLatex(0.65, 0.81,"#sqrt{s} = 13 TeV");
    TLatex l3;
    l3.SetTextFont(132); 
    l3.SetNDC(); 
    l3.SetTextSize(0.04);
    l3.SetTextFont(42);
    l3.DrawLatex(0.65, 0.76,"Anti-k_{t} R = 0.4 PF Jets");
    
    legend.Clear();
    legend.AddEntry(h1, h1_name);
    legend.AddEntry(h2, h2_name,  "l");
    legend.AddEntry(h3, h3_name);
    legend.Draw();
    
    if(ratio){
        
        TPad *p2 = new TPad("p2", "p2", 0.0, 0.03, 1.0, 0.30);
        c1->cd();
        p2->Draw();
        p2->cd();
        gPad->SetGridy();

        TH1D *hr = (TH1D*) h1->Clone();
        dataStyle(hr, kBlue+1);
        hr->Divide(h3);
        hr->Draw("e1");
        hr->SetMinimum(0.0);
        hr->SetMaximum(2.0);
        hr->GetYaxis()->SetTitle("Data/Reference");
        setTitleLabelSize(hr, 0.12);
        hr->GetXaxis()->SetTitleOffset(1.15);
        hr->GetYaxis()->SetTitleOffset(0.5);
        hr->GetYaxis()->SetRangeUser(0.0, 3.0);
        hr->GetYaxis()->SetNdivisions(5);
        gPad->SetBottomMargin(0.32);

        c1->cd();
    }
}


void stackHists(TH1D*h1, TH1D* h2, TH1D* h3, TString xtitle){
    
    h1->SetMaximum(h1->GetMaximum()*1.5);
    h1->GetXaxis()->SetTitle(xtitle);
    setTitleLabelSize(h1, 0.035);
    mcStyle(h1,TColor::GetColor("#f7c930"));
    mcStyle(h2,TColor::GetColor("#3ed967"));
    mcStyle(h3,TColor::GetColor("#22a7b3"));
    
    h1->Draw("hist");
    h2->Draw("hist+same");
    h3->Draw("hist+same");
    
    legend.Clear();
    //legend.AddEntry(h1, "No Cleaning");
    //legend.AddEntry(h2, "Loose [NO low-EMF]");
    //legend.AddEntry(h3, "Run 1 Medium [NO low-EMF]");
    legend.AddEntry(h1, "No Cleaning");
    legend.AddEntry(h2, "Loose");
    legend.AddEntry(h3, "Tight");
    legend.Draw();
    
    gPad->RedrawAxis();
}

void ratioPlot(TH1D*hdata, TH1D* hmc, TString xtitle){
    
    c1 = new TCanvas("c1", "First canvas", 600, 600);

    TPad *p1 = new TPad("p1", "p1", 0.0, 0.3, 1.0, 1.0);
    p1->Draw();
    p1->cd();
    gPad->SetLogy(1);

    hmc->Scale(hdata->Integral()/hmc->Integral());
    hmc->SetMaximum(max(hmc->GetMaximum(), hdata->GetMaximum())*100);
    hmc->GetXaxis()->SetTitle(xtitle);
    setTitleLabelSize(hmc, 0.05);
    mcStyle(hmc,kAzure+10);
    dataStyle(hdata);
    
    hmc->Draw("hist");
    hdata->Draw("e1+same");
    
    legend.Clear();
    //legend.AddEntry(h1, "No Cleaning");
    //legend.AddEntry(h2, "Loose [NO low-EMF]");
    //legend.AddEntry(h3, "Run 1 Medium [NO low-EMF]");
    legend.AddEntry(hdata, "Data");
    legend.AddEntry(hmc, "Pythia8 Dijet");
    legend.Draw();
    
    TPad *p2 = new TPad("p2", "p2", 0.0, 0.03, 1.0, 0.30);
    c1->cd();
    p2->Draw();
    p2->cd();
    gPad->SetGridy();

    TH1D *hr = (TH1D*) hdata->Clone();
    hr->Divide(hmc);
    hr->Draw("e1");
    hr->SetMinimum(0.0);
    hr->SetMaximum(2.0);
    hr->GetYaxis()->SetTitle("Data/MC");
    setTitleLabelSize(hr, 0.12);
    hr->GetXaxis()->SetTitleOffset(1.15);
    hr->GetYaxis()->SetTitleOffset(0.5);
    hr->GetYaxis()->SetRangeUser(0.0, 2.0);
    hr->GetYaxis()->SetNdivisions(5);
    gPad->SetBottomMargin(0.32);

    c1->cd();
}

void make_indiv(TH1D* j15,TH1D* j25,TH1D* j35,TH1D* j60,TH1D* j150,TH1D* j260,TH1D* j380, TString saveAs, TString xtitle){ 

	j15->SetLineWidth(2);
	j25->SetLineWidth(2);
	j35->SetLineWidth(2);
	j60->SetLineWidth(2);
	j150->SetLineWidth(2);
	j260->SetLineWidth(2);
	j380->SetLineWidth(2);

	j25->SetLineColor(2);
	j35->SetLineColor(kGreen+2);
	j60->SetLineColor(4); 
  	j150->SetLineColor(kMagenta); 
	j260->SetLineColor(kYellow+1); 
	j380->SetLineColor(kViolet+2);

	j15->Draw("hist");
	c1->SetLogy(); 
	j25->Draw("hist+same");
	j35->Draw("hist+same");
	j60->Draw("hist+same");
	j150->Draw("hist+same");
	j260->Draw("hist+same");
	j380->Draw("hist+same");
        j15->GetYaxis()->SetRangeUser(10000,100000000000);

   	/*TH1D* htempLo = new TH1D("htempLo","",50, 0, 500);
	TH1D* htempHi = new TH1D("htempHi","",50, 0, 500);

	htempLo->Add(j15); htempLo->Add(j25); htempLo->Add(j35); //htempLo->Add(j60); 
	htempHi->Add(j60); htempHi->Add(j150);htempHi->Add(j260);htempHi->Add(j380);

	htempLo->SetLineColor(2);

	htempLo->Draw("hist");
	htempLo->GetXaxis()->SetTitle(xtitle);
  	htempLo->GetYaxis()->SetRangeUser(10, 1000000000000);
	c1->SetLogy();
	htempHi->Draw("hist+same");*/ 

	legend.Clear();
	//legend.AddEntry(htempLo, "Random seed", "l");
	//legend.AddEntry(htempHi, "L1 seed", "l");
	legend.AddEntry(j15, "HLT_j15", "l");
	legend.AddEntry(j25, "HLT_j25", "l");
	legend.AddEntry(j35, "HLT_j35", "l");
	legend.AddEntry(j60, "HLT_j60", "l");
	legend.AddEntry(j150, "HLT_j150", "l");
	legend.AddEntry(j260, "HLT_j260", "l");
	legend.AddEntry(j380, "HLT_j380", "l");
	legend.Draw(); 

	c1->SaveAs("indiv_" + saveAs + ".png");
  	

} 


TH1D* addHist(TH1D*h1, TH1D*h2, TString param){

    TH1D*htemp;

    if (param == "pt") htemp = new TH1D("htemp","",120, 0, 1200);
    else if (param == "mu") htemp = new TH1D("htemp","",50, 0, 50);
    else if (param == "eta") htemp = new TH1D("htemp","",25, -5, 5);
    else if (param == "phi") htemp = new TH1D("htemp","",28, -3.14159, 3.14159);
    else if (param == "emfrac" || param == "hecf" || param == "hecq" || param == "avglarq" || param == "larq")
        htemp = new TH1D("htemp","",50, -0.1, 1.1);
    else if (param == "fmax") htemp = new TH1D("htemp","",50, -0.1, 1.2);
    else if (param == "fmaxi") htemp = new TH1D("htemp","",24, 0, 24);
    else if (param == "fch") htemp = new TH1D("htemp","",50, -0.1, 2.0);
    else if (param == "fchfmax") htemp = new TH1D("htemp","",50, -0.1, 6.0);
    else if (param.Contains( "timing")) htemp = new TH1D("htemp","",40, -35, 35);
    else if (param == "nege") htemp = new TH1D("htemp","",50, -100, 50);
    else if (param == "lbn") htemp = new TH1D("htemp","",200, 0, 600);
    else if (param == "bcid") htemp = new TH1D("htemp","",50, 0, 3500);
    else if (param == "run") htemp = new TH1D("htemp","",50, 295000, 302000);
    else if (param == ("n90const")) htemp = new TH1D("htemp","",30, 0, 30);
    else if (param == "n_jet") htemp = new TH1D("htemp","",10, -0.5, 9.5);
    else if (param == "mht") htemp = new TH1D("htemp","",100, 0, 500);

    htemp->GetXaxis()->SetTitle(param);
    htemp->Add(h1); htemp->Add(h2); 
    htemp->Draw("hist"); 
    c1->SaveAs("exot2_addRedo_" + param);

    return htemp;
}


void make_eff_plot(TH1D* h_den, TH1D* h_num,  TH1D* h_num2, TString xtitle, TString saveAs1, TString saveAs2){
    c1 = new TCanvas("c1", "First canvas", 800, 600);
    //c1->SetLogx();
    h_num->Divide(h_den);
    h_num2->Divide(h_den);
    dataStyle(h_num2, kOrange-3,kFullTriangleUp);
    dataStyle(h_num, kBlue);
    h_num->GetXaxis()->SetTitle(xtitle);
 
    if(saveAs2.Contains("eta")){
	std::cout << "Eta plot" << std::endl; 
	h_num->GetXaxis()->SetRangeUser(-5, 5);
	 //h_num->GetYaxis()->SetRangeUser(0.95, 1.05);
    }
    else    h_num->GetXaxis()->SetRangeUser(0, 3000);
    h_num->GetYaxis()->SetRangeUser(0.92, 1.08);
    h_num->Draw("e1");
    h_num2->Draw("e1+same");
    
    TLatex l;
    l.SetTextFont(132); 
    l.SetNDC(); 
    l.SetTextSize(0.04);
    l.SetTextFont(42);
    l.DrawLatex(0.65, 0.86,"#bf{#it{ATLAS}} Internal");
    TLatex l2;
    l2.SetTextFont(132); 
    l2.SetNDC(); 
    l2.SetTextSize(0.04);
    l2.SetTextFont(42);
    l2.DrawLatex(0.65, 0.81,"#sqrt{s} = 13 TeV");
    TLatex l3;
    l3.SetTextFont(132); 
    l3.SetNDC(); 
    l3.SetTextSize(0.04);
    l3.SetTextFont(42);
    l3.DrawLatex(0.65, 0.76,"Anti-k_{t} R = 0.4 PF Jets");

    legend.Clear();
    legend.AddEntry(h_num, "Loose Selection", "l");
    legend.AddEntry(h_num2, "Tight Selection",  "l");
    legend.Draw();
    c1->SaveAs(saveAs1 + "_effPlot_" + saveAs2 + ".png");
}

void make_rej_plot(TH1D* fake, TH1D* fakeloose, TH1D* faketight, TString saveAs){
    c1 = new TCanvas("c1", "First canvas", 800, 600);
    fake->GetXaxis()->SetTitle("p_{T} [GeV]"); 
    fake->GetYaxis()->SetTitle("Events"); 
    fake->GetYaxis()->SetRangeUser(1, fake->GetMaximum()*1000);
    fake->SetMinimum(0.5);
    fake->SetLineColor(kBlack);
    //fake->SetMaximum(50000000);
    fake->Draw("hist"); 
    c1->SetLogy(); 
    //fake->Scale(1/fake->Integral());
    //fakeloose->Scale(1/fakeloose->Integral());
    //faketight->Scale(1/faketight->Integral());
    fakeloose->SetLineColor(4);
    faketight->SetLineColor(2); 
    fakeloose->Draw("hist+same");
    faketight->Draw("hist+same");

    TLatex l;
    l.SetTextFont(132); 
    l.SetNDC(); 
    l.SetTextSize(0.04);
    l.SetTextFont(42);
    l.DrawLatex(0.65, 0.86,"#bf{#it{ATLAS}} Internal");
    TLatex l2;
    l2.SetTextFont(132); 
    l2.SetNDC(); 
    l2.SetTextSize(0.04);
    l2.SetTextFont(42);
    l2.DrawLatex(0.65, 0.81,"#sqrt{s} = 13 TeV");
    TLatex l3;
    l3.SetTextFont(132); 
    l3.SetNDC(); 
    l3.SetTextSize(0.04);
    l3.SetTextFont(42);
    l3.DrawLatex(0.65, 0.76,"Anti-k_{t} R = 0.4 PF Jets");

    legend.Clear();
    legend.AddEntry(fake, "No Cleaning", "l");
    legend.AddEntry(fakeloose, "Loose Cleaning",  "l");
    legend.AddEntry(faketight, "Tight Cleaning",  "l");
    legend.Draw();

    c1->SaveAs("rejPlot_" + saveAs + ".png");

}

void plot_maker_HCW(){
	basicBeautification();
	formatLegend(legend);

	//TFile * f1 = new TFile("exot2_grl8109_postHCW_hists.root", "READ");  
	//TFile * f1 = new TFile("exot2_grl8109_HCWEM_hists.root", "READ");  
	//TFile * f1 = new TFile("exot2_grl8109_HCWLC_hists.root", "READ");  
	//TFile * f1 = new TFile("jetm1_grlPiece_HCWPF_hists.root", "READ");  
	//TFile * f2 = new TFile("exot5_grl8109_bibHCW_hists.root", "READ");
	//TFile * f2 = new TFile("exot2_grl8109_HCWEM_hists.root", "READ");

	//For just dijet looks	
	//TFile * f2 = new TFile("exot2_grl8109_chainWJVT_hists.root", "READ"); 
	//TFile * f2 = new TFile("exot5_grl78109_chain_hists.root", "READ");   

	//For dijet/fake comparisons (pT 250 and eta 2.4)
	//TFile * f2 = new TFile("exot2_grl8109_final.root", "READ");
	//TFile * f2 = new TFile("exot5_grl8109_final.root", "READ");   

	//Referencess
	//TFile * fref = new TFile("exot2_forJES15_fakeRef_data_hists.root", "READ");
	//TFile * fref = new TFile("exot5_grl8109_fakeRef250_hists.root", "READ"); 
	//TFile * fref = new TFile("exot2_rawEvent_JZW.root", "READ"); 
	//TFile * fref = new TFile("exot2_mc_20to32_hists.root", "READ"); 

	TFile* f1 = new TFile("626_data18_350479_JETM1_hists.root", "READ"); 
	TFile * f2 = new TFile("/afs/cern.ch/work/j/jgonski/Harvard/jetCleaning/Scripts_newr/exot5_grl8109_fakeRef250_hists.root", "READ");   
	TFile * fref = new TFile("/afs/cern.ch/work/j/jgonski/Harvard/jetCleaning/Scripts_newr/exot2_grl8109_HCWEM_hists.root", "READ");  //reference
	//TFile * fref = new TFile("exot2_rawEvent_JZW.root", "READ");   
	//TFile * fref = new TFile("0817_HCW17_MET_data2_hists.root", "READ"); 
	TFile * fdata = new TFile("/afs/cern.ch/work/j/jgonski/Harvard/jetCleaning/Scripts_newr/0817_HCW17_MET_data2_hists.root", "READ"); 
	TFile * fmc = new TFile("/afs/cern.ch/work/j/jgonski/Harvard/jetCleaning/Scripts_newr/0817_HCW17_MET_mc2_hists.root", "READ"); 


	///////////////////////////////////////
	//            Make Plots             //
	/////////////////////////////////////// 

	//make_indiv(j15, j25, j35, j60, j150, j260, j380, "dijetpT", "Dijet pT [GeV]");
	make_rej_plot( (TH1D*) f1->Get("dijet_pt"), (TH1D*) f1->Get("dijetloose_pt"), (TH1D*) f1->Get("dijettight_pt"), "626_dead2Tile_dijet");
	make_rej_plot( (TH1D*) f1->Get("fake_pt"), (TH1D*) f1->Get("fakeloose_pt"), (TH1D*) f1->Get("faketight_pt"), "626_dead2Tile_fake");
        //make_eff_plot((TH1D*) f1->Get("eff_all_eta"), (TH1D*) f1->Get("eff_loose_eta"), (TH1D*) f1->Get("eff_tight_eta"), "Jet #eta", "626_dead2Tile","eta");
 	//make_eff_plot((TH1D*) f1->Get("eff_all"), (TH1D*) f1->Get("eff_loose"), (TH1D*) f1->Get("eff_tight"), "Jet p_{T}", "626_dead2Tile","pt" );//,

	drawHist3pt((TH1D*) f1->Get("dijet_pt"),(TH1D*) f1->Get("fake_pt"), (TH1D*) fref->Get("dijet_pt"),"p_{T} [GeV]","Data18, Good Jet Selection", "Data18, Fake Jet Selection", "Data17, Good Jet Selection", true);
	c1->SaveAs("626_dead2Tile_pt.png");

	drawHist3((TH1D*) f1->Get("dijet_eta"),(TH1D*) f1->Get("fake_eta"), (TH1D*) fref->Get("dijet_eta"),"#eta","Data18, Good Jet Selection", "Data18, Fake Jet Selection", "Data17, Good Jet Selection", true);
	c1->SaveAs("626_dead2Tile_eta.png");
	
	drawHist3((TH1D*) f1->Get("dijet_phi"),(TH1D*) f1->Get("fake_phi"), (TH1D*) fref->Get("dijet_phi"),"#phi","Data18, Good Jet Selection", "Data18, Fake Jet Selection", "Data17, Good Jet Selection", true);
	c1->SaveAs("626_dead2Tile_phi.png");
	
	drawHist3((TH1D*) f1->Get("dijet_fmax"),(TH1D*) f1->Get("fake_fmax"),(TH1D*) fref->Get("dijet_fmax"), "Frac Sampling Max","Data18, Good Jet Selection", "Data18, Fake Jet Selection", "Data17, Good Jet Selection", true);
	c1->SaveAs("626_dead2Tile_fmax.png");
	
	drawHist3((TH1D*) f1->Get("dijet_fmaxi"),(TH1D*) f1->Get("fake_fmaxi"),(TH1D*) fref->Get("dijet_fmaxi"), "Frac Sampling Max Index","Data18, Good Jet Selection", "Data18, Fake Jet Selection", "Data17, Good Jet Selection", true);
	c1->SaveAs("626_dead2Tile_fmaxi.png");
	
	drawHist3((TH1D*) f1->Get("dijet_timing"),(TH1D*) f1->Get("fake_timing"),(TH1D*) fref->Get("dijet_timing"),"Timing [ns]","Data18, Good Jet Selection", "Data18, Fake Jet Selection", "Data17, Good Jet Selection", true);
	c1->SaveAs("626_dead2Tile_timing.png");
	
	drawHist3((TH1D*) f1->Get("dijet_emfrac"),(TH1D*) f1->Get("fake_emfrac"),(TH1D*) fref->Get("dijet_emfrac"),"EM Frac","Data18, Good Jet Selection", "Data18, Fake Jet Selection", "Data17, Good Jet Selection", true);
	c1->SaveAs("626_dead2Tile_emfrac.png");
	
	drawHist3((TH1D*) f1->Get("dijet_hecf"),(TH1D*) f1->Get("fake_hecf"), (TH1D*) fref->Get("dijet_hecf"),"HEC Frac","Data18, Good Jet Selection", "Data18, Fake Jet Selection", "Data17, Good Jet Selection", true);
	c1->SaveAs("626_dead2Tile_hecf.png");
	
	drawHist3((TH1D*) f1->Get("dijet_hecq"),(TH1D*) f1->Get("fake_hecq"), (TH1D*) fref->Get("dijet_hecq"),"HEC Q","Data18, Good Jet Selection", "Data18, Fake Jet Selection", "Data17, Good Jet Selection", true);
	c1->SaveAs("626_dead2Tile_hecq.png");
	
	drawHist3((TH1D*) f1->Get("dijet_nege"),(TH1D*) f1->Get("fake_nege"),(TH1D*) fref->Get("dijet_nege"),"Negative Energy [GeV]","Data18, Good Jet Selection", "Rel20 Fake Jets","Data17, Good Jet Selection", true);
	c1->SaveAs("626_dead2Tile_nege.png");
	 
	 drawHist3((TH1D*) f1->Get("dijet_fch"),(TH1D*) f1->Get("fake_fch"), (TH1D*) fref->Get("dijet_fch"),"Charge Fraction","Data18, Good Jet Selection", "Data18, Fake Jet Selection", "Data17, Good Jet Selection", true);
	 c1->SaveAs("626_dead2Tile_fch.png");
	 
	drawHist3((TH1D*) f1->Get("dijet_larq"),(TH1D*) f1->Get("fake_larq"),(TH1D*) fref->Get("dijet_larq"),"LArQ Frac","Data18, Good Jet Selection", "Data18, Fake Jet Selection", "Data17, Good Jet Selection", true);
	 c1->SaveAs("626_dead2Tile_larq.png");
	 
	drawHist3((TH1D*) f1->Get("dijet_n_jet"),(TH1D*) f1->Get("fake_n_jet"),(TH1D*) fref->Get("dijet_n_jet"), "# jets","Data18, Good Jet Selection", "Data18, Fake Jet Selection", "Data17, Good Jet Selection", true);
	 c1->SaveAs("626_dead2Tile_njet.png"); 

	drawHist3((TH1D*) f1->Get("dijet_avglarq"),(TH1D*) f1->Get("fake_avglarq"),(TH1D*) fref->Get("dijet_avglarq"),"Avg LArQ","Release 21 Data, Good Jet Selection", "Release 21 Data, Fake Jet Selection", "Data17, Good Jet Selection", true);
	c1->SaveAs("626_dead2Tile_avglarq.png");

 	//drawHist3((TH1D*) f1->Get("dijet_mht"),(TH1D*) f1->Get("h_jvtmht"), (TH1D*) f2->Get("dijet_mht"),"MHT [GeV]","BIB Selection", "Fake Selection", "Good Selection", true);
	//c1->SaveAs("626_dead2Tile_jvtmht.png");
	
	//drawHist3((TH1D*) f1->Get("dijet_met"),(TH1D*) f1->Get("dijet_met"), (TH1D*) f2->Get(""),MET [GeV]","Exot5", "Exot5", true);
	//c1->SaveAs("626_dead2Tile_met.png");
	
	//drawHist3((TH1D*) f1->Get("dijet_met_phi"),(TH1D*) f1->Get("dijet_met_phi"),"MET #phi","Exot5", "Exot5", true);
	//c1->SaveAs("626_dead2Tile_met_phi.png");
	
	
  	///////////////////////////////////////
	//           2D Plots                //
  	///////////////////////////////////////
	 
 	//draw2D((TH2D*) f2->Get("dijetloose_eta_timing"), "Jet #eta", "Jet timing [ns]");
	//c1->SaveAs("rejPlotNew_dijetloose_2D_etatiming.png");
	
	//draw2D((TH2D*) f2->Get("dijettight_eta_timing"), "Jet #eta", "Jet timing [ns]");
	//c1->SaveAs("rejPlotNew_dijettight_2D_etatiming.png");

	draw2D((TH2D*) f1->Get("dijet_eta_timing"), "Jet #eta", "Jet timing [ns]");
	c1->SaveAs("626_dead2Tile_2D_etatiming.png");
	
	draw2D((TH2D*) f1->Get("dijet_eta_pt"), "Jet #eta", "Jet pT [GeV]");
	c1->SaveAs("626_dead2Tile_2D_etapt.png");

	draw2D((TH2D*) f1->Get("dijet_eta_phi"), "Jet #eta", "Jet #phi");
	c1->SaveAs("626_dead2Tile_2D_etaphi.png");

	//draw2D((TH2D*) f1->Get("dijet_lbn_timing"), "LBN", "jet timing [ns]");
	//c1->SaveAs("626_dead2Tile_2D_lbntiming.png");
	
	// draw2D((TH2D*) f1->Get("dijet_bcid_timing"), "BCID", "jet timing [ns]");
	//c1->SaveAs("626_dead2Tile_2D_bcidtiming.png");
	
	// draw2D((TH2D*) f1->Get("dijet_run_timing"),  "Run", "jet timing [ns]"); 
	//c1->SaveAs("626_dead2Tile_2D_runtiming.png");
	
	draw2D((TH2D*) f1->Get("dijet_nege_hecf"), "Neg E", "HEC F");
	c1->SaveAs("626_dead2Tile_2D_hecfnege.png");

	draw2D((TH2D*) f1->Get("dijet_hecq_nege"), "HEC Q", "Neg E");
	c1->SaveAs("626_dead2Tile_2D_hecqnege.png");

	//draw2D((TH2D*) f1->Get("dijet_timing[0]_timing[1]"), "leading jet timing [ns]", "subleading jet timing [ns]");
	//c1->SaveAs("626_dead2Tile_2D_timing2.png"); 
	//
 	//draw2D((TH2D*) f1->Get("fake_pt_nege"), "p_{T}", "negE"); 
	//c1->SaveAs("0830_fake_pt_nege.eps"); 
	
 	//draw2D((TH2D*) f1->Get("fake_eta_nege"), "#eta", "negE"); 
	//c1->SaveAs("0830_fake_eta_nege.eps"); 
	
	//draw2D((TH2D*) f1->Get("fake_timing_nege"), "Timing [ns]", "negE"); 
	//c1->SaveAs("0830_fake_timing_nege.eps"); 
	
	//draw2D((TH2D*) f1->Get("fake_hecf_nege"), "HEC Frac", "negE"); 
	//c1->SaveAs("0830_fake_hecf_nege.eps"); 
	
	//draw2D((TH2D*) f1->Get("fake_hecq_nege"), "HEC Frac", "negE"); 
	//c1->SaveAs("0830_fake_hecq_nege.eps"); 

	//stackHists((TH1D*) f->Get("dijet_phi"),(TH1D*) f->Get("dijetloose_phi"),(TH1D*) f->Get("dijettight_phi"),"Leading jet #phi");

}
