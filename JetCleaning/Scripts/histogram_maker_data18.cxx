
#include "cleaningUtils.hxx"
#include "TLorentzVector.h"

// a class to store MC weight info
// and read xsec info from a file
class MCWeights {
	public:
		map<int, float> mc_xsec;
		map<int, float> mc_ngen;

		MCWeights() {
			//read_xsec_info("data/susy_crosssections_13TeV.txt");
			//TODO: these need to be updated
			mc_ngen[361000] = 1.999000e+06;
			mc_ngen[361001] = 1.973200e+06;
			mc_ngen[361002] = 1.984600e+06;
			mc_ngen[361003] = 1.999000e+06;
			mc_ngen[361004] = 1.994094e+06;
			mc_ngen[361005] = 1.998800e+06;
			mc_ngen[361006] = 1.996000e+06;
			mc_ngen[361007] = 1.998765e+06;
			mc_ngen[361008] = 1.988991e+06;
			mc_ngen[361009] = 1.156400e+06;
			mc_ngen[361020] = 1999400.00;
			//mc_ngen[361021] = 72317040.00;
			mc_ngen[361021] = 1999000.0 ;
			///c_ngen[361022] = 72317040.00;
			mc_ngen[361022] = 1994600.0;
			//mc_ngen[361023] = 289155644;
			mc_ngen[361023] = 7884500.0;
			//mc_ngen[361024] = 201660706; 
			mc_ngen[361024] = 7979800.0 ;
			//mc_ngen[361025] = 126274756;
			mc_ngen[361025] = 7977600.0 ;
			//mc_ngen[361026] = 19138946.00;
			mc_ngen[361026] = 1893400.0 ;
			//mc_ngen[361027] = 50187992.00;
			mc_ngen[361027] = 1770200.0  ;
			mc_ngen[361028] = 1743200.00;
			mc_ngen[361029] = 1813200.00;
			mc_ngen[361030] = 1996000;
			mc_ngen[361031] = 1993200.00;
			mc_ngen[361032] = 1974600.00;


		}

		float getWeight(const int mcid) {
			if (mc_ngen.count(mcid) > 0 && mc_xsec.count(mcid) > 0) return  mc_xsec[mcid] / mc_ngen[mcid];
			else return 0;
		}

		//read xsec info from a txt file
		void read_xsec_info(const TString filename) {
			TTree * t = new TTree();
			t->ReadFile(filename, "id/I:name/C:xsec/F:kfac/F:eff/F:relunc/F");
			int mcid;
			float xsec, kfac, eff;
			t->SetBranchAddress("id", &mcid);
			t->SetBranchAddress("xsec", &xsec);
			t->SetBranchAddress("kfac", &kfac);
			t->SetBranchAddress("eff", &eff);
			for (int i = 0; i < t->GetEntries(); i++) {
				t->GetEntry(i);
				mc_xsec[mcid] = xsec * kfac * eff * 1000;
			}
			delete t;
		}
};

// return lowest-prescale efficient single jet trigger
// for a given leading jet pT
TString passTrigger(const float leading_pt, TreeEvtInfo * evt) {
	if (leading_pt > 500) {
		if (evt->get("trig_HLT_j460") > 0) return "HLT_j460";
	} else if (leading_pt > 400) {
		if (evt->get("trig_HLT_j360") > 0) return "HLT_j360";
	} else if (leading_pt > 275) {
		if (evt->get("trig_HLT_j260") > 0) return "HLT_j260";
	} else if (leading_pt > 190) {
		if (evt->get("trig_HLT_j175") > 0) return "HLT_j175";
	} else if (leading_pt > 75) {
		//if (evt->get("trig_HLT_j60_L1RD0_FILLED")  > 0) return "HLT_j60_L1RD0_FILLED";
		if (evt->get("trig_HLT_j60")  > 0) return "HLT_j60";
	} else if (leading_pt > 45) {
		if (evt->get("trig_HLT_j35")  > 0) return "HLT_j35";
	} else if (leading_pt > 35) {
		if (evt->get("trig_HLT_j25")  > 0) return "HLT_j25";
	} else if (leading_pt > 20) {
		if (evt->get("trig_HLT_j15")  > 0) return "HLT_j15";
	}
	return "";
}

/*************************
 * Jet cleaning criteria *
 *************************/
enum CleaningLevel {BAD, LOOSE, TIGHT, LOOSENEGE};

//find cleaning criteria met by jet j

bool passLoose(TreeEvtInfo * evt, int j) {
	//LOOSER Cuts
	//HEC cut
	if(evt->get("jet_a4em_pt", j) < 20 || fabs(evt->get("jet_a4em_eta", j)) > 4.5) return true;
	if (evt->get("jet_a4em_hecf", j)   > 0.5
			&& fabs(evt->get("jet_a4em_hecq", j))  > 0.5
			&&      evt->get("jet_a4em_avglarq", j) / 65535 > 0.8) {
		//std::cout << "Fails hecf, hecq, avglarq!" << endl; 
		return false;}
	//NegE cut; removed as of 2017
	//if (fabs(evt->get("jet_a4em_nege", j)) > 60) return false;
	//NC1 cut
	if (evt->get("jet_a4em_emfrac", j) < 0.05
			&&      evt->get("jet_a4em_fch", j)    < 0.05
			&& fabs(evt->get("jet_a4em_eta", j))   < 2){
		//std::cout << "Fails efmrac, fch, etc!" << endl; 
		return false;}
	//NC2 cut
	if (evt->get("jet_a4em_emfrac", j) < 0.05 && fabs(evt->get("jet_a4em_eta", j)) >= 2) {
		//std::cout << "Fails efmrac, eta!" << endl; 
		return false;} 
	//fmax cut
	if (evt->get("jet_a4em_fmax", j) > 0.99 && fabs(evt->get("jet_a4em_eta", j)) < 2) {
		//std::cout << "Fails fmax, etc!" << endl; 
		return false;}
	//emfrac cut
	if (evt->get("jet_a4em_emfrac", j) > 0.95
			&& fabs(evt->get("jet_a4em_larq", j))  > 0.8
			&&      evt->get("jet_a4em_avglarq", j) / 65535 > 0.8
			&& fabs(evt->get("jet_a4em_eta", j))   < 2.8){
		//std::cout << "Fails larq, avg larq, etc!" << endl; 
		return false;}

	return true;
}

bool passTight(TreeEvtInfo * evt, int j) {
	//TIGHT Cut
	if (evt->get("jet_a4em_fch", j) / evt->get("jet_a4em_fmax", j) < 0.1
			&& fabs(evt->get("jet_a4em_eta", j))   < 2.4) return false;

	return true;
}

int getCleaning(TreeEvtInfo * evt, int j) {
	if (!passLoose(evt,j)) return CleaningLevel::BAD;
	if (!passTight(evt,j)) return CleaningLevel::LOOSE;
	return CleaningLevel::TIGHT;
}


/******************
  cutflow functions
 *******************/

bool pass_dijet(TreeEvtInfo * evt, float p0_cut = 0, float p1_cut = 0) {
	if (evt->get("n_jet") >= 2 && evt->get("jet_a4em_dphi_leadsublead") > 3.){
		float p0 = evt->get("jet_a4em_pt", 0);
		float p1 = evt->get("jet_a4em_pt", 1);
		if (p0 <= p0_cut) return false;
		if (p1 <= p1_cut) return false;
		if (p0 < 60 && abs(evt->get("jet_a4em_eta", 0)) < 2.4){
			if(evt->get("jet_a4em_jvt", 0) < 0.59) return false;}
		if (p1 < 60 && abs(evt->get("jet_a4em_eta", 1)) < 2.4){
			if(evt->get("jet_a4em_jvt", 1) < 0.59) return false;}

		return fabs(p1 - p0) / (p1 + p0) < 0.3;
	}
	return false;
}
bool pass_good(TreeEvtInfo * evt) {
	return  pass_dijet(evt, 20, 20) && (evt->get("passVxCut") > 0);
}
bool pass_goodloose(TreeEvtInfo * evt) {
	return pass_good(evt) && getCleaning(evt, 0) >= CleaningLevel::LOOSE && getCleaning(evt, 1) >= CleaningLevel::LOOSE;
}
bool pass_goodtight(TreeEvtInfo * evt) {
	return pass_good(evt) && getCleaning(evt, 0) >= CleaningLevel::TIGHT && getCleaning(evt, 1) >= CleaningLevel::TIGHT;
} 
bool pass_goodforward(TreeEvtInfo * evt) {
	return pass_good(evt) && abs(evt->get("jet_a4em_eta", 0)) > 2.8;
}
bool pass_monojet(TreeEvtInfo * evt) {
	if (evt->get("n_jet") > 0 && 
			evt->get("jet_a4em_pt", 0) > 250
			//&& evt->get("mht") > 400
	   ){ //return true;

		float mhtx = 0;
		float mhty = 0;
		TLorentzVector vec;
		for(int z = 0; z < evt->get("n_jet"); z++){
			if(evt->get("jet_a4em_pt", z) < 60 && fabs(evt->get("jet_a4em_eta", z)) < 2.4){
				if(evt->get("jet_a4em_jvt", z) > 0.59){
					vec.SetPtEtaPhiE(evt->get("jet_a4em_pt", z), evt->get("jet_a4em_eta", z), evt->get("jet_a4em_phi", z),evt->get("jet_a4em_e", z));
					mhtx += vec.Px();
					mhty += vec.Py();
				}
			}
			else{
				vec.SetPtEtaPhiE(evt->get("jet_a4em_pt", z), evt->get("jet_a4em_eta", z), evt->get("jet_a4em_phi", z),evt->get("jet_a4em_e", z));
				mhtx += vec.Px();
				mhty += vec.Py();
			}
		}
		float mht = sqrt(mhtx * mhtx + mhty * mhty);
		float dphi_leadmht;   float mht_phi;
		mht_phi = atan2(-1*mhty, -1*mhtx);
		dphi_leadmht = TMath::ACos(TMath::Cos(evt->get("jet_a4em_phi", 0) - mht_phi));
		//if(evt->get("met") > 80) return true;
		if(dphi_leadmht < 3.0) return false;
		if(mht < 250) return false;
		else return true;
	} else return false;
}
bool pass_fake(TreeEvtInfo * evt) {
	if (//evt->get("jet_a4em_dphi_leadmht") > 3.0 &&
			//evt->get("n_goodel") == 0 && evt->get("n_goodmu") == 0 &&
			fabs(evt->get("jet_a4em_eta",0)) < 2.1 && 
			pass_monojet(evt) && fabs(evt->get("jet_a4em_timing", 0)) > 6){ //&& evt->get("n_jet") == 1 && abs(evt->get("jet_a4em_eta", 0)) < 2.4 && evt->get("jet_a4em_jvt") < 0.64) {
		return true;
	} 
	else return false;
}
bool pass_fakeloose(TreeEvtInfo * evt) {
	return pass_fake(evt) && getCleaning(evt, 0) >= CleaningLevel::LOOSE;
}

bool pass_faketight(TreeEvtInfo * evt) {
	return pass_fake(evt) && getCleaning(evt, 0) >= CleaningLevel::TIGHT;
}
bool pass_allloose(TreeEvtInfo * evt) {
	bool isAllClean = true; 
	bool isJetClean = false;
	for(int i = 0; i < evt->get("n_jet"); i++){
		if(evt->get("jet_a4em_pt",i) < 20 || fabs(evt->get("jet_a4em_eta",i)) > 4.5) isJetClean = true;
		else isJetClean = getCleaning(evt, i) >= CleaningLevel::LOOSE;
		if(!isJetClean) std::cout << "currDec: " << isJetClean << endl;
		isAllClean = isAllClean && isJetClean;
	}
	return isAllClean;
}
bool pass_all(TreeEvtInfo * evt) {
	if (evt->get("jet_a4em_pt", 0) < 60 && fabs(evt->get("jet_a4em_eta", 0) ) < 2.4){
		if (evt->get("jet_a4em_jvt", 0) < 0.59) return false;
	}
	return true;
}
bool pass_bib(TreeEvtInfo * evt) {
	if(evt->get("bib") > 0) return true;
	else return false;
}
bool pass_deadmodule_etaphi(TreeEvtInfo * evt) {
	bool isAnyJetPointedThere = false;
	for(int j = 0; j < evt->get("n_jet"); j++){
		if (evt->get("jet_a4em_eta", j) > 0.1 && evt->get("jet_a4em_eta", j) < 0.7 && evt->get("jet_a4em_phi", j) > 2.8 && evt->get("jet_a4em_phi", j) < 3.0) isAnyJetPointedThere = true;
	}
	return isAnyJetPointedThere;
}
bool pass_deadmodule_etaphi_dijet(TreeEvtInfo * evt) {
	if(!pass_good(evt)){
		//std::cout << "Did not pass dijet!" << std::endl;
		 return false;}
	bool isAnyJetPointedThere = false;
	for(int j = 0; j < evt->get("n_jet"); j++){
		if (evt->get("jet_a4em_eta", j) > 0.1 && evt->get("jet_a4em_eta", j) < 0.7 && evt->get("jet_a4em_phi", j) > 2.8 && evt->get("jet_a4em_phi", j) < 3.0) isAnyJetPointedThere = true;
	}
	return isAnyJetPointedThere;
}
bool pass_deadmodule_etaphi_dijet_leading(TreeEvtInfo * evt) {
	if(!pass_good(evt)){
		//std::cout << "Did not pass dijet!" << std::endl;
		 return false;}
	bool isAnyJetPointedThere = false;
	if (evt->get("jet_a4em_eta", 0) > 0.1 && evt->get("jet_a4em_eta", 0) < 0.7 && evt->get("jet_a4em_phi", 0) > 2.8 && evt->get("jet_a4em_phi", 0) < 3.0) isAnyJetPointedThere = true;
	return isAnyJetPointedThere;
}

/******************
  Main function
 *******************/

TChain * c;
TCanvas* c1;

enum JetFillingScheme {LEADING, SUBLEADING, DIJET, INCLUSIVE} ;
enum VarSet {DEFAULT, SHORT} ;
enum OptHist2D {NONE, BANANA, TIMING} ;

//helper function to streamline configuration of multiple SelectionHists
void add_selection_hist(vector<CleanSelectionHists*> & sel_list, TreeEvtInfo * evt,
		bool (*sel_func)(TreeEvtInfo *),
		TString name,
		int jetscheme = JetFillingScheme::LEADING,
		int opt2d = OptHist2D::NONE, int varset = VarSet::DEFAULT) {
	CleanSelectionHists * sel = new CleanSelectionHists(evt, sel_func, name);
	switch (jetscheme) {
		case JetFillingScheme::DIJET:
			sel->setDijet();
			break;
		case JetFillingScheme::LEADING:
			sel->setLeadjet();
			break;
		case JetFillingScheme::SUBLEADING:
			sel->setSubleadjet();
			break;
		case JetFillingScheme::INCLUSIVE:
			sel->setInclusiveJet(4);
			break;
		default:
			sel->setLeadjet();
	}
	sel->add2DHist("eta", "phi");
	sel->add2DHist("eta", "pt");
	sel->add2DHist("timing[0]", "timing[1]");
	switch (opt2d) {
		case OptHist2D::TIMING:
			sel->add2DHist("lbn", "timing");
			sel->add2DHist("bcid", "timing");
			sel->add2DHist("run", "timing");
			sel->add2DHist("larq", "eta");
			sel->add2DHist("nege", "hecf");
			for (TString var : CleanSelectionHists::getDefaultVars()) sel->add2DHist(var, "timing");
			for (TString var : CleanSelectionHists::getDefaultVars()) sel->add2DHist(var, "nege");
			break;
		case OptHist2D::BANANA:
			sel->add2DHist("eta", "timing");;
			break;
	}

	if (varset == VarSet::SHORT){
		sel->setVars(CleanSelectionHists::getShortVars());
	}
	sel_list.push_back(sel);
}

void parseNtuple(TChain *c, TString outname){
	//   cout << "Parsing " << inpath << endl;
	bool isData = outname.Contains("data");

	// class that intelligently reads event info from trees
	TreeEvtInfo * evt = new CleanTreeEvtInfo();

	// mc weight container
	//MCWeights mc_weights = MCWeights();

	// histograms are filled in groups based on the event selection
	// since determining the jet prescale trigger is a bit complicated, all of the
	// single-jet-trigger selections are grouped in one vector
	vector<CleanSelectionHists*> singlejet_selections; // selections that use jet triggering
	add_selection_hist(singlejet_selections, evt, pass_good, "dijet", JetFillingScheme::DIJET, OptHist2D::TIMING);
	vector<CleanSelectionHists*> singlejetloose_selections; // selections that use jet triggering
	add_selection_hist(singlejetloose_selections, evt, pass_goodloose, "dijetloose", JetFillingScheme::DIJET, OptHist2D::TIMING);
	vector<CleanSelectionHists*> singlejettight_selections; // selections that use jet triggering
	add_selection_hist(singlejettight_selections, evt, pass_goodtight, "dijettight", JetFillingScheme::DIJET, OptHist2D::TIMING);
	
	vector<CleanSelectionHists*> deadmodule_etaphi_dijet_leading_selections; // selections that use jet triggering
	add_selection_hist(deadmodule_etaphi_dijet_leading_selections, evt, pass_deadmodule_etaphi_dijet_leading, "deadmodule_etaphi_dijet_leading", JetFillingScheme::LEADING, OptHist2D::TIMING);
	vector<CleanSelectionHists*> deadmodule_etaphi_dijet_selections; // selections that use jet triggering
	add_selection_hist(deadmodule_etaphi_dijet_selections, evt, pass_deadmodule_etaphi_dijet, "deadmodule_etaphi_dijet", JetFillingScheme::DIJET, OptHist2D::TIMING);
	vector<CleanSelectionHists*> deadmodule_etaphi_selections;
	add_selection_hist(deadmodule_etaphi_selections, evt, pass_deadmodule_etaphi, "deadmodule_etaphi", JetFillingScheme::DIJET, OptHist2D::TIMING);

	// met-trigger selection is stored separately
	//vector<CleanSelectionHists*> met_selections; // selections that use met triggering
	//add_selection_hist(met_selections, evt, pass_fake,      "fake",    JetFillingScheme::LEADING, OptHist2D::TIMING);  
	//vector<CleanSelectionHists*> metloose_selections; // selections that use met triggering
	//add_selection_hist(metloose_selections, evt, pass_fakeloose,      "fakeloose",    JetFillingScheme::LEADING, OptHist2D::TIMING);  
	//vector<CleanSelectionHists*> mettight_selections; // selections that use met triggering
	//add_selection_hist(mettight_selections, evt, pass_faketight,      "faketight",    JetFillingScheme::LEADING, OptHist2D::TIMING);  
	
	//efficiency histogams - pT
	Float_t bins[] = { 0, 25, 50, 75, 100, 125, 150, 175, 200, 250, 300, 350, 400, 450, 500, 600, 700, 800, 900, 1000, 1500, 2000, 3000}; //23
	TH1D* eff_all = new TH1D("eff_all","eff_all",22, bins);
	TH1D* eff_loose = new TH1D("eff_loose","eff_loose",22, bins);
	TH1D* eff_tight = new TH1D("eff_tight","eff_tight",22, bins);
	TH1D* eff_all_slice1 = new TH1D("eff_all_slice1","eff_all_slice1",22, bins);
	TH1D* eff_loose_slice1 = new TH1D("eff_loose_slice1","eff_loose_slice1",22, bins);
	TH1D* eff_tight_slice1 = new TH1D("eff_tight_slice1","eff_tight_slice1",22, bins);
	TH1D* eff_all_slice2 = new TH1D("eff_all_slice2","eff_all_slice2",22, bins);
	TH1D* eff_loose_slice2 = new TH1D("eff_loose_slice2","eff_loose_slice2",22, bins);
	TH1D* eff_tight_slice2 = new TH1D("eff_tight_slice2","eff_tight_slice2",22, bins);
	TH1D* eff_all_slice3 = new TH1D("eff_all_slice3","eff_all_slice3",22, bins);
	TH1D* eff_loose_slice3 = new TH1D("eff_loose_slice3","eff_loose_slice3",22, bins);
	TH1D* eff_tight_slice3 = new TH1D("eff_tight_slice3","eff_tight_slice3",22, bins);
	TH1D* eff_all_slice4 = new TH1D("eff_all_slice4","eff_all_slice4",22, bins);
	TH1D* eff_loose_slice4 = new TH1D("eff_loose_slice4","eff_loose_slice4",22, bins);
	TH1D* eff_tight_slice4 = new TH1D("eff_tight_slice4","eff_tight_slice4",22, bins);
	TH1D* eff_all_slice5 = new TH1D("eff_all_slice5","eff_all_slice5",22, bins);
	TH1D* eff_loose_slice5 = new TH1D("eff_loose_slice5","eff_loose_slice5",22, bins);
	TH1D* eff_tight_slice5 = new TH1D("eff_tight_slice5","eff_tight_slice5",22, bins);

	//efficiency histogams - eta
	TH1D* eff_all_eta = new TH1D("eff_all_eta","eff_all_eta",24,-5,5);
	TH1D* eff_loose_eta = new TH1D("eff_loose_eta","eff_loose_eta",24,-5,5);
	TH1D* eff_tight_eta = new TH1D("eff_tight_eta","eff_tight_eta",24,-5,5);

	//TreeEvtInfo needs to be attached to tree before running
	evt->attachToTree(c);
	Long64_t n = c->GetEntries();

 	std::cout << "Number of entries: " << n << std::endl;

	for (Long64_t i = 0; i < n; i++) {
		printProgress(n, i);

		// pass event # to TreeEvtInfo
		// event variables are loaded as they are requested
		evt->setEntry(i);
		bool isMC = evt->get("isMC") > 0;

		//if(!(pass_allloose(evt))){
		//	std::cout << "This event FAILS loose cleaning!" << std::endl;}
		//cout << "passMC: " << isMC << ", Pass GRL: " << evt->get("passGRLCut") << endl;	
		
 		if ( (!isMC && evt->get("passGRLCut") > 0) || (isMC && evt->get("passMCCut") > 0)) {

			//////////////////////////////////////////////////
			// determine single jet trigger logic & prescales
			//////////////////////////////////////////////////
			float leading_pt = evt->get("jet_a4em_pt", 0);
			// get name of jet trigger for event
			// leading jet pT must fall into proper pT range fo each prescaled trigger
			TString trigger = passTrigger(leading_pt, evt);
			//cout << "Leading jet pt: " << leading_pt << ", Trigger: " << trigger <<endl;
			int run = evt->get("run");
			float weight = 1.0;
			float lbn = evt->get("lbn");

			if (trigger.Contains("HLT")){ // single jet trigger in event
				// get mc weight
				if (isMC && evt->get("passMCCut") > 0) { 
					//going to need some prep to handle mc16d... 
					//weight *= mc_weights.getWeight(run) * evt->get("event_weight");
					//weight *= evt->get("pu_weight"); //disable as necessary
					//cout << "Weight: " << weight << endl;
					// get trigger prescale if running on data
					// NOTE: prescales need to be updated with prescales/getPrescaleForTrigger.py
				} else {
					weight = getPS(trigger, run, lbn);
				}	

				//std::cout << "Weight: " << weight << std::endl;
				
				// fill all histograms for all selections that use single jet triggers
				for (CleanSelectionHists * sel : singlejet_selections) sel->fill(weight);
				for (CleanSelectionHists * sel : singlejetloose_selections) sel->fill(weight);
				for (CleanSelectionHists * sel : singlejettight_selections) sel->fill(weight);
				for (CleanSelectionHists * sel : deadmodule_etaphi_dijet_leading_selections) sel->fill(weight);
				for (CleanSelectionHists * sel : deadmodule_etaphi_dijet_selections) sel->fill(weight);
				for (CleanSelectionHists * sel : deadmodule_etaphi_selections) sel->fill(weight);
				//for (CleanSelectionHists * sel : met_selections) sel->fill(weight);
				//for (CleanSelectionHists * sel : metloose_selections) sel->fill(weight);
				//for (CleanSelectionHists * sel : mettight_selections) sel->fill(weight);

				// fill efficiency histograms 
                   		// int jet1_cleaning = getCleaning(evt,0);
                   		// int jet2_cleaning = getCleaning(evt,1);
                   		// float jet1_pT = evt->get("jet_a4em_pt",0);
                   		// float jet2_pT = evt->get("jet_a4em_pt",1);
                   		// float jet1_eta = evt->get("jet_a4em_eta",0);
                   		// float jet2_eta = evt->get("jet_a4em_eta",1);
                   		// float jet1_timing = evt->get("jet_a4em_timing",0);
                   		// float jet2_timing = evt->get("jet_a4em_timing",1);
                        	
		   		// if(fabs(jet1_timing) > 6 && fabs(jet2_timing) > 6)  nBothOut++;
		   		// else if (fabs(jet1_timing) > 6 && fabs(jet2_timing) <= 6)  nOneOut++;
		   		// else if (fabs(jet2_timing) > 6 && fabs(jet1_timing) <= 6)  nOneOut++;
		   		// else if (fabs(jet1_timing) <= 6 && fabs(jet2_timing) <= 6) nBothIn++;
 		        	
                        	
                   		// if (jet1_cleaning == CleaningLevel::TIGHT || jet2_cleaning == CleaningLevel::TIGHT){
                   		//     eff_all->Fill(jet1_pT, weight);
                   		//     eff_all->Fill(jet2_pT, weight);
		   		//     if(abs(jet1_eta) < 1.2) eff_all_slice1->Fill(jet1_pT, weight);
		   		//     if(abs(jet2_eta) < 1.2) eff_all_slice1->Fill(jet2_pT, weight);
		   		//     if(abs(jet1_eta) < 2.0 && abs(jet1_eta) > 1.2) eff_all_slice2->Fill(jet1_pT, weight);
		   		//     if(abs(jet2_eta) < 2.0 && abs(jet2_eta) > 1.2) eff_all_slice2->Fill(jet2_pT, weight);
		   		//     if(abs(jet1_eta) < 2.8 && abs(jet1_eta) > 2.0) eff_all_slice3->Fill(jet1_pT, weight);
		   		//     if(abs(jet2_eta) < 2.8 && abs(jet2_eta) > 2.0) eff_all_slice3->Fill(jet2_pT, weight);
		   		//     if(abs(jet1_eta) < 3.2 && abs(jet1_eta) > 2.8) eff_all_slice4->Fill(jet1_pT, weight);
		   		//     if(abs(jet2_eta) < 3.2 && abs(jet2_eta) > 2.8) eff_all_slice4->Fill(jet2_pT, weight);
		   		//     if(abs(jet1_eta) < 4.5 && abs(jet1_eta) > 3.2) eff_all_slice5->Fill(jet1_pT, weight);
		   		//     if(abs(jet2_eta) < 4.5 && abs(jet2_eta) > 3.2) eff_all_slice5->Fill(jet2_pT, weight);
                   		//     //if (jet1_pT > 100) 
                   		//     eff_all_eta->Fill(evt->get("jet_a4em_eta",0), weight);
                   		//     //if (jet2_pT > 100) 
                   		//     eff_all_eta->Fill(evt->get("jet_a4em_eta",1), weight);
                   		//     
                   		//     if (jet1_cleaning == CleaningLevel::TIGHT && jet2_cleaning >= CleaningLevel::LOOSE){
                   		//         eff_loose->Fill(jet2_pT, weight);
		   		//     if(abs(jet2_eta) < 1.2) eff_loose_slice1->Fill(jet2_pT, weight);
		   		//     if(abs(jet2_eta) < 2.0 && abs(jet2_eta) > 1.2) eff_loose_slice2->Fill(jet2_pT, weight);
		   		//     if(abs(jet2_eta) < 2.8 && abs(jet2_eta) > 2.0) eff_loose_slice3->Fill(jet2_pT, weight);
		   		//     if(abs(jet2_eta) < 3.2 && abs(jet2_eta) > 2.8) eff_loose_slice4->Fill(jet2_pT, weight);
		   		//     if(abs(jet2_eta) < 4.5 && abs(jet2_eta) > 3.2) eff_loose_slice5->Fill(jet2_pT, weight);
                   		//         //if (jet2_pT > 100) 
                   		//         eff_loose_eta->Fill(evt->get("jet_a4em_eta",1), weight);
                   		//     }
                   		//     if (jet2_cleaning == CleaningLevel::TIGHT && jet1_cleaning >= CleaningLevel::LOOSE){
                   		//         eff_loose->Fill(jet1_pT, weight);
		   		//     if(abs(jet1_eta) < 1.2) eff_loose_slice1->Fill(jet1_pT, weight);
		   		//     if(abs(jet1_eta) < 2.0 && abs(jet1_eta) > 1.2) eff_loose_slice2->Fill(jet1_pT, weight);
		   		//     if(abs(jet1_eta) < 2.8 && abs(jet1_eta) > 2.0) eff_loose_slice3->Fill(jet1_pT, weight);
		   		//     if(abs(jet1_eta) < 3.2 && abs(jet1_eta) > 2.8) eff_loose_slice4->Fill(jet1_pT, weight);
		   		//     if(abs(jet1_eta) < 4.5 && abs(jet1_eta) > 3.2) eff_loose_slice5->Fill(jet1_pT, weight);

			}//if pass single jet trigger

			//////////////////////////////////////////////////
			// other trigger selections
			//////////////////////////////////////////////////
			//if ( evt->get("trig_HLT_xe100_mht_L1XE50") > 0) {
			//	// fill all histograms for all selections that use HLT_xe80
			//	//if(isData) for (CleanSelectionHists * sel : met_selections) sel->fill(1.); 
			//	float weight = 1.0;
			//	//cout << "got an event with this MET trigger!" << endl;
			//	//TString trigger_met = "HLT_xe100_mht_L1XE50";
			//	//weight = getPS(trigger_met, evt->get("run"), evt->get("lbn"));
			//	//cout << "Weight: " << weight << endl;
			//	for (CleanSelectionHists * sel : met_selections) sel->fill(weight);
			//	for (CleanSelectionHists * sel : metloose_selections) sel->fill(weight);
			//	for (CleanSelectionHists * sel : mettight_selections) sel->fill(weight);
			//}
		}//if pass GRL cut
	}// loop over 

	TFile * f = new TFile("76_data18_"+outname+"_JETM1_hists.root", "RECREATE");
	
	// write all histograms
	for (CleanSelectionHists * sel : singlejet_selections)  sel->write();
	for (CleanSelectionHists * sel : singlejetloose_selections)  sel->write();
	for (CleanSelectionHists * sel : singlejettight_selections)  sel->write();
	for (CleanSelectionHists * sel : deadmodule_etaphi_dijet_leading_selections)  sel->write();
	for (CleanSelectionHists * sel : deadmodule_etaphi_dijet_selections)  sel->write();
	for (CleanSelectionHists * sel : deadmodule_etaphi_selections)  sel->write();
	//for (CleanSelectionHists * sel : met_selections)        sel->write();
	//for (CleanSelectionHists * sel : metloose_selections)        sel->write();
	//for (CleanSelectionHists * sel : mettight_selections)        sel->write();
	
	for (CleanSelectionHists * sel : singlejet_selections)  sel->clearMaps();
	for (CleanSelectionHists * sel : singlejetloose_selections)  sel->clearMaps();
	for (CleanSelectionHists * sel : singlejettight_selections)  sel->clearMaps();
	for (CleanSelectionHists * sel : deadmodule_etaphi_dijet_leading_selections)  sel->clearMaps();
	for (CleanSelectionHists * sel : deadmodule_etaphi_dijet_selections)  sel->clearMaps();
	for (CleanSelectionHists * sel : deadmodule_etaphi_selections)  sel->clearMaps();
	//for (CleanSelectionHists * sel : met_selections)        sel->clearMaps();
	//for (CleanSelectionHists * sel : metloose_selections)        sel->clearMaps();
	//for (CleanSelectionHists * sel : mettight_selections)        sel->clearMaps();
	
	eff_loose->Write();
	eff_loose_slice1->Write();
	eff_loose_slice2->Write();
	eff_loose_slice3->Write();
	eff_loose_slice4->Write();
	eff_loose_slice5->Write();
	eff_tight->Write();
	eff_tight_slice1->Write();
	eff_tight_slice2->Write();
	eff_tight_slice3->Write();
	eff_tight_slice4->Write();
	eff_tight_slice5->Write();
	eff_all->Write(); 
	eff_all_slice1->Write(); 
	eff_all_slice2->Write(); 
	eff_all_slice3->Write(); 
	eff_all_slice4->Write(); 
	eff_all_slice5->Write(); 
	eff_loose_eta->Write();
	eff_tight_eta->Write();
	eff_all_eta->Write(); 
	
	eff_loose->Delete();
	eff_loose_slice1->Delete();
	eff_loose_slice2->Delete();
	eff_loose_slice3->Delete();
	eff_loose_slice4->Delete();
	eff_loose_slice5->Delete();
	eff_tight->Delete();
	eff_tight_slice1->Delete();
	eff_tight_slice2->Delete();
	eff_tight_slice3->Delete();
	eff_tight_slice4->Delete();
	eff_tight_slice5->Delete();
	eff_all->Delete();
	eff_all_slice1->Delete();
	eff_all_slice2->Delete();
	eff_all_slice3->Delete();
	eff_all_slice4->Delete();
	eff_all_slice5->Delete();
	eff_loose_eta->Delete();
	eff_tight_eta->Delete();
	eff_all_eta->Delete();
	
	f->Close();
}

void histogram_maker_data18() {

	TChain * c0; c0 = new TChain("nominal");
	TChain * c1; c1 = new TChain("nominal");
	TChain * c2; c2 = new TChain("nominal");
	//bool isData = inpath.Contains("data");
	//  4.23 G Jun 26 data18_13TeV.350479.tileModule2Dead_minitrees.root
	//  4.10 G Jun 26 data18_13TeV.350531.tileModule2Dead_minitrees.root
	//  1.40 G Jun 26 data18_13TeV.350751.tileModule2Dead_minitrees.root
	//  4.52 G Jun 26 data18_13TeV.350803.tileModule2Dead_minitrees.root
	//  3.92 G Jun 26 data18_13TeV.350842.tileModule2Dead_minitrees.root
	//  5.23 G Jun 26 data18_13TeV.350848.tileModule2Dead_minitrees.root
	//  4.67 G Jun 26 data18_13TeV.350880.tileModule2Dead_minitrees.root
	//  5.91 G Jun 26 data18_13TeV.350923.tileModule2Dead_minitrees.root
	//  3.39 G Jun 26 data18_13TeV.351062.tileModule2Dead_minitrees.root
	//514.84 M Jun 26 data18_13TeV.351160.tileModule2Dead_minitrees.root
	//  5.13 G Jun 26 data18_13TeV.351223.tileModule2Dead_minitrees.root
	//  1.65 G Jun 26 data18_13TeV.351296.tileModule2Dead_minitrees.root
	//  2.88 G Jun 26 data18_13TeV.351325.tileModule2Dead_minitrees.root	
	//c0->Add("root://eosatlas//eos/atlas/user/j/jgonski/data18/data18_13TeV.350803.tileModule2Dead_minitrees.root"); 
	//c1->Add("root://eosatlas//eos/atlas/user/j/jgonski/data18/data18_13TeV.350880.tileModule2Dead_minitrees.root"); 
	//c2->Add("root://eosatlas//eos/atlas/user/j/jgonski/data18/data18_13TeV.351062.tileModule2Dead_minitrees.root"); 
	c0->Add("root://eosatlas//eos/atlas/user/j/jgonski/data18/data18_13TeV.350479.tileModule2Dead_minitrees.root"); 
	c1->Add("root://eosatlas//eos/atlas/user/j/jgonski/data18/data18_13TeV.350848.tileModule2Dead_minitrees.root"); 
	c2->Add("root://eosatlas//eos/atlas/user/j/jgonski/data18/data18_13TeV.350923.tileModule2Dead_minitrees.root"); 
	//c0->Add("/afs/cern.ch/work/j/jgonski/public/forShion/submitDir/data-minitrees/data18_13TeV.root");  
        parseNtuple(c0, "350479");
        parseNtuple(c1, "350848");
        parseNtuple(c2, "350923");
        
        //c0->Add("root://eosatlas//eos/atlas/user/j/jgonski/data17/data_C3C4E5_minitrees.root");
        //c1->Add("root://eosatlas//eos/atlas/user/j/jgonski/data17/data_F84be_minitrees.root");
        //data_C3C4E5_minitrees.root
        //data_CEplus_minitrees.root
        //data_D3_lowMu_minitrees.root
        //data_F84be_minitrees.root
        //data_I_highMu_minitrees.root
        //c0->Add("root://eosatlas//eos/atlas/user/j/jgonski/data18/data18_13TeV.350479.tileModule2Dead_minitrees.root");
	//parseNtuple(c0, "C3C4E5");
	//parseNtuple(c1, "F84be");
}
