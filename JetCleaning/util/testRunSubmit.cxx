#include <EventLoop/DirectDriver.h>
#include <EventLoop/Job.h>
#include <JetCleaning/JetCleaning.h>
#include <TSystem.h>
#include <SampleHandler/ScanDir.h>
#include <SampleHandler/ToolsDiscovery.h>

void testRunSubmit (const std::string& submitDir){
	// Set up the job for xAOD access:
	xAOD::Init().ignore();

	// create a new sample handler to describe the data files we use
	SH::SampleHandler sh;

	// scan for datasets in the given directory
	// this works if you are on lxplus, otherwise you'd want to copy over files
	// to your local machine and use a local path.  if you do so, make sure
	// that you copy all subdirectories and point this to the directory
	// containing all the files, not the subdirectories.

	// use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
	//const char* inputFilePath = gSystem->ExpandPathName ("/tmp/jgonski");
	//SH::ScanDir().filePattern("AOD.11038614._004726.pool.root.1").scan(sh,inputFilePath);
	SH::scanRucio (sh, "data17_13TeV.00333707.physics_Main.deriv.DAOD_JETM1.f857_m1855_p3355/");

	// set the name of the tree in our files
	// in the xAOD the TTree containing the EDM containers is "CollectionTree"
	sh.setMetaString ("nc_tree", "CollectionTree");

	// further sample handler configuration may go here

	// print out the samples we found
	sh.print ();

	// this is the basic description of our job
	EL::Job job;
	job.sampleHandler (sh); // use SampleHandler in this job
	job.options()->setDouble (EL::Job::optMaxEvents, 500); // for testing purposes, limit to run over the first 500 events only!

	// add output stream (i.e. files in the data-XXX directory)
	EL::OutputStream output("minitrees");
	job.outputAdd(output);
	//if (!gridSamples && !useProof) {
	//      job.options()->setDouble(EL::Job::optMaxEvents, 200000000);
	//}

	// add ntuple service (i.e. "syst_XXX" in each file)
	EL::NTupleSvc *ntuple = new EL::NTupleSvc("minitrees");
	ntuple->treeName("nominal");
	job.algsAdd(ntuple);

	// add our algorithm to the job
	JetCleaning *alg = new JetCleaning;

	// set the name of the algorithm (this is the name use with
	// messages)
	alg->SetName ("JetCleaningAlg");

	// later on we'll add some configuration options for our algorithm that go here

	job.algsAdd (alg);

	// make the driver we want to use:
	// this one works by running the algorithm directly:
	//EL::DirectDriver driver;
	EL::PrunDriver driver;
	// we can use other drivers to run things on the Grid, with PROOF, etc.

        //driver.options()->setDouble("nc_skipScout", 1);
        driver.options()->setString(EL::Job::optGridNGBPerJob, "10");
        driver.options()->setString(EL::Job::optGridExcludedSite, "ANALY_GLASGOW_SL6");
        driver.options()->setString("nc_outputSampleName", "user.jgonski." + submitDir + ".%in:name[1]%.%in:name[2]%");
        driver.submitOnly(job, submitDir);

	// process the job using the driver
	//driver.submit (job, submitDir);
}

