#include <TSystem.h>
#include <TPRegexp.h>
#include <xAODRootAccess/Init.h>
#include <AsgTools/MessageCheck.h>

#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
//#include "EventLoop/ProofDriver.h"
#include "EventLoopGrid/PrunDriver.h"
#include "EventLoop/OutputStream.h"
#include "EventLoopAlgs/NTupleSvc.h"

#include "SampleHandler/Sample.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "SampleHandler/ScanDir.h"//contains scanEOS
#include "SampleHandler/DiskListLocal.h"

#include "JetCleaning/JetCleaning.h"

using namespace std;

int main(int argc, char* argv[])
{
    //set to true for local parallel computation
    bool useProof = false;

    // Take the submit directory from the input if provided:
    std::string submitDir = "submitDirZ";
    //std::string submitDir = "/afs/cern.ch/user/e/etolley/work/public/MonoJet/test_samples/mc15c";
    if (argc > 1) submitDir = argv[ 1 ];

    // Possibly run on the grid over a given sample
    const Bool_t gridSamples(argc > 2);

    // Set up the job for xAOD access:
    xAOD::Init().ignore();

    // Construct the samples to run on:
    SH::SampleHandler sh;

    if (!gridSamples) {
	    //SH::readFileList(sh, "data17_JETM1", "data17_rel21.txt");
	    //SH::readFileList(sh, "mc15_JETM1", "mc15_test.txt");
	    SH::ScanDir scanit;
	    scanit.filePattern("DAOD**");
	    scanit.scan(sh, "/afs/cern.ch/work/j/jgonski/public/data18_13TeV/");

    } else {
        for (Int_t i = 2; i < argc; i++) {
            //testRun <folderName> <space-separated-list-of-datasets>
            SH::scanRucio(sh, argv[i]); //scanDQ2
        }
    }

    // Set the name of the input TTree. It's always "CollectionTree"
    // for xAOD files.
    sh.setMetaString("nc_tree", "CollectionTree");

    // fix for grid data15 files
    TString dsetName(sh.at(0)->name());
    if (dsetName.Contains("data15") && !dsetName.Contains("DAOD"))
        sh.setMetaString("nc_grid_filter", "*AOD.*");

    // Print what we found:
    sh.print();

    // Create an EventLoop job:
    EL::Job job;

    job.sampleHandler(sh);

    // add output stream (i.e. files in the data-XXX directory)
    EL::OutputStream output("minitrees");
    job.outputAdd(output);
    if (!gridSamples && !useProof) {
      job.options()->setDouble(EL::Job::optMaxEvents, 500);
    }

    // add ntuple service (i.e. "syst_XXX" in each file)
    EL::NTupleSvc *ntuple = new EL::NTupleSvc("minitrees");
    ntuple->treeName("nominal");
    job.algsAdd(ntuple);

    // Add our analysis to the job:
    JetCleaning* alg = new JetCleaning;
    alg->SetName ("JetCleaningAlg"); 
    alg->doSkim = kTRUE;  //skim to only dijet/fake/trigger events
    job.algsAdd(alg);

    // set sample options
    sh.setMetaString("isAFII", "NO");
    sh.setMetaString("is13TeV", "YES");

    // set access mode
    job.options()->setString(EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_class);

    for (UInt_t i = 0; i < sh.size(); i++) {
        SH::Sample *sample = sh.at(i);
        TString samplename(sample->name());

        // isAFII
        TPRegexp re("a\\d\\d\\d");
        if (samplename.Contains(re) || samplename.Contains("af2")) {
            cout << "Sample " << samplename << " is recognized as AFII" << endl;
            sample->setMetaString("isAFII", "YES");
        } else {
            cout << "Sample " << samplename << " is recognized as FULLSIM" << endl;
        }

        // is13TeV
        if (samplename.Contains("8TeV")) {
            cout << "Sample " << samplename << " is recognized as 8 TeV" << endl;

        } else {
            cout << "Sample " << samplename << " is recognized as 13 TeV" << endl;
            sample->setMetaString("is13TeV", "YES");
        }
        if (samplename.Contains("data")) {
            cout << "Sample " << samplename << " is recognized as Data" << endl;
            sample->setMetaString("isData", "YES");
        } else {
            cout << "Sample " << samplename << " is recognized as MC" << endl;

        }
        
        //15 vs 16
        if (samplename.Contains("data16")) {
            sample->setMetaString("isData16", "YES");
            cout << "Sample " << samplename << " is recognized as Data16" << endl;
        }

        if (samplename.Contains("JETM1")) {
            sample->setMetaString("isJETM1", "YES");
            cout << "Sample " << samplename << " is recognized as JETM1 Derivation" << endl;
        }
        if (samplename.Contains("EXOT2") || samplename.Contains("exot2")) {
            sample->setMetaString("isEXOT2", "YES");
            cout << "Sample " << samplename << " is recognized as EXOT2 Derivation" << endl;
        }
	if (samplename.Contains("EXOT5") || samplename.Contains("exot5")) {
            sample->setMetaString("isEXOT5", "YES");
            cout << "Sample " << samplename << " is recognized as EXOT5 Derivation" << endl;
        }
		if (samplename.Contains("DAOD")) {
            sample->setMetaString("isDeriv", "YES");
            cout << "Sample " << samplename << " is recognized as Derivation" << endl;
        }
    }
    
    //Testing Release 20.7 and EXOT0 derivations for 2016
    //job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_class);

    // Run the job using the local/direct driver:
    if (!gridSamples) {
        //if (useProof) {
            //EL::ProofDriver driver;
            //driver.numWorkers = 8;
            //job.options()->setString(EL::Job::optSubmitFlags, "-q 8nm");
            //driver.submit(job, submitDir);
        //} else {
            EL::DirectDriver driver;
            driver.submit(job, submitDir);
        //}
    } else {
        EL::PrunDriver driver;
        //driver.options()->setDouble("nc_skipScout", 1);
        driver.options()->setString(EL::Job::optGridNGBPerJob, "10");
        driver.options()->setString(EL::Job::optGridExcludedSite, "ANALY_GLASGOW_SL6");
        driver.options()->setString("nc_outputSampleName", "user.jgonski." + submitDir + ".%in:name[1]%.%in:name[2]%");
        driver.submitOnly(job, submitDir);
        //minimizing for JETM1 only!
    }

    return 0;
}

